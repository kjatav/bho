package com.bho.smart.framework;

public interface KeyboardStateListener {

    void onKeyboardOpen();

    void onKeyboardClose();
}
