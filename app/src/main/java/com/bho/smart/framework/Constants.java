package com.bho.smart.framework;

public interface Constants {

    String TAG = "Universal Log TAG======";

    String IS_LOGGED_IN = "is_logged_in";
    String ENGLISH_STR = "en";
    String GERMAN_STR = "de";
    String ENGLISH_CODE = "1";
    String GERMAN_CODE = "2";


    String SELECTED_LOCALE = "selected_local";


    int PERMISSIONS_REQUEST_CODE = 501;
    int SELECT_AREA = 101;
    int CAMERA_PROFILE_SOURCE = 234;
    int GALLERY_PROFILE_SOURCE = 235;
    int CAMERA_CAR_SOURCE = 236;
    int GALLERY_CAR_SOURCE = 237;

    String IS_SYNCED = "is_synced";

    String FROM_NAV = "from_nav";
    String HP_COMPLETED = "hpCompleted";


    /*-----------------SharedPreferences-----------*/
    String SP_COOKIES = "Set-Cookie";
    String SP_USER_LATITUDE = "userLatitude";
    String SP_USER_LONGITUDE = "userLongitude";
    String SP_USER_ID = "getUserId";
    String SP_FIREBASE_REGID = "firebaseRegId";
    String NO_USER_ID = "no_user_id";
    String CHAT_NOTIFICATION = "chat_notification";
    String CURRENT_OPPONENT_ID = "current_opponent_id";


    /* --------------Web Services-----------------*/
    String GET_PACKS = "get_packs";
    String UPDATE_PROFILE = "update_profile";


    /* --------------TAGS--------------------*/

    int STATUS_SUCCESS = 200;
    int STATUS_FAIL = 108;

    String RESULTS = "Results";

    String ID = "id";
    String IS_CHECKED = "ischecked";

    String HISTORY = "History";
    String TRAVEL_HISTORY = "travelhistory";

    String OTHER_HISTORY = "otherHistory";
    String OTHER_TRAVEL_HISTORY = "otherTravelhistory";


    String NAME = "name";


    String USER_ID = "userId";
    String USER_DATA = "userData";


    String FIRST_NAME = "firstName";
    String MIDDLE_NAME = "middleName";
    String LAST_NAME = "lastName";

    String NO_DATA = "no_data";


    String CHAT_ID = "chat_id";


    String DATA = "data";
    String MESSAGE_FOR = "messageFor";
    String NOTIFICATION_ID = "notificationId";


     /* --------------LOGIN & REGISTER--------------------*/


    String USER_NAME = "username";
    String PASSWORD = "password";
    String DEVICE_TOKEN = "deviceToken";
    String DEVICE_TYPE = "deviceType";
    String ANDROID = "Android";
    String RECOVERY_NAME = "recoveryEmail";
    String PROFILE_IMAGE = "profileImage";
    String IS_IMAGE_UPLOADED = "is_image_uploaded";



    String IS_MEMBER = "isMember";


    String USER_EMAIL = "user_email";
    String USER_PASSWORD = "user_password";

    String REMEMBER_ME = "remember_me";


    String COUNTRY_ID = "countryId";
    String COUNTRY_NAME = "countryName";
    String COUNTRY_CODE = "countryCode";

    String DAY = "day";
    String MONTH = "month";
    String YEAR = "year";

    /* --------------TABLES--------------------*/
    String TABLE_COUNTRY_DATA = "table_country_data";
    String TABLE_PERSONAL_INFO = "table_personal_info";
    String TABLE_ALLERGIES = "table_allergies";
    String TABLE_CURRENT_MEDICATIONS = "table_current_medications";
    String TABLE_MEDICAL_HISTORY = "table_medical_history";
    String TABLE_SURGICAL_HISTORY = "table_surgical_history";
    String TABLE_REVIEW_SYSTEMS = "table_review_systems";
    String TABLE_SOCIAL_HISTORY = "table_social_history";
    String TABLE_TRAVEL_HISTORY = "table_travel_history";
    String TABLE_FAMILY_HISTORY = "table_family_history";


    /* --------------PERSONAL INFORMATION--------------------*/
    String GENDER = "gender";
    String MARITAL_STATUS = "maritalstatus";
    String MOBILE_NUMBER = "mobilenumber";
    String STATE = "state";
    String CITY = "city";
    String ADDRESS = "address";
    String ZIP_CODE = "zipCode";
    String EMAIL = "email";
    String RECOVERY_EMAIL = "recoveryEmail";
    String OCCUPATION = "occupation";
    String WEIGHT = "weight";
    String WEIGHT_UNIT = "weightUnit";
    String HEIGHT_1 = "height";
    String HEIGHT_2 = "feet";
    String HEIGHT_UNIT = "heightUnit";

    String MALE = "male";
    String FEMALE = "female";

    String YES = "Yes";
    String NO = "No";


    String SINGLE = "single";
    String MARRIED = "married";
    String DIVORCED = "divorced";
    String WIDOWED = "widowed";
    String SEPARATED = "separated";

    String POUNDS = "Pounds";
    String KILOGRAM = "Kilogram";

    String FEET = "Feet";
    String INCHES = "Inches";
    String METER = "Meter";
    String CENTIMETRE = "Centimetre";

    String FEET_INCHES = "Feet/Inches";
    String METER_CENTIMETRE = "Meter/Centimetre";

    /* --------------ALLERGIES AND ADVERSE REACTION--------------------*/

    String SUBSTANCES = "substances";
    String DRUG_ALLERGIES = "drugAllergies";


    /* --------------CURRENT MEDICATION--------------------*/
    String CURRENT_MEDICATION = "CurrentMadication";

    String MEDICATION = "medication";
    String DOSE = "dose";
    String FREQUENCY = "frequency";
    String ESTIMATE = "eastimate";


    /* --------------REVIEW OF SYSTEMS--------------------*/

    String OTHER_CONSTITUTIONAL = "otherConstitutional";
    String OTHER_HEENT = "otherHeent";
    String OTHER_RESPIRATORY = "otherRespiratory";
    String OTHER_CARDIOVASCULAR = "otherCardiovascular";
    String OTHER_GASTROINTESTINAL = "otherGastrointestinal";
    String OTHER_GENITOURINARY = "oterGenitourinary";
    String OTHER_METABOLIC = "oterMetabolic";
    String OTHER_NEUROLOGIC = "OtherNeurologic";
    String OTHER_MUSCULOSKELETA = "otherMusculoskeleta";
    String OTHER_HEMATOLOGIC = "otherHematologic";
    String OTHER_IMMUNOLOGIC = "otherImmunologic";

    String FEVER = "fever";
    String FATIGUE = "fatigue";
    String NIGHT_SWEATS = "nightsweats";
    String WEIGHT_GAIN = "weightgain";
    String WEIGHT_LOSS = "weightloss";
    String HEADACHES = "headaches";
    String HEARING_LOSS = "hearingloss";
    String VISION_CHANGE = "visionchange";
    String COUGH = "cough";
    String WHEEZING = "wheezing";
    String CHEST_PAIN = "chestpain";
    String IRREGULAR_HEAR_BEAT = "irregularheartbeat";
    String PALPITATION = "palpitation";
    String ABDONINAL_PAIN = "abdominalpain";
    String BLOOD_IN_STOOL = "bloodinthestool";
    String CONSTIPATION = "constipation";
    String DARK_STOOL = "darkstool";
    String DIARRHEA = "diarrhea";
    String VOMITING = "vomiting";
    String BLOOD_IN_URINE = "bloodintheurine";
    String FREQUENT_URINATION = "frequenturination";
    String PAIN_WITH_URINATION = "painwithurination";
    String COLD_OR_HEAT_INTOLERANCE = "coldorheatintolerance";
    String FREQUENT_THIRST_HUNGER = "frequentthirstorhunger";
    String DEPRESSION = "depression";
    String DIZZINESS = "dizzines";
    String EMOTIONAL_DISTURBANCES = "emotionaldisturbances";
    String JOINT_PAIN = "jointpain";
    String JOINT_SWELLING = "jointswelling";
    String WEEKNESS = "weakness";
    String BLEEDING = "bleeding";
    String BRUSING = "bruising";
    String BEE_STRING_ALLERGY = "beestingallergy";
    String ENVIRONMENTAL_ALLERGIES = "environmentalallergies";
    String FOOD_ALLERGIES = "foodallergies";
    String SEASONAL_ALLERGIES = "seasonalallergies";

    /* --------------SOCIAL HISTORY--------------------*/

    String EXERCISE = "exercise";
    String OTHER_EXERCISE = "otherExercise";
    String SMOKE = "smoke";
    String ALCOHOL = "alcohol";
    String PACKS_A_DAY = "packsaday";
    String IND_QUANTITY = "indQuentity";

    String DAILY = "daily";
    String DAILY_2 = "3-5 Times Per Week";
    String DAILY_3 = "1-2 Times Per Week";
    String OCCASIONALLY = "Occasionally";
    String NEVER = "Never";

    /* --------------PAYMENT HISTORY--------------------*/
    String PAYMENT_DESC = "discription";
    String PAYMENT_CREATED_DATE = "createdDate";
    String PAYMENT_AMOUNT = "amount";
    String PAYMENT_TRASACTION_ID = "tid";

    /* --------------TESTIMONIAL--------------------*/
    String TESTI_PUBLISHED_DATE = "publishDate";
    String TESTI_TITLE = "title";
    String TESTI_AUTHOR = "author";
    String TESTI_APPROVED = "approved";
    String IS_EDIT = "is_edit";
    String TESTI_ID = "testId";

    /* --------------ADD TESTIMONIAL--------------------*/
    String TITLE = "title";
    String PUBLISH_DATE = "publishDate";
    String SHORT_CONTENT = "shortContent";
    String PAGE_CONTENT = "pageContent";
    String FILE_PATH = "filepath";


    /* --------------QC ARCHIVE--------------------*/
    String REQUEST_STATUS = "status";
    String REQUEST_TIME = "requestTime";
    String REQUEST_DESC = "description";
    String REQUEST_RESPONSE = "responseReceived";

    /* --------------NOTIFICATION--------------------*/
    String NOTIFICATION_DESC = "discription";
    String NOTIFICATION_SERVICE_TYPE = "serviceType";

    /* --------------CONSULT CHAT DETAILS--------------------*/
    String CONSULT_TYPE = "consult_type";


    String CONSULT_CHAT_ID = "consult_chat_id";
    String IS_RESPONDED = "is_responded";
    String CHAT_DATE = "date";
    String FILES = "files";
    String CHAT_DESC = "discription";
    String CREATE_USER_ID = "createUserId";
    String CHAT_MODEL = "chatmodel";
    String SERVICE_ID = "serviceId";
    String SERVICE_TYPE = "serviceType";


    /* --------------PAYMENT--------------------*/
    String QC = "QC";
    String EC = "EC";
    String MHT = "MHT";
    String MC = "MC";
    String UG = "UG";

    String PAYMENT_DESCRIPTION = "discription";
    String PAYMENT_FILE = "file";

    String SERVICE_DATA = "serviceData";

    String AMOUNT = "amount";
    String TOKEN = "token";
    String MEMBERSHIP = "membership";

    String FIRST_APPOINTMENT = "firstAppoinment";
    String FROM_PAYMENT = "from_payment";

    /* --------------EC PAYMENT--------------------*/
    String MEETING_DATE_1 = "meetingdate1";
    String MEETING_DATE_2 = "meetingdate2";
    String MEETING_DATE_3 = "meetingdate3";
    String TIME_ZONE_1 = "TImeZone1";
    String TIME_ZONE_2 = "TImeZone2";
    String TIME_ZONE_3 = "TImeZone3";
    String COM_MODE = "commode";
    String CONTACT_DETAIL = "contactDetail";

    String FACE_TIME = "FaceTime";
    String SKYPE = "Skype";
    String WHATSAPP = "WhatsApp";
    String IMO = "Imo";
    String VIBER = "Viber";

    String PAGENAME = "pagename";

    String IS_CONSUMER = "is_consumer";
}
