package com.bho.smart.framework;

import android.location.Location;

public interface MyLocationListener {

    void onReceived(Location mLastLocation);
}
