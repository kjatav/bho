package com.bho.smart.weservice;

import android.content.Context;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bho.smart.framework.Constants;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

public class SmartWebManager implements Constants {

    private static final int TIMEOUT = 20000;

    public enum REQUEST_METHOD_PARAMS {CONTEXT, URL, PARAMS, TAG, RESPONSE_LISTENER}

    private static SmartWebManager mInstance;
    private RequestQueue mRequestQueue;
    private Context mCtx;

    private SmartWebManager(Context context) {
        mCtx = context;
        mRequestQueue = getRequestQueue();
    }

    public static synchronized SmartWebManager getInstance(Context context) {
        if (mInstance == null) {
            mInstance = new SmartWebManager(context);
        }
        return mInstance;
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            mRequestQueue = Volley.newRequestQueue(mCtx.getApplicationContext());
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        JSONObject jsonBody = (JSONObject) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS);
        Log.v("@@@@@WSParameters", jsonBody.toString());
        JsonObjectRequest jsonObjectRequest = new JsonObjectRequest(Request.Method.POST, (String) requestParams.get(REQUEST_METHOD_PARAMS.URL), jsonBody,
                new Response.Listener<JSONObject>() {

                    @Override
                    public void onResponse(JSONObject response) {
                        try {
                            Log.v("@@@@@WSResponse_k", response.toString());
                            if (SmartUtils.getResponseCode(response) == STATUS_SUCCESS) {
                                String errorMessage = SmartUtils.validateResponse(response, null);
                                SmartUtils.hideSoftKeyboard(mContext);
                                if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                                    SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                                }
                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(response, true, 200);
                            } else {
                                int responseCode = SmartUtils.getResponseCode(response);
                                String errorMessage = SmartUtils.validateResponse(response, null);
                                SmartUtils.hideSoftKeyboard(mContext);
                                if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                                    SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                                }
                                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError volleyError) {

                        Log.v("@@@@@WSResponse", volleyError.toString());

                        SmartUtils.hideSoftKeyboard(mContext);
                        String errorMessage = VolleyErrorHelper.getMessage(volleyError, mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
                    }
                }) {
            @Override
            public Map<String, String> getHeaders() throws AuthFailureError {
                HashMap<String, String> headers = new HashMap<>();
                headers.put("Content-Type", "application/json; charset=utf-8");
                return headers;
            }
        };
        jsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        jsonObjectRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(jsonObjectRequest);
    }


    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, String filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest55", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);

        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),
                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, new Response.Listener() {

            @Override
            public void onResponse(Object object) {
                JSONObject webData = (JSONObject) object;
                Log.v("@@@@@WSResponsekkk", webData.toString());
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());

                    Log.v("@@@@@WSResponsekkk2", webData.toString());
                    if (SmartUtils.getResponseCode(webData) == STATUS_SUCCESS) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            // SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 200);
                    } else if (SmartUtils.getResponseCode(webData) == STATUS_FAIL) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            //SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 400);
                    } else {
                        int responseCode = SmartUtils.getResponseCode(webData);
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, false, responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.v("@@@@@WSError99", error.toString());

                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }

    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, HashMap<String, String> filePath, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),

                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, new Response.Listener() {
            @Override
            public void onResponse(Object object) {

                JSONObject webData = (JSONObject) object;
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());

                    Log.v("@@@@@WSResponsekkk2", webData.toString());
                    if (SmartUtils.getResponseCode(webData) == STATUS_SUCCESS) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            // SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 200);
                    } else if (SmartUtils.getResponseCode(webData) == STATUS_FAIL) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            //SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 400);
                    } else {
                        int responseCode = SmartUtils.getResponseCode(webData);
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, false, responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.getMessage() != null) {
                    Log.v("@@@@@WSError", error.getMessage());
                } else {
                    Log.v("@@@@@WSError", error.toString());

                }
                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }


    public <T> void addToRequestQueueMultipart(final HashMap<REQUEST_METHOD_PARAMS, Object> requestParams, HashMap<String, String> filePath, String singleKey, final boolean isShowSnackbar) {
        Log.v("@@@@@WSRequest", ((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)));
        final Context mContext = (Context) requestParams.get(REQUEST_METHOD_PARAMS.CONTEXT);
        MultiPartRequest smMultiPartRequest = new MultiPartRequest(((String) requestParams.get(REQUEST_METHOD_PARAMS.URL)),

                (Map<String, String>) requestParams.get(REQUEST_METHOD_PARAMS.PARAMS), filePath, singleKey, new Response.Listener() {
            @Override
            public void onResponse(Object object) {

                JSONObject webData = (JSONObject) object;
                try {
                    //  JSONObject webData = (JSONObject) object;
                    //     JSONObject response = webData.getJSONObject("Results");
                    Log.v("@@@@@WSResponsekkk", webData.toString());

                    Log.v("@@@@@WSResponsekkk2", webData.toString());
                    if (SmartUtils.getResponseCode(webData) == STATUS_SUCCESS) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            // SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 200);
                    } else if (SmartUtils.getResponseCode(webData) == STATUS_FAIL) {
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            //SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, true, 400);
                    } else {
                        int responseCode = SmartUtils.getResponseCode(webData);
                        String errorMessage = SmartUtils.validateResponse2(webData, null);
                        SmartUtils.hideSoftKeyboard(mContext);
                        if (isShowSnackbar && !TextUtils.isEmpty(errorMessage)) {
                            SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                        }
                        ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseReceived(webData, false, responseCode);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                if (error.getMessage() != null) {
                    Log.v("@@@@@WSError", error.getMessage());
                } else {
                    Log.v("@@@@@WSError", error.toString());

                }
                SmartUtils.hideSoftKeyboard(mContext);
                String errorMessage = VolleyErrorHelper.getMessage(error, mContext);
                SmartUtils.showSnackBar(mContext, errorMessage, Snackbar.LENGTH_LONG);
                ((OnResponseReceivedListener) requestParams.get(REQUEST_METHOD_PARAMS.RESPONSE_LISTENER)).onResponseError();
            }
        });
        smMultiPartRequest.setRetryPolicy(new DefaultRetryPolicy(
                TIMEOUT,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        smMultiPartRequest.setTag(requestParams.get(REQUEST_METHOD_PARAMS.TAG));
        getRequestQueue().add(smMultiPartRequest);
    }

    public interface OnResponseReceivedListener {
        void onResponseReceived(JSONObject tableRows, boolean isValidResponse, int responseCode) throws JSONException;

        void onResponseError();
    }


}