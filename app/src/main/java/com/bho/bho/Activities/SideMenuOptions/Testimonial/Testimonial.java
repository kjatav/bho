package com.bho.bho.Activities.SideMenuOptions.Testimonial;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Environment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bho.R;
import com.bho.bho.Activities.Login;
import com.bho.bho.Fragments.PaymentHistory;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.EntriesObject;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.AlertMagnatic;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.security.SecureRandom;
import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class Testimonial extends MasterActivity {

    JSONArray testimonialDataJson = new JSONArray();
    ArrayList<EntriesObject> testimonialDataArrayList = new ArrayList<>();
    ArrayList<JSONObject> testimonialDataSetArrayList = new ArrayList<>();

    private SmartTextView addTestimonialTv;
    private SmartEditText searchTestimonialEt;
    private RecyclerView testimonialListRv;
    private SmartTextView countTestimonialTv;
    private SmartTextView previousTv;
    private SmartTextView nextTv;
    private View testimonialHistoryNoData;

    private int totalDataCount = 0;


    private int pageNo = 0;

    private TestimonialHistoryAdapter testimonialHistoryAdapter;


    @Override
    public int getLayoutID() {
        return R.layout.activity_testimonial;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Testimonial");

        addTestimonialTv = findViewById(R.id.add_testimonial_tv);
        searchTestimonialEt = findViewById(R.id.search_testimonial_et);
        testimonialListRv = findViewById(R.id.testimonial_list_rv);
        countTestimonialTv = findViewById(R.id.count_testimonial_tv);
        testimonialHistoryNoData = findViewById(R.id.testimonial_history_no_data);
        previousTv = findViewById(R.id.previous_tv);
        nextTv = findViewById(R.id.next_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Testimonial.this);
        testimonialListRv.setLayoutManager(linearLayoutManager);

        testimonialHistoryAdapter = new TestimonialHistoryAdapter();
        testimonialListRv.setAdapter(testimonialHistoryAdapter);
    }

    @Override
    protected void onResume() {
        super.onResume();
        getTestimonials();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addTestimonialTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivityForResult(new Intent(Testimonial.this, AddTestimonial.class), 205);
            }
        });

        nextTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo < testimonialDataArrayList.size() - 1) {
                    pageNo = pageNo + 1;
                    setEntryData();
                }
            }
        });

        previousTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo > 0) {
                    pageNo = pageNo - 1;
                    setEntryData();
                }
            }
        });

        searchTestimonialEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (testimonialDataJson != null) {
                    try {
                        insertData(s.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }

    SecureRandom rnd = new SecureRandom();

    String randomString(int len, String AB) {
        StringBuilder sb = new StringBuilder(len);
        for (int i = 0; i < len; i++)
            sb.append(AB.charAt(rnd.nextInt(AB.length())));
        return sb.toString();
    }

    private void getTestimonials() {
        SmartUtils.showLoadingDialog(Testimonial.this);
        final JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetTestimonialList");


        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Testimonial.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        testimonialDataJson = response.getJSONArray(RESULTS);
                        insertData("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(Testimonial.this).addToRequestQueue(requestParams, false);
    }


    public class TestimonialHistoryAdapter extends RecyclerView.Adapter<TestimonialHistoryAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_testimonial, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.publishedDateTestiTv.setText(testimonialDataSetArrayList.get(position).getString(TESTI_PUBLISHED_DATE));
                holder.titleTestiTv.setText(testimonialDataSetArrayList.get(position).getString(TESTI_TITLE));
                holder.authorTestiTv.setText(testimonialDataSetArrayList.get(position).getString(TESTI_AUTHOR));

                if (Boolean.parseBoolean(testimonialDataSetArrayList.get(position).getString(TESTI_APPROVED))) {
                    holder.approvedTestiIv.setImageResource(R.drawable.ic_approved_testi);
                } else {
                    holder.approvedTestiIv.setImageResource(R.drawable.ic_unapproved_testi);
                }

                holder.editTestiIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(Testimonial.this, AddTestimonial.class);
                        i.putExtra(IS_EDIT, true);
                        try {
                            i.putExtra(TESTI_ID, testimonialDataSetArrayList.get(position).getString(TESTI_ID));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                });


                holder.deleteTestiIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        try {
                            deleteTestimonial(testimonialDataSetArrayList.get(position).getString(TESTI_ID));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }


        }

        @Override
        public int getItemCount() {
            return testimonialDataSetArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private SmartTextView publishedDateTestiTv;
            private SmartTextView titleTestiTv;
            private SmartTextView authorTestiTv;
            private ImageView approvedTestiIv;
            private ImageView editTestiIv;
            private ImageView deleteTestiIv;


            public ViewHolder(View itemView) {
                super(itemView);
                publishedDateTestiTv = itemView.findViewById(R.id.published_date_testi_tv);
                titleTestiTv = itemView.findViewById(R.id.title_testi_tv);
                authorTestiTv = itemView.findViewById(R.id.author_testi_tv);
                approvedTestiIv = itemView.findViewById(R.id.approved_testi_iv);
                editTestiIv = itemView.findViewById(R.id.edit_testi_iv);
                deleteTestiIv = itemView.findViewById(R.id.delete_testi_iv);
            }
        }
    }

    private void insertData(String filter) throws JSONException {
        testimonialDataArrayList.clear();
        testimonialDataSetArrayList.clear();
        pageNo = 0;
        totalDataCount = 0;
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        ArrayList<JSONObject> dataSet = new ArrayList<>();
        for (int i = 1; i <= testimonialDataJson.length(); i++) {
            if (TextUtils.isEmpty(filter)) {
                totalDataCount = ++totalDataCount;
                jsonObjects.add(testimonialDataJson.getJSONObject(i - 1));
            } else {
                String titleFromJsonArray = testimonialDataJson.getJSONObject(i - 1).getString(TESTI_TITLE);
                String dateFromJsonArray = testimonialDataJson.getJSONObject(i - 1).getString(TESTI_PUBLISHED_DATE);
                String authorFromJsonArray = testimonialDataJson.getJSONObject(i - 1).getString(TESTI_AUTHOR);
                filter = filter.toLowerCase();
                if (titleFromJsonArray.toLowerCase().contains(filter) ||
                        dateFromJsonArray.toLowerCase().contains(filter) ||
                        authorFromJsonArray.toLowerCase().contains(filter)) {
                    totalDataCount = ++totalDataCount;
                    jsonObjects.add(testimonialDataJson.getJSONObject(i - 1));
                }
            }
        }
        Log.e("@@K_DATA", jsonObjects.toString());
        int firstEntry = 0, lastEntry;
        for (int m = 1; m <= jsonObjects.size(); m++) {

//            Log.e("@@Module_test", "" + (m % 10));
            int k = m % 10;
            dataSet.add(jsonObjects.get(m - 1));
            if (m == 1) {
                firstEntry = 1;
            } else if (k == 1) {
                firstEntry = m;
            }
            if (k == 0 || m == jsonObjects.size()) {
                lastEntry = m;
                testimonialDataArrayList.add(new EntriesObject(firstEntry, lastEntry, dataSet));
                Log.e("@@PAGE", testimonialDataArrayList.size() + "---" + dataSet);

                dataSet = new ArrayList<>();
            }
        }
        setEntryData();
    }


    private void deleteTestimonial(final String testiId) {
        SmartUtils.getConfirmDialog(Testimonial.this, "Delete Testimonial", "Are you sure you want to delete this testimonial?",
                "Delete", "Cancel", true, new AlertMagnatic() {

                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {

                        SmartUtils.showLoadingDialog(Testimonial.this);
                        final JSONObject params = new JSONObject();
                        try {
                            params.put(USER_ID, getUserId());
                            params.put(TESTI_ID, testiId);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "DeleteTestimonial");


                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Testimonial.this);
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

                            @Override
                            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                                SmartUtils.hideLoadingDialog();
                                if (responseCode == 200) {
                                    getTestimonials();
                                }
                            }

                            @Override
                            public void onResponseError() {

                                SmartUtils.hideLoadingDialog();
                                Log.e("@@ERROR_HERE", "ERROR_HERE");
                            }
                        });
                        SmartWebManager.getInstance(Testimonial.this).addToRequestQueue(requestParams, false);
                    }

                    @Override
                    public void NegativeMethod(DialogInterface dialog, int id) {

                    }
                });

    }


    @SuppressLint("SetTextI18n")
    private void setEntryData() {
        if (testimonialDataArrayList.size() != 0) {
            testimonialHistoryNoData.setVisibility(View.GONE);

            testimonialDataSetArrayList = testimonialDataArrayList.get(pageNo).getDataSet();
            countTestimonialTv.setText("Showing " +
                    testimonialDataArrayList.get(pageNo).getFirstEntry()
                    + " to " +
                    testimonialDataArrayList.get(pageNo).getLastEntry()
                    + " of " +
                    totalDataCount
                    + " entries"
            );
        } else {
            testimonialHistoryNoData.setVisibility(View.VISIBLE);
            testimonialDataSetArrayList.clear();
        }
        testimonialHistoryAdapter.notifyDataSetChanged();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == 205) {
                getTestimonials();
            }
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
