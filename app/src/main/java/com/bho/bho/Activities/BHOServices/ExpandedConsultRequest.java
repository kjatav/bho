package com.bho.bho.Activities.BHOServices;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;

import com.bho.R;
import com.bho.bho.Activities.PrivacyPolicy;
import com.bho.bho.Activities.SideMenuOptions.Payment;
import com.bho.bho.Activities.SideMenuOptions.TermsAndConditions;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.CountryData;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.common.Object_Image;
import com.bho.smart.customViews.CustomClickListener;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import static com.bho.smart.framework.SmartUtils.checkAndfocusEditTextRed;
import static com.bho.smart.framework.SmartUtils.focusEditTextRed;
import static com.bho.smart.framework.SmartUtils.getCurrentDate;
import static com.bho.smart.framework.SmartUtils.getDateTimeDialog;
import static com.bho.smart.framework.SmartUtils.getImageFileUri;

public class ExpandedConsultRequest extends MasterActivity {


    private LinearLayout layout1;
    private View layout2;

    private RadioGroup communicationEcRequestRg;
    private LinearLayout codeEcRequestLl;
    private Spinner codeEcRequestSpn;
    private SmartEditText phoneEcRequestEt;
    private Spinner country1EcRequestSpn;
    private LinearLayout time1EcRequestLl;
    private SmartEditText time1EcRequestEt;
    private Spinner country2EcRequestSpn;
    private LinearLayout time2EcRequestLl;
    private SmartEditText time2EcRequestEt;
    private Spinner country3EcRequestSpn;
    private LinearLayout time3EcRequestLl;
    private SmartEditText time3EcRequestEt;
    private SmartTextView nextEcRequestEt;

    private RadioButton facetimeRb;
    private RadioButton skypeRb;
    private RadioButton whatsappRb;
    private RadioButton imoRb;
    private RadioButton viberRb;


    private SmartEditText reasonEcConsultEt;
    private SmartEditText descEcConsultEt;
    private RecyclerView filesEcConsultRv;
    private SmartTextView uploadEcConsultTv;
    private ImageView uploadPlaceholderIv;
    private CheckBox privacyEcConsultCb;
    private SmartTextView privacyEcConsultTv;
    private SmartTextView refundEcConsultTv;
    private SmartTextView termsEcConsultTv;
    private SmartTextView submitEcConsultTv;
    private SmartTextView cancelEcConsultTv;


    private boolean isLayout2Shown = false;

    private FilesConsultRequestAdapter filesConsultDetailAdapter;

    private ArrayList<Image> imageFiles = new ArrayList<>();

    private Uri currentImageUri;
    private String cameraImageName;

    private SmartCaching smartCaching;

    private ArrayList<CountryData> countryData = new ArrayList<>();
    private ArrayList<String> countryCodes = new ArrayList<>();
    private ArrayList<String> countryNames = new ArrayList<>();

    private String comMode;
    private String timeZone1;
    private String timeZone2;
    private String timeZone3;
    private String countryCode;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_expanded_consult_request;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Expanded Consult Request Detail");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        layout1 = findViewById(R.id.layout_1);
        layout2 = findViewById(R.id.layout_2);

        communicationEcRequestRg = findViewById(R.id.communication_ec_request_rg);
        codeEcRequestLl = findViewById(R.id.code_ec_request_ll);
        codeEcRequestSpn = findViewById(R.id.code_ec_request_spn);
        phoneEcRequestEt = findViewById(R.id.phone_ec_request_et);
        country1EcRequestSpn = findViewById(R.id.country1_ec_request_spn);
        time1EcRequestLl = findViewById(R.id.time1_ec_request_ll);
        time1EcRequestEt = findViewById(R.id.time1_ec_request_et);
        country2EcRequestSpn = findViewById(R.id.country2_ec_request_spn);
        time2EcRequestLl = findViewById(R.id.time2_ec_request_ll);
        time2EcRequestEt = findViewById(R.id.time2_ec_request_et);
        country3EcRequestSpn = findViewById(R.id.country3_ec_request_spn);
        time3EcRequestLl = findViewById(R.id.time3_ec_request_ll);
        time3EcRequestEt = findViewById(R.id.time3_ec_request_et);
        nextEcRequestEt = findViewById(R.id.next_ec_request_et);


        facetimeRb = findViewById(R.id.facetime_rb);
        skypeRb = findViewById(R.id.skype_rb);
        whatsappRb = findViewById(R.id.whatsapp_rb);
        imoRb = findViewById(R.id.imo_rb);
        viberRb = findViewById(R.id.viber_rb);

        facetimeRb.setChecked(true);
        comMode = FACE_TIME;

        reasonEcConsultEt = findViewById(R.id.reason_ec_consult_et);
        descEcConsultEt = findViewById(R.id.desc_ec_consult_et);
        filesEcConsultRv = findViewById(R.id.files_ec_consult_rv);
        uploadEcConsultTv = findViewById(R.id.upload_ec_consult_tv);
        uploadPlaceholderIv = findViewById(R.id.upload_ec_placeholder_iv);
        privacyEcConsultCb = findViewById(R.id.privacy_ec_consult_cb);
        privacyEcConsultTv = findViewById(R.id.privacy_ec_consult_tv);
        refundEcConsultTv = findViewById(R.id.refund_ec_consult_tv);
        termsEcConsultTv = findViewById(R.id.terms_ec_consult_tv);
        submitEcConsultTv = findViewById(R.id.submit_ec_consult_tv);
        cancelEcConsultTv = findViewById(R.id.cancel_ec_consult_tv);

        smartCaching = new SmartCaching(this);


        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        filesEcConsultRv.setLayoutManager(gridLayoutManager);
//        filesConsultChatRv.setItemAnimator(null);
        filesConsultDetailAdapter = new FilesConsultRequestAdapter();
        filesEcConsultRv.setAdapter(filesConsultDetailAdapter);

        setCountries();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        communicationEcRequestRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.facetime_rb:
                        codeEcRequestLl.setVisibility(View.VISIBLE);
                        comMode = FACE_TIME;
                        break;
                    case R.id.skype_rb:
                        codeEcRequestLl.setVisibility(View.GONE);
                        comMode = SKYPE;
                        break;
                    case R.id.whatsapp_rb:
                        codeEcRequestLl.setVisibility(View.VISIBLE);
                        comMode = WHATSAPP;
                        break;
                    case R.id.imo_rb:
                        codeEcRequestLl.setVisibility(View.VISIBLE);
                        comMode = IMO;
                        break;
                    case R.id.viber_rb:
                        codeEcRequestLl.setVisibility(View.VISIBLE);
                        comMode = VIBER;
                        break;
                }
            }
        });


        codeEcRequestSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                countryCode = parent.getItemAtPosition(position).toString();
                Log.d("@@CountryCode_raw", parent.getItemAtPosition(position).toString());
                Log.d("@@CountryCode", countryCode);
//                Log.d("@@CountryCode", countryCode);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        country1EcRequestSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                timeZone1 = parent.getItemAtPosition(position).toString();


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        country2EcRequestSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                timeZone2 = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        country3EcRequestSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                timeZone3 = parent.getItemAtPosition(position).toString();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        nextEcRequestEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, phoneEcRequestEt, true, null, 0)) {
                } else if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, time1EcRequestEt, true, null, 0)) {
                } else if (!check72Hour(time1EcRequestEt.getText().toString().trim())) {
                    focusEditTextRed(ExpandedConsultRequest.this, time1EcRequestEt, true, "Selected date and time should be more than 72 hours", 0);
                } else if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, time2EcRequestEt, true, null, 0)) {
                } else if (!check72Hour(time2EcRequestEt.getText().toString().trim())) {
                    focusEditTextRed(ExpandedConsultRequest.this, time2EcRequestEt, true, "Selected date and time should be more than 72 hours", 0);
                } else if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, time3EcRequestEt, true, null, 0)) {
                } else if (!check72Hour(time3EcRequestEt.getText().toString().trim())) {
                    focusEditTextRed(ExpandedConsultRequest.this, time3EcRequestEt, true, "Selected date and time should be more than 72 hours", 0);
                } else if (!checkSimilar()) {
                } else {
                    layout1.setVisibility(View.GONE);
                    layout2.setVisibility(View.VISIBLE);
                    isLayout2Shown = true;
                }
            }
        });

        setDateTimePicker(time1EcRequestEt, time1EcRequestEt);
        setDateTimePicker(time1EcRequestLl, time1EcRequestEt);

        setDateTimePicker(time2EcRequestEt, time2EcRequestEt);
        setDateTimePicker(time2EcRequestLl, time2EcRequestEt);

        setDateTimePicker(time3EcRequestEt, time3EcRequestEt);
        setDateTimePicker(time3EcRequestLl, time3EcRequestEt);


        privacyEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ExpandedConsultRequest.this, PrivacyPolicy.class));
            }
        });

        refundEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ExpandedConsultRequest.this, PrivacyPolicy.class));
            }
        });

        termsEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(ExpandedConsultRequest.this, TermsAndConditions.class));
            }
        });


        uploadEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFiles();
            }
        });

        submitEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (check()) {
                        passDataAndSubmit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });


        cancelEcConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }


    private void setDateTimePicker(View view, final SmartEditText etView) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View v) {
                getDateTimeDialog(ExpandedConsultRequest.this, getCurrentDate(), true, new CustomClickListener() {
                    @Override
                    public void onClick(String value) {
                        etView.setText(value);
                        if (!check72Hour(value)) {
                            focusEditTextRed(ExpandedConsultRequest.this, etView, true, "Selected date and time should be more than 96 hours", 0);
                        }
                        Log.d("@@IS_LESS_72", String.valueOf(
                                check72Hour(value)));


                    }
                }, getString(R.string.date_time_format_4));
            }
        });
    }

    private boolean checkSimilar() {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_time_format_4));
        long dateTime1 = 0;
        long dateTime2 = 0;
        long dateTime3 = 0;

        try {
            dateTime1 = simpleDateFormat.parse(time1EcRequestEt.getText().toString()).getTime();
            dateTime2 = simpleDateFormat.parse(time2EcRequestEt.getText().toString()).getTime();
            dateTime3 = simpleDateFormat.parse(time3EcRequestEt.getText().toString()).getTime();

            Log.d("@@dateTime1", String.valueOf(dateTime1));
            Log.d("@@dateTime2", String.valueOf(dateTime2));
            Log.d("@@dateTime3", String.valueOf(dateTime3));
        } catch (ParseException e) {
            e.printStackTrace();
        }

        if (dateTime1 == dateTime2 && timeZone1.equalsIgnoreCase(timeZone2)) {
            focusEditTextRed(ExpandedConsultRequest.this, true, "No two timings can be equal of the same countries", 0, time1EcRequestEt, time2EcRequestEt);
            time3EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        } else if (dateTime1 == dateTime3 && timeZone1.equalsIgnoreCase(timeZone3)) {
            focusEditTextRed(ExpandedConsultRequest.this, true, "No two timings can be equal of the same countries", 0, time1EcRequestEt, time3EcRequestEt);
            time2EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        } else if (dateTime2 == dateTime3 && timeZone2.equalsIgnoreCase(timeZone3)) {
            focusEditTextRed(ExpandedConsultRequest.this, true, "No two timings can be equal of the same countries", 0, time2EcRequestEt, time3EcRequestEt);
            time1EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return false;
        } else {
            time1EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            time2EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            time3EcRequestEt.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
            return true;
        }
    }

    private boolean check72Hour(String dateToSend) {
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(getString(R.string.date_time_format_4));
        Date date2 = null;
        try {
            date2 = simpleDateFormat.parse(dateToSend);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        assert date2 != null;
        long different = date2.getTime() - Calendar.getInstance().getTime().getTime();

        System.out.println("date1 : " + Calendar.getInstance().getTime());
        System.out.println("date2 : " + date2);
        System.out.println("different : " + different);

        Log.d("@@HOUR_DIFFERENCE", "" + TimeUnit.MILLISECONDS.toHours(different));

        return TimeUnit.MILLISECONDS.toHours(different) > 96;
    }


    private void passDataAndSubmit() throws JSONException {

        JSONObject dataToPass = new JSONObject();
        JSONArray filePathData = new JSONArray();

        dataToPass.put(COM_MODE, comMode);
        dataToPass.put(COUNTRY_CODE, countryCode);
        dataToPass.put(CONTACT_DETAIL, phoneEcRequestEt.getText().toString().trim());
        dataToPass.put(MEETING_DATE_1, time1EcRequestEt.getText().toString().trim());
        dataToPass.put(TIME_ZONE_1, timeZone1);
        dataToPass.put(MEETING_DATE_2, time2EcRequestEt.getText().toString().trim());
        dataToPass.put(TIME_ZONE_2, timeZone2);
        dataToPass.put(MEETING_DATE_3, time3EcRequestEt.getText().toString().trim());
        dataToPass.put(TIME_ZONE_3, timeZone3);

        dataToPass.put(TITLE, reasonEcConsultEt.getText().toString().trim());
        dataToPass.put(PAYMENT_DESCRIPTION, descEcConsultEt.getText().toString().trim());
        for (Image i : imageFiles) {
            filePathData.put(i.path);
        }
        dataToPass.put(PAYMENT_FILE, filePathData);

        Log.d("@@SERVICE_DATA===", dataToPass.toString());
        Intent i = new Intent(ExpandedConsultRequest.this, Payment.class);
        i.putExtra(SERVICE_TYPE, EC);
        i.putExtra(SERVICE_DATA, dataToPass.toString());
        startActivity(i);
    }

    private boolean check() {
        boolean isValid = true;
        if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, reasonEcConsultEt, true, null, 0)) {
            isValid = false;
        } else if (!checkAndfocusEditTextRed(ExpandedConsultRequest.this, descEcConsultEt, true, null, 0)) {
            isValid = false;
        } else if (!privacyEcConsultCb.isChecked()) {
            SmartUtils.showSnackBar(ExpandedConsultRequest.this, "Please accept Policies and Terms before submitting", Snackbar.LENGTH_LONG);
            isValid = false;
        }
        return isValid;
    }

    private class FilesConsultRequestAdapter extends RecyclerView.Adapter<ExpandedConsultRequest.FilesConsultRequestAdapter.ViewHolder> {

        @Override
        public FilesConsultRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_files, viewGroup, false);
            return new ExpandedConsultRequest.FilesConsultRequestAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ExpandedConsultRequest.FilesConsultRequestAdapter.ViewHolder holder, final int position) {
            if (!TextUtils.isEmpty(imageFiles.get(position).name))
                Picasso.with(ExpandedConsultRequest.this).load(new File(imageFiles.get(position).path)).placeholder(R.drawable.ic_upload_img).into(holder.imageFilesIv);
            else {
                holder.imageFilesIv.setImageResource(R.drawable.ic_upload_img);
            }
            holder.nameFilesTv.setText(imageFiles.get(position).name);
            holder.deleteFilesIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageFiles.remove(position);
                    notifyDataSetChanged();
                    if (imageFiles.size() == 0) {
                        uploadPlaceholderIv.setVisibility(View.VISIBLE);
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return imageFiles.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageFilesIv;
            private SmartTextView nameFilesTv;
            private ImageView deleteFilesIv;


            public ViewHolder(View itemView) {
                super(itemView);
                imageFilesIv = itemView.findViewById(R.id.image_files_iv);
                nameFilesTv = itemView.findViewById(R.id.name_files_tv);
                deleteFilesIv = itemView.findViewById(R.id.delete_files_iv);
            }
        }
    }


    private void addFiles() {
        final Dialog dialogImageSource = new Dialog(ExpandedConsultRequest.this);
        dialogImageSource.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImageSource.setContentView(R.layout.dialog_imagesource);

        SmartTextView cameraBtn = dialogImageSource.findViewById(R.id.camera_btn);
        SmartTextView galleryBtn = dialogImageSource.findViewById(R.id.gallery_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Object_Image object_image = getImageFileUri(ExpandedConsultRequest.this);
                currentImageUri = object_image.getImageUri();
                cameraImageName = object_image.getImageName();

                Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri); // set the image file name
                startActivityForResult(intentPicture, CAMERA_PROFILE_SOURCE);  // 1 for REQUEST_CAMERA and 2 for REQUEST_CAMERA_ATT
                dialogImageSource.dismiss();
            }
        });

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ExpandedConsultRequest.this, AlbumSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 4);
                startActivityForResult(intent, GALLERY_PROFILE_SOURCE);
                dialogImageSource.dismiss();
            }
        });
        dialogImageSource.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PROFILE_SOURCE) {
                imageFiles.add(new Image(0, cameraImageName, (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + cameraImageName), false));
                filesConsultDetailAdapter.notifyDataSetChanged();
                if (imageFiles.size() > 0) {
                    uploadPlaceholderIv.setVisibility(View.GONE);
                }
            } else if (requestCode == GALLERY_PROFILE_SOURCE) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                imageFiles.addAll(images);
                filesConsultDetailAdapter.notifyDataSetChanged();
                if (imageFiles.size() > 0) {
                    uploadPlaceholderIv.setVisibility(View.GONE);
                }
            }
        }
    }

    private void setCountries() {
        ArrayList<ContentValues> countryFromDatabase = smartCaching.getDataFromCache(TABLE_COUNTRY_DATA);
        if (countryFromDatabase != null) {
            for (int i = 0; i < countryFromDatabase.size(); i++) {
                countryData.add(new CountryData(countryFromDatabase.get(i).getAsString(COUNTRY_ID),
                        countryFromDatabase.get(i).getAsString(COUNTRY_NAME),
                        countryFromDatabase.get(i).getAsString(COUNTRY_CODE)));


                countryCodes.add(countryFromDatabase.get(i).getAsString(COUNTRY_NAME).trim() + " +" + countryFromDatabase.get(i).getAsString(COUNTRY_ID).trim());
                countryNames.add(countryFromDatabase.get(i).getAsString(COUNTRY_NAME));
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(ExpandedConsultRequest.this, android.R.layout.simple_spinner_item, countryCodes);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            codeEcRequestSpn.setAdapter(dataAdapter);

            ArrayAdapter<String> dataAdapter2 = new ArrayAdapter<>(ExpandedConsultRequest.this, android.R.layout.simple_spinner_item, countryNames);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            country1EcRequestSpn.setAdapter(dataAdapter2);


            ArrayAdapter<String> dataAdapter3 = new ArrayAdapter<>(ExpandedConsultRequest.this, android.R.layout.simple_spinner_item, countryNames);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            country2EcRequestSpn.setAdapter(dataAdapter3);


            ArrayAdapter<String> dataAdapter4 = new ArrayAdapter<>(ExpandedConsultRequest.this, android.R.layout.simple_spinner_item, countryNames);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            country3EcRequestSpn.setAdapter(dataAdapter4);

        }

    }


    @Override
    public void onBackPressed() {
        if (isLayout2Shown) {
            layout1.setVisibility(View.VISIBLE);
            layout2.setVisibility(View.GONE);
            isLayout2Shown = false;
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
