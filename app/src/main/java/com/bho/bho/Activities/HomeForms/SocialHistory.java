package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;

public class SocialHistory extends MasterActivity {

    private RadioGroup socialHistoryRg;

    private RadioButton dailySocialHistoryRb;
    private RadioButton daily2SocialHistoryRb;
    private RadioButton daily3SocialHistoryRb;
    private RadioButton occasionallySocialHistoryRb;
    private RadioButton neverSocialHistoryRb;

    private RecyclerView fieldsSocialHistoryRv;

    private SmartEditText addSocialHistoryEt;

    private SmartTextView addSocialHistoryTv;

    private RadioGroup smokeSocialHistoryRg;

    private RadioButton smokeYesRb;
    private RadioButton smokeNoRb;

    private RadioGroup drinkSocialHistoryRg;

    private RadioButton drinkYesRg;
    private RadioButton drinkNoRg;

    private LinearLayout smokeQuantityLl;
    private LinearLayout drinkQuantityLl;

    private SmartEditText smokeQuantityEt;
    private SmartEditText drinkQuantityEt;


    private SmartTextView submitSocialHistoryTv;

    private AdditionalFieldsAdapter additionalFieldsAdapter;

    private SmartCaching smartCaching;

    private ArrayList<String> socialExtraHistoryData = new ArrayList<>();

    private String exerciseFrequency = NEVER;
    private String doesSmoke = NO;
    private String doesDrink = NO;

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_social_history;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Social History");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        socialHistoryRg = findViewById(R.id.social_history_rg);

        dailySocialHistoryRb = findViewById(R.id.daily_social_history_rb);
        daily2SocialHistoryRb = findViewById(R.id.daily2_social_history_rb);
        daily3SocialHistoryRb = findViewById(R.id.daily3_social_history_rb);
        occasionallySocialHistoryRb = findViewById(R.id.occasionally_social_history_rb);
        neverSocialHistoryRb = findViewById(R.id.never_social_history_rb);

        fieldsSocialHistoryRv = findViewById(R.id.fields_social_history_rv);

        addSocialHistoryEt = findViewById(R.id.add_social_history_et);

        addSocialHistoryTv = findViewById(R.id.add_social_history_tv);

        smokeSocialHistoryRg = findViewById(R.id.smoke_social_history_rg);

        smokeYesRb = findViewById(R.id.smoke_yes_rb);
        smokeNoRb = findViewById(R.id.smoke_no_rb);

        smokeQuantityLl = findViewById(R.id.smoke_quantity_ll);
        smokeQuantityEt = findViewById(R.id.smoke_quantity_et);

        drinkQuantityLl = findViewById(R.id.drink_quantity_ll);
        drinkQuantityEt = findViewById(R.id.drink_quantity_et);


        drinkSocialHistoryRg = findViewById(R.id.drink_social_history_rg);

        drinkYesRg = findViewById(R.id.drink_yes_rg);
        drinkNoRg = findViewById(R.id.drink_no_rg);

        submitSocialHistoryTv = findViewById(R.id.submit_social_history_tv);


        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(SocialHistory.this);
        fieldsSocialHistoryRv.setLayoutManager(linearLayoutManager);

        additionalFieldsAdapter = new AdditionalFieldsAdapter(SocialHistory.this, socialExtraHistoryData);
        fieldsSocialHistoryRv.setAdapter(additionalFieldsAdapter);


        smartCaching = new SmartCaching(this);

        getDataFromDataBase();

//        getSocialHistory();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        socialHistoryRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isUpdated = true;

                switch (checkedId) {
                    case R.id.daily_social_history_rb:
                        exerciseFrequency = DAILY;
                        break;
                    case R.id.daily2_social_history_rb:
                        exerciseFrequency = DAILY_2;
                        break;
                    case R.id.daily3_social_history_rb:
                        exerciseFrequency = DAILY_3;
                        break;
                    case R.id.occasionally_social_history_rb:
                        exerciseFrequency = OCCASIONALLY;
                        break;
                    case R.id.never_social_history_rb:
                        exerciseFrequency = NEVER;
                        break;
                }
            }
        });

        smokeSocialHistoryRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isUpdated = true;

                switch (checkedId) {
                    case R.id.smoke_yes_rb:
                        doesSmoke = YES;
                        showSmokeQuantity(true);
                        break;
                    case R.id.smoke_no_rb:
                        doesSmoke = NO;
                        showSmokeQuantity(false);
                        break;
                }
            }
        });


        drinkSocialHistoryRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isUpdated = true;

                switch (checkedId) {
                    case R.id.drink_yes_rg:
                        doesDrink = YES;
                        showDrinkQuantity(true);
                        break;
                    case R.id.drink_no_rg:
                        doesDrink = NO;
                        showDrinkQuantity(false);
                        break;
                }
            }
        });

        addSocialHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(addSocialHistoryEt.getText().toString().trim())) {
                    additionalFieldsAdapter.addAdditionalFields(addSocialHistoryEt.getText().toString().trim());
                    addSocialHistoryEt.setText("");
                    isUpdated = true;
                }
            }
        });


        smokeQuantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isUpdated = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        drinkQuantityEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                isUpdated = true;
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });

        submitSocialHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isUpdated || additionalFieldsAdapter.giveUpadateStautus()) {
                    if (doesSmoke.equalsIgnoreCase(YES) && TextUtils.isEmpty(smokeQuantityEt.getText().toString().trim())) {
                        SmartUtils.focusEditTextRed(SocialHistory.this, smokeQuantityEt, true, null, 0);
                    } else if (doesDrink.equalsIgnoreCase(YES) && TextUtils.isEmpty(drinkQuantityEt.getText().toString().trim())) {
                        SmartUtils.focusEditTextRed(SocialHistory.this, drinkQuantityEt, true, null, 0);
                    } else {
                        putDataIntoDatabaseAndSync(viewToJson());
                    }
                } else {
                    startActivity(new Intent(SocialHistory.this, Home.class));
                }
            }
        });
    }
    
    
    /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getSocialHistory() {
        SmartUtils.showLoadingDialog(SocialHistory.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetSocialHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, SocialHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_SOCIAL_HISTORY, OTHER_EXERCISE);
                        String query = "SELECT * FROM " + TABLE_SOCIAL_HISTORY + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_SOCIAL_HISTORY, query).get(0);
                        Log.d("@@@SocialHistory_data", dataFromDatabase.getAsString(OTHER_EXERCISE));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateSocialHistory(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(SocialHistory.this);


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdateSocialHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, SocialHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    smartCaching.updateSyncStatus(TABLE_SOCIAL_HISTORY, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(SocialHistory.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(SocialHistory.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


        /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/

    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_SOCIAL_HISTORY + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_SOCIAL_HISTORY, query) != null && smartCaching.getDataFromCache(TABLE_SOCIAL_HISTORY, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_SOCIAL_HISTORY, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_SOCIAL_HISTORY, getUserId())) {
                getSocialHistory();
            } else {
                if (doesSmoke.equalsIgnoreCase(YES) && TextUtils.isEmpty(smokeQuantityEt.getText().toString().trim())) {
                    SmartUtils.focusEditTextRed(SocialHistory.this, smokeQuantityEt, true, null, 0);
                } else if (doesSmoke.equalsIgnoreCase(YES) && TextUtils.isEmpty(smokeQuantityEt.getText().toString().trim())) {
                    SmartUtils.focusEditTextRed(SocialHistory.this, smokeQuantityEt, true, null, 0);
                } else {
                    updateSocialHistory(viewToJson(), false);
                }
            }
        } else {
            getSocialHistory();
        }
    }

    private JSONObject viewToJson() {

        JSONObject params = new JSONObject();
        try {
            //       Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(EXERCISE, exerciseFrequency);
            params.put(OTHER_EXERCISE, additionalFieldsAdapter.getAdditionalFields());
            params.put(SMOKE, doesSmoke);
            params.put(ALCOHOL, doesDrink);
            params.put(IND_QUANTITY, drinkQuantityEt.getText().toString().trim());
            params.put(PACKS_A_DAY, smokeQuantityEt.getText().toString().trim());

            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_SOCIAL_HISTORY, OTHER_EXERCISE);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_SOCIAL_HISTORY, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_SOCIAL_HISTORY, getUserId())) {
            updateSocialHistory(data, true);
        }
    }


    private void setData(ContentValues data) {
        socialExtraHistoryData.clear();

        if (data.getAsString(EXERCISE) != null) {
            switch (data.getAsString(EXERCISE)) {
                case DAILY:
                    exerciseFrequency = DAILY;
                    dailySocialHistoryRb.setChecked(true);
                    break;

                case DAILY_2:
                    exerciseFrequency = DAILY_2;
                    daily2SocialHistoryRb.setChecked(true);
                    break;

                case DAILY_3:
                    exerciseFrequency = DAILY_3;
                    daily3SocialHistoryRb.setChecked(true);
                    break;

                case OCCASIONALLY:
                    exerciseFrequency = OCCASIONALLY;
                    occasionallySocialHistoryRb.setChecked(true);
                    break;

                case NEVER:
                    exerciseFrequency = NEVER;
                    neverSocialHistoryRb.setChecked(true);
                    break;
                default:
                    exerciseFrequency = NEVER;
                    neverSocialHistoryRb.setChecked(true);
                    break;
            }
        } else {
            exerciseFrequency = NEVER;
            neverSocialHistoryRb.setChecked(true);
        }
        try {
            JSONArray otherHistoryJson = new JSONArray(data.getAsString(OTHER_EXERCISE));

            for (int i = 0; i < otherHistoryJson.length(); i++) {
                socialExtraHistoryData.add(otherHistoryJson.getString(i));
            }
            additionalFieldsAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }


        switch (data.getAsString(SMOKE)) {
            case YES:
                doesSmoke = YES;
                smokeQuantityEt.setText(data.getAsString(PACKS_A_DAY));
                smokeQuantityEt.setSelection(smokeQuantityEt.length());
                showSmokeQuantity(true);
                smokeYesRb.setChecked(true);
                break;

            case NO:
                doesSmoke = NO;
                showSmokeQuantity(false);
                smokeNoRb.setChecked(true);
                break;
        }


        switch (data.getAsString(ALCOHOL)) {
            case YES:
                doesDrink = YES;
                showDrinkQuantity(true);
                drinkQuantityEt.setText(data.getAsString(IND_QUANTITY));
                drinkQuantityEt.setSelection(drinkQuantityEt.length());
                drinkYesRg.setChecked(true);
                break;

            case NO:
                doesDrink = NO;
                showDrinkQuantity(false);
                drinkNoRg.setChecked(true);
                break;
        }

    }


    private void showSmokeQuantity(boolean show) {
        if (show) {
            smokeQuantityLl.setVisibility(View.VISIBLE);
            Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein_anmation);
            smokeQuantityLl.startAnimation(fadeOut);
        } else {
            smokeQuantityLl.clearAnimation();
            smokeQuantityLl.setVisibility(View.GONE);
        }
    }


    private void showDrinkQuantity(boolean show) {
        if (show) {
            drinkQuantityLl.setVisibility(View.VISIBLE);
            Animation fadeOut = AnimationUtils.loadAnimation(getApplicationContext(), R.anim.fadein_anmation);
            drinkQuantityLl.startAnimation(fadeOut);
        } else {
            drinkQuantityLl.clearAnimation();
            drinkQuantityLl.setVisibility(View.GONE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
