package com.bho.bho.Activities.BHOServices;

import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;

import com.bho.R;
import com.bho.bho.Activities.PrivacyPolicy;
import com.bho.bho.Activities.SideMenuOptions.Payment;
import com.bho.bho.Activities.SideMenuOptions.TermsAndConditions;
import com.bho.bho.MasterActivity;
import com.bho.smart.common.Object_Image;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;

import static com.bho.smart.framework.SmartUtils.checkAndfocusEditTextRed;
import static com.bho.smart.framework.SmartUtils.getImageFileUri;

public class MyHealthTrendRequest extends MasterActivity {

    private RadioGroup appointmentMhtRg;
    private RadioButton oneMonthMhtRb;
    private SmartEditText notesMhtEt;
    private RecyclerView filesMhtRv;
    private CheckBox privacyMhtCb;
    private SmartTextView privacyMhtTv;
    private SmartTextView refundMhtTv;
    private SmartTextView termsMhtTv;
    private SmartTextView submitMhtTv;
    private SmartTextView cancelMhtTv;
    private SmartTextView uploadMhtPlaceholderTv;

    private ImageView uploadMhtPlaceholderIv;

    private FilesConsultRequestAdapter filesConsultDetailAdapter;

    private ArrayList<Image> imageFiles = new ArrayList<>();

    private Uri currentImageUri;
    private String cameraImageName;

    private String firstAppointment = "1";

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_my_health_trend_request;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("My Health Trends");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        appointmentMhtRg = findViewById(R.id.appointment_mht_rg);
        oneMonthMhtRb = findViewById(R.id.one_month_mht_rb);
        notesMhtEt = findViewById(R.id.notes_mht_et);
        filesMhtRv = findViewById(R.id.files_mht_rv);
        privacyMhtCb = findViewById(R.id.privacy_mht_cb);
        privacyMhtTv = findViewById(R.id.privacy_mht_tv);
        refundMhtTv = findViewById(R.id.refund_mht_tv);
        termsMhtTv = findViewById(R.id.terms_mht_tv);
        submitMhtTv = findViewById(R.id.submit_mht_tv);
        cancelMhtTv = findViewById(R.id.cancel_mht_tv);

        uploadMhtPlaceholderIv = findViewById(R.id.upload_mht_placeholder_iv);
        uploadMhtPlaceholderTv = findViewById(R.id.upload_mht_placeholder_tv);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        filesMhtRv.setLayoutManager(gridLayoutManager);
        filesConsultDetailAdapter = new FilesConsultRequestAdapter();
        filesMhtRv.setAdapter(filesConsultDetailAdapter);

        oneMonthMhtRb.setChecked(true);

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        appointmentMhtRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.one_month_mht_rb:
                        firstAppointment = "1";
                        break;

                    case R.id.two_month_mht_rb:
                        firstAppointment = "2";
                        break;

                    case R.id.six_month_mht_rb:
                        firstAppointment = "6";
                        break;
                }
            }
        });


        privacyMhtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyHealthTrendRequest.this, PrivacyPolicy.class));
            }
        });

        refundMhtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyHealthTrendRequest.this, PrivacyPolicy.class));
            }
        });

        termsMhtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(MyHealthTrendRequest.this, TermsAndConditions.class));
            }
        });


        uploadMhtPlaceholderTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFiles();
            }
        });

        submitMhtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (check()) {
                        passDataAndSubmit();
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        cancelMhtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

    }

    private boolean check() {
        boolean isValid = true;
        if (!checkAndfocusEditTextRed(MyHealthTrendRequest.this, notesMhtEt, true, null, 0)) {
            isValid = false;
        } else if (!privacyMhtCb.isChecked()) {
            SmartUtils.showSnackBar(MyHealthTrendRequest.this, "Please accept Policies and Terms before submitting", Snackbar.LENGTH_LONG);
            isValid = false;
        }
        return isValid;

    }

    private void passDataAndSubmit() throws JSONException {

        JSONObject dataToPass = new JSONObject();
        JSONArray filePathData = new JSONArray();
        dataToPass.put(FIRST_APPOINTMENT, firstAppointment);
        dataToPass.put(PAYMENT_DESCRIPTION, notesMhtEt.getText().toString().trim());
        for (Image i : imageFiles) {
            filePathData.put(i.path);
        }
        dataToPass.put(PAYMENT_FILE, filePathData);

        Log.d("@@SERVICE_DATA===", dataToPass.toString());
        Intent i = new Intent(MyHealthTrendRequest.this, Payment.class);
        i.putExtra(SERVICE_TYPE, MHT);
        i.putExtra(SERVICE_DATA, dataToPass.toString());
        startActivity(i);
    }

    private class FilesConsultRequestAdapter extends RecyclerView.Adapter<MyHealthTrendRequest.FilesConsultRequestAdapter.ViewHolder> {


        @Override
        public MyHealthTrendRequest.FilesConsultRequestAdapter.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_files, viewGroup, false);
            return new MyHealthTrendRequest.FilesConsultRequestAdapter.ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final MyHealthTrendRequest.FilesConsultRequestAdapter.ViewHolder holder, final int position) {
            if (!TextUtils.isEmpty(imageFiles.get(position).name))
                Picasso.with(MyHealthTrendRequest.this).load(new File(imageFiles.get(position).path)).placeholder(R.drawable.ic_upload_img).into(holder.imageFilesIv);
            else {
                holder.imageFilesIv.setImageResource(R.drawable.ic_upload_img);
            }
            holder.nameFilesTv.setText(imageFiles.get(position).name);
            holder.deleteFilesIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageFiles.remove(position);
                    notifyDataSetChanged();
                    if (imageFiles.size() == 0) {
                        uploadMhtPlaceholderIv.setVisibility(View.VISIBLE);
                    }
                }
            });
        }


        @Override
        public int getItemCount() {
            return imageFiles.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageFilesIv;
            private SmartTextView nameFilesTv;
            private ImageView deleteFilesIv;


            public ViewHolder(View itemView) {
                super(itemView);
                imageFilesIv = itemView.findViewById(R.id.image_files_iv);
                nameFilesTv = itemView.findViewById(R.id.name_files_tv);
                deleteFilesIv = itemView.findViewById(R.id.delete_files_iv);
            }
        }
    }


    private void addFiles() {
        final Dialog dialogImageSource = new Dialog(MyHealthTrendRequest.this);
        dialogImageSource.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImageSource.setContentView(R.layout.dialog_imagesource);

        SmartTextView cameraBtn = dialogImageSource.findViewById(R.id.camera_btn);
        SmartTextView galleryBtn = dialogImageSource.findViewById(R.id.gallery_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Object_Image object_image = getImageFileUri(MyHealthTrendRequest.this);
                currentImageUri = object_image.getImageUri();
                cameraImageName = object_image.getImageName();

                Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri); // set the image file name
                startActivityForResult(intentPicture, CAMERA_PROFILE_SOURCE);  // 1 for REQUEST_CAMERA and 2 for REQUEST_CAMERA_ATT
                dialogImageSource.dismiss();
            }
        });

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MyHealthTrendRequest.this, AlbumSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 4);
                startActivityForResult(intent, GALLERY_PROFILE_SOURCE);
                dialogImageSource.dismiss();
            }
        });
        dialogImageSource.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PROFILE_SOURCE) {
                imageFiles.add(new Image(0, cameraImageName, (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + cameraImageName), false));
                filesConsultDetailAdapter.notifyDataSetChanged();
                if (imageFiles.size() > 0) {
                    uploadMhtPlaceholderIv.setVisibility(View.GONE);
                } else {
                    uploadMhtPlaceholderIv.setVisibility(View.VISIBLE);
                }
            } else if (requestCode == GALLERY_PROFILE_SOURCE) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                imageFiles.addAll(images);
                filesConsultDetailAdapter.notifyDataSetChanged();
                if (imageFiles.size() > 0) {
                    uploadMhtPlaceholderIv.setVisibility(View.GONE);
                } else {
                    uploadMhtPlaceholderIv.setVisibility(View.VISIBLE);
                }
            }
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
