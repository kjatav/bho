package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;
import static java.util.jar.Pack200.Unpacker.TRUE;

public class AllergiesReaction extends MasterActivity {

    private RadioGroup allergiesRg;
    private RadioButton yesAllergiesRb;
    private RadioButton noAllergiesRb;
    private RecyclerView fieldsAllergiesRv;
    private SmartEditText addFieldsAllergiesEt;
    private SmartTextView addFieldsAllergiesTv;
    private SmartTextView submitAllergiesTv;


    private SmartCaching smartCaching;

    private AdditionalFieldsAdapter additionalFieldsAdapter;


    ArrayList<String> drugAllergiesData = new ArrayList<>();

    String substance = "Yes";

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_allergies_reaction;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Allergies and adverse reaction");

        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        smartCaching = new SmartCaching(this);

        allergiesRg = findViewById(R.id.allergies_rg);
        yesAllergiesRb = findViewById(R.id.yes_allergies_rb);
        noAllergiesRb = findViewById(R.id.no_allergies_rb);

        fieldsAllergiesRv = findViewById(R.id.fields_allergies_rv);
        addFieldsAllergiesEt = findViewById(R.id.add_fields_allergies_et);

        addFieldsAllergiesTv = findViewById(R.id.add_fields_allergies_tv);
        submitAllergiesTv = findViewById(R.id.submit_allergies_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(AllergiesReaction.this);
        fieldsAllergiesRv.setLayoutManager(linearLayoutManager);

        additionalFieldsAdapter = new AdditionalFieldsAdapter(AllergiesReaction.this, drugAllergiesData);
        fieldsAllergiesRv.setAdapter(additionalFieldsAdapter);

        getDataFromDataBase();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        addFieldsAllergiesTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(addFieldsAllergiesEt.getText().toString().trim())) {
                    additionalFieldsAdapter.addAdditionalFields(addFieldsAllergiesEt.getText().toString().trim());
                    addFieldsAllergiesEt.setText("");
                    isUpdated = true;
                }
            }
        });

        allergiesRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isUpdated = true;
                switch (checkedId) {
                    case R.id.yes_allergies_rb:
                        substance = "Yes";
                        break;
                    case R.id.no_allergies_rb:
                        substance = "No";
                        break;
                }
            }
        });

        submitAllergiesTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdated || additionalFieldsAdapter.giveUpadateStautus()) {
                    putDataIntoDatabaseAndSync(viewToJson());
                } else {
                    startActivity(new Intent(AllergiesReaction.this, Home.class));
                }
            }
        });
    }

    /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/

    private void getAllergiesData() {
        SmartUtils.showLoadingDialog(AllergiesReaction.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetAllergies");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, AllergiesReaction.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_ALLERGIES, DRUG_ALLERGIES);
                        String query = "SELECT * FROM " + TABLE_ALLERGIES + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_ALLERGIES, query).get(0);
                        Log.d("@@@Allergies_data", dataFromDatabase.getAsString(DRUG_ALLERGIES));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateAllergiesData(JSONObject params, final boolean isSubmit) {
        //  SmartUtils.showLoadingDialog(AllergiesReaction.this);

        Log.d("@@Request_JSON", params.toString());

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdateAllergies");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, AllergiesReaction.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@PI_success", ": true");
                    smartCaching.updateSyncStatus(TABLE_ALLERGIES, getUserId(), true);
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(AllergiesReaction.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(AllergiesReaction.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }




    /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_ALLERGIES + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_ALLERGIES, query) != null && smartCaching.getDataFromCache(TABLE_ALLERGIES, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_ALLERGIES, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_ALLERGIES, getUserId())) {
                getAllergiesData();
            } else {
                updateAllergiesData(viewToJson(), false);
            }
        } else {
            getAllergiesData();
        }
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(SUBSTANCES, substance);
            params.put(DRUG_ALLERGIES, additionalFieldsAdapter.getAdditionalFields());
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_ALLERGIES, DRUG_ALLERGIES);
        //  Log.d("@@SYNC_STATUS", smartCaching.getSyncStatus(TABLE_ALLERGIES, getUserId());
        if (!smartCaching.getSyncStatus(TABLE_ALLERGIES, getUserId())) {
            updateAllergiesData(data, true);
        }
    }

    private void setData(ContentValues data) {
        drugAllergiesData.clear();
        if (data.getAsString(SUBSTANCES) != null) {
            switch (data.getAsString(SUBSTANCES)) {
                case YES:
                    substance = YES;
                    yesAllergiesRb.setChecked(true);
                    break;
                case NO:
                    substance = NO;
                    noAllergiesRb.setChecked(true);
                    break;
            }
        } else {
            substance = NO;
            noAllergiesRb.setChecked(true);
        }

        if (data.getAsString(DRUG_ALLERGIES) != null) {
            try {
                JSONArray otherHistoryJson = new JSONArray(data.getAsString(DRUG_ALLERGIES));

                for (int i = 0; i < otherHistoryJson.length(); i++) {
                    drugAllergiesData.add(otherHistoryJson.getString(i));
                }
                additionalFieldsAdapter.notifyDataSetChanged();

            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
