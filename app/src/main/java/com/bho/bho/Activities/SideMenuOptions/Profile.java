package com.bho.bho.Activities.SideMenuOptions;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;

import com.bho.R;
import com.bho.bho.MasterActivity;

public class Profile extends MasterActivity {

    @Override
    public int getLayoutID() {
        return R.layout.activity_profile;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Profile");
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }

}
