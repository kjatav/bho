package com.bho.bho.Activities.BHOServices;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Toast;


import com.bho.R;
import com.bho.bho.MasterActivity;
import com.bho.smart.framework.SmartUtils;

import java.util.Arrays;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class MyChart extends MasterActivity {

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_my_chart;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("My Chart");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        showPayment();
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void showPayment() {
        WebView webPayment = findViewById(R.id.my_chart_wv);
        webPayment.setVerticalScrollBarEnabled(false);
        webPayment.setHorizontalScrollBarEnabled(false);
        webPayment.setWebViewClient(new OAuthWebViewClient());
        webPayment.getSettings().setJavaScriptEnabled(true);

        String url = "http://52.205.252.190/MemberProfile/ConsumerMyChartMobile?id=" + getUserId();
//        webPayment.loadUrl("http://192.168.0.122:421/MemberProfile/ConsumerMyChartMobile?id=555");
        //webPayment.loadUrl(url);

        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));


    }

    private class OAuthWebViewClient extends WebViewClient {

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {

            return false;
        }

        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {

        }

        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
            super.onPageStarted(view, url, favicon);
            SmartUtils.showLoadingDialog(MyChart.this);
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            super.onPageFinished(view, url);
            SmartUtils.hideLoadingDialog();
          /*  String[] paymentUrl = url.split("\\?");
            if (paymentUrl.length > 1) {
                String[] paymentStatus = paymentUrl[1].split("=");
                Log.e("@@@@", Arrays.toString(paymentStatus));

                HashMap<String, String> hashMap = new HashMap<>();
                if (paymentStatus.length > 1) {
                    if (paymentStatus[1].contains("&msg")) {
                        String[] finalPayment = paymentStatus[1].split("&");

                        hashMap.put("id", finalPayment[0]);
                        hashMap.put("msg", paymentStatus[2]);

                        Log.e("@@Final", hashMap.toString());
                        if (!paymentStatus[2].equalsIgnoreCase("decline")) {
                            Toast.makeText(MyChart.this, "Payment failed", Toast.LENGTH_SHORT).show();
                         //   checkOutPackage();
                        } else {
                          //  paymenyDecline();
                        }
                    }
                }

            }
            Log.d("", "onPageFinished URL: " + url);
            SmartUtils.hideLoadingDialog();*/
        }

        @Override
        public WebResourceResponse shouldInterceptRequest(WebView view, String url) {
            return null;
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


}
