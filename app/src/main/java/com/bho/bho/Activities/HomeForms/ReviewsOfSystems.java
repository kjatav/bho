package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v4.os.IResultReceiver;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;

public class ReviewsOfSystems extends MasterActivity {

    private CheckBox feverCb;
    private CheckBox nightsweatsCb;
    private CheckBox weightlossCb;
    private CheckBox fatigueCb;
    private CheckBox weightgainCb;
    private RecyclerView constitutionalRv;
    private SmartEditText constitutionalEt;
    private SmartTextView addConstitutionalTv;
    private CheckBox headachesCb;
    private CheckBox visionchangeCb;
    private CheckBox hearinglossCb;
    private RecyclerView heentRv;
    private SmartEditText heentEt;
    private SmartTextView addHeentTv;
    private CheckBox coughCb;
    private CheckBox wheezingCb;
    private RecyclerView respiratoryRv;
    private SmartEditText respiratoryEt;
    private SmartTextView addRespiratoryTv;
    private CheckBox chestpainCb;
    private CheckBox palpitationCb;
    private CheckBox irregularheartbeatCb;
    private RecyclerView cardiovascularRv;
    private SmartEditText cardiovascularEt;
    private SmartTextView addCardiovascularTv;
    private CheckBox abdominalpainCb;
    private CheckBox constipationCb;
    private CheckBox diarrheaCb;
    private CheckBox bloodinthestoolCb;
    private CheckBox darkstoolCb;
    private CheckBox vomitingCb;
    private RecyclerView gastrointestinalRv;
    private SmartEditText gastrointestinalEt;
    private SmartTextView addGastrointestinalTv;
    private CheckBox bloodintheurineCb;
    private CheckBox painwithurinationCb;
    private CheckBox frequenturinationCb;
    private RecyclerView genitourinaryRv;
    private SmartEditText genitourinaryEt;
    private SmartTextView addGenitourinaryTv;
    private CheckBox coldorheatintoleranceCb;
    private CheckBox frequentthirstorhungerCb;
    private RecyclerView metabolicRv;
    private SmartEditText metabolicEt;
    private SmartTextView addMetabolicTv;
    private CheckBox depressionCb;
    private CheckBox emotionaldisturbancesCb;
    private CheckBox dizzinesCb;
    private RecyclerView neurologicRv;
    private SmartEditText neurologicEt;
    private SmartTextView addNeurologicTv;
    private CheckBox jointpainCb;
    private CheckBox weaknessCb;
    private CheckBox jointswellingCb;
    private RecyclerView musculoskeletaRv;
    private SmartEditText musculoskeletaEt;
    private SmartTextView addMusculoskeletaTv;
    private CheckBox bleedingCb;
    private CheckBox bruisingCb;
    private RecyclerView hematologicRv;
    private SmartEditText hematologicEt;
    private SmartTextView addHematologicTv;
    private CheckBox beestingallergyCb;
    private CheckBox foodallergiesCb;
    private CheckBox environmentalallergiesCb;
    private CheckBox seasonalallergiesCb;
    private RecyclerView immunologicRv;
    private SmartEditText immunologicEt;
    private SmartTextView addImmunologicTv;
    private SmartTextView submitReviewSystemTv;

    private String hasTravelHistroy;

    private AdditionalFieldsAdapter additionalFieldsAdapter1;
    private AdditionalFieldsAdapter additionalFieldsAdapter2;
    private AdditionalFieldsAdapter additionalFieldsAdapter3;
    private AdditionalFieldsAdapter additionalFieldsAdapter4;
    private AdditionalFieldsAdapter additionalFieldsAdapter5;
    private AdditionalFieldsAdapter additionalFieldsAdapter6;
    private AdditionalFieldsAdapter additionalFieldsAdapter7;
    private AdditionalFieldsAdapter additionalFieldsAdapter8;
    private AdditionalFieldsAdapter additionalFieldsAdapter9;
    private AdditionalFieldsAdapter additionalFieldsAdapter10;
    private AdditionalFieldsAdapter additionalFieldsAdapter11;

    private ArrayList<String> otherConstitutionaData = new ArrayList<>();
    private ArrayList<String> otherHeentData = new ArrayList<>();
    private ArrayList<String> otherRespiratoryData = new ArrayList<>();
    private ArrayList<String> otherCardivascularData = new ArrayList<>();
    private ArrayList<String> otherGastrointestinalData = new ArrayList<>();
    private ArrayList<String> otherGenitourinaryData = new ArrayList<>();
    private ArrayList<String> otherMetabolicData = new ArrayList<>();
    private ArrayList<String> otherNeurologicData = new ArrayList<>();
    private ArrayList<String> otherMusculoskeletaData = new ArrayList<>();
    private ArrayList<String> otherHematologicData = new ArrayList<>();
    private ArrayList<String> otherImmunologicData = new ArrayList<>();

    private SmartCaching smartCaching;

    private boolean isUpdated = false;


    @Override
    public int getLayoutID() {
        return R.layout.activity_reviews_of_systems;
    }

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Review of Systems");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        feverCb = findViewById(R.id.fever_cb);
        
        nightsweatsCb = findViewById(R.id.nightsweats_cb);
        weightlossCb = findViewById(R.id.weightloss_cb);
        fatigueCb = findViewById(R.id.fatigue_cb);
        weightgainCb = findViewById(R.id.weightgain_cb);

        constitutionalRv = findViewById(R.id.constitutional_rv);
        constitutionalEt = findViewById(R.id.constitutional_et);
        addConstitutionalTv = findViewById(R.id.add_constitutional_tv);


        headachesCb = findViewById(R.id.headaches_cb);
        visionchangeCb = findViewById(R.id.visionchange_cb);
        hearinglossCb = findViewById(R.id.hearingloss_cb);
        

        heentRv = findViewById(R.id.heent_rv);
        heentEt = findViewById(R.id.heent_et);
        addHeentTv = findViewById(R.id.add_heent_tv);


        coughCb = findViewById(R.id.cough_cb);
        wheezingCb = findViewById(R.id.wheezing_cb);
        

        respiratoryRv = findViewById(R.id.respiratory_rv);
        respiratoryEt = findViewById(R.id.respiratory_et);
        addRespiratoryTv = findViewById(R.id.add_respiratory_tv);


        chestpainCb = findViewById(R.id.chestpain_cb);
        palpitationCb = findViewById(R.id.palpitation_cb);
        irregularheartbeatCb = findViewById(R.id.irregularheartbeat_cb);
        

        cardiovascularRv = findViewById(R.id.cardiovascular_rv);
        cardiovascularEt = findViewById(R.id.cardiovascular_et);
        addCardiovascularTv = findViewById(R.id.add_cardiovascular_tv);


        abdominalpainCb = findViewById(R.id.abdominalpain_cb);
        constipationCb = findViewById(R.id.constipation_cb);
        diarrheaCb = findViewById(R.id.diarrhea_cb);
        bloodinthestoolCb = findViewById(R.id.bloodinthestool_cb);
        darkstoolCb = findViewById(R.id.darkstool_cb);
        vomitingCb = findViewById(R.id.vomiting_cb);
        

        gastrointestinalRv = findViewById(R.id.gastrointestinal_rv);
        gastrointestinalEt = findViewById(R.id.gastrointestinal_et);
        addGastrointestinalTv = findViewById(R.id.add_gastrointestinal_tv);


        bloodintheurineCb = findViewById(R.id.bloodintheurine_cb);
        painwithurinationCb = findViewById(R.id.painwithurination_cb);
        frequenturinationCb = findViewById(R.id.frequenturination_cb);
        
        genitourinaryRv = findViewById(R.id.genitourinary_rv);
        
        genitourinaryEt = findViewById(R.id.genitourinary_et);
        addGenitourinaryTv = findViewById(R.id.add_genitourinary_tv);


        coldorheatintoleranceCb = findViewById(R.id.coldorheatintolerance_cb);
        frequentthirstorhungerCb = findViewById(R.id.frequentthirstorhunger_cb);

        metabolicRv = findViewById(R.id.metabolic_rv);
        metabolicEt = findViewById(R.id.metabolic_et);
        addMetabolicTv = findViewById(R.id.add_metabolic_tv);


        depressionCb = findViewById(R.id.depression_cb);
        emotionaldisturbancesCb = findViewById(R.id.emotionaldisturbances_cb);
        dizzinesCb = findViewById(R.id.dizzines_cb);

        neurologicRv = findViewById(R.id.neurologic_rv);
        neurologicEt = findViewById(R.id.neurologic_et);
        addNeurologicTv = findViewById(R.id.add_neurologic_tv);


        jointpainCb = findViewById(R.id.jointpain_cb);
        weaknessCb = findViewById(R.id.weakness_cb);
        jointswellingCb = findViewById(R.id.jointswelling_cb);

        musculoskeletaRv = findViewById(R.id.musculoskeleta_rv);
        musculoskeletaEt = findViewById(R.id.musculoskeleta_et);
        addMusculoskeletaTv = findViewById(R.id.add_musculoskeleta_tv);


        bleedingCb = findViewById(R.id.bleeding_cb);
        bruisingCb = findViewById(R.id.bruising_cb);

        hematologicRv = findViewById(R.id.hematologic_rv);
        hematologicEt = findViewById(R.id.hematologic_et);
        addHematologicTv = findViewById(R.id.add_hematologic_tv);


        beestingallergyCb = findViewById(R.id.beestingallergy_cb);
        foodallergiesCb = findViewById(R.id.foodallergies_cb);
        environmentalallergiesCb = findViewById(R.id.environmentalallergies_cb);
        seasonalallergiesCb = findViewById(R.id.seasonalallergies_cb);

        immunologicRv = findViewById(R.id.immunologic_rv);
        immunologicEt = findViewById(R.id.immunologic_et);
        addImmunologicTv = findViewById(R.id.add_immunologic_tv);


        submitReviewSystemTv = findViewById(R.id.submit_review_system_tv);

        constitutionalRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        heentRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        respiratoryRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        cardiovascularRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        gastrointestinalRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        genitourinaryRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        metabolicRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        neurologicRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        musculoskeletaRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        hematologicRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));
        immunologicRv.setLayoutManager(new LinearLayoutManager(ReviewsOfSystems.this));

        smartCaching = new SmartCaching(this);

        additionalFieldsAdapter1 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherConstitutionaData);
        additionalFieldsAdapter2 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherHeentData);
        additionalFieldsAdapter3 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherRespiratoryData);
        additionalFieldsAdapter4 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherCardivascularData);
        additionalFieldsAdapter5 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherGastrointestinalData);
        additionalFieldsAdapter6 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherGenitourinaryData);
        additionalFieldsAdapter7 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherMetabolicData);
        additionalFieldsAdapter8 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherNeurologicData);
        additionalFieldsAdapter9 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherMusculoskeletaData);
        additionalFieldsAdapter10 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherHematologicData);
        additionalFieldsAdapter11 = new AdditionalFieldsAdapter(ReviewsOfSystems.this, otherImmunologicData);

        constitutionalRv.setAdapter(additionalFieldsAdapter1);
        heentRv.setAdapter(additionalFieldsAdapter2);
        respiratoryRv.setAdapter(additionalFieldsAdapter3);
        cardiovascularRv.setAdapter(additionalFieldsAdapter4);
        gastrointestinalRv.setAdapter(additionalFieldsAdapter5);
        genitourinaryRv.setAdapter(additionalFieldsAdapter6);
        metabolicRv.setAdapter(additionalFieldsAdapter7);
        neurologicRv.setAdapter(additionalFieldsAdapter8);
        musculoskeletaRv.setAdapter(additionalFieldsAdapter9);
        hematologicRv.setAdapter(additionalFieldsAdapter10);
        immunologicRv.setAdapter(additionalFieldsAdapter11);

        getDataFromDataBase();

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addConstitutionalTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(constitutionalEt.getText().toString().trim())) {
                    additionalFieldsAdapter1.addAdditionalFields(constitutionalEt.getText().toString().trim());
                    constitutionalEt.setText("");
                }
            }
        });
        addHeentTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(heentEt.getText().toString().trim())) {
                    additionalFieldsAdapter2.addAdditionalFields(heentEt.getText().toString().trim());
                    heentEt.setText("");
                }
            }
        });
        addRespiratoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(respiratoryEt.getText().toString().trim())) {
                    additionalFieldsAdapter3.addAdditionalFields(respiratoryEt.getText().toString().trim());
                    respiratoryEt.setText("");
                }
            }
        });
        addCardiovascularTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(cardiovascularEt.getText().toString().trim())) {
                    additionalFieldsAdapter4.addAdditionalFields(cardiovascularEt.getText().toString().trim());
                    cardiovascularEt.setText("");
                }
            }
        });
        addGastrointestinalTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(gastrointestinalEt.getText().toString().trim())) {
                    additionalFieldsAdapter5.addAdditionalFields(gastrointestinalEt.getText().toString().trim());
                    gastrointestinalEt.setText("");
                }
            }
        });
        addGenitourinaryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(genitourinaryEt.getText().toString().trim())) {
                    additionalFieldsAdapter6.addAdditionalFields(genitourinaryEt.getText().toString().trim());
                    genitourinaryEt.setText("");
                }
            }
        });
        addMetabolicTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(metabolicEt.getText().toString().trim())) {
                    additionalFieldsAdapter7.addAdditionalFields(metabolicEt.getText().toString().trim());
                    metabolicEt.setText("");
                }
            }
        });
        addNeurologicTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(neurologicEt.getText().toString().trim())) {
                    additionalFieldsAdapter8.addAdditionalFields(neurologicEt.getText().toString().trim());
                    neurologicEt.setText("");
                }
            }
        });
        addMusculoskeletaTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(musculoskeletaEt.getText().toString().trim())) {
                    additionalFieldsAdapter9.addAdditionalFields(musculoskeletaEt.getText().toString().trim());
                    musculoskeletaEt.setText("");
                }
            }
        });
        addHematologicTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(hematologicEt.getText().toString().trim())) {
                    additionalFieldsAdapter10.addAdditionalFields(hematologicEt.getText().toString().trim());
                    hematologicEt.setText("");
                }
            }
        });
        addImmunologicTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(immunologicEt.getText().toString().trim())) {
                    additionalFieldsAdapter11.addAdditionalFields(immunologicEt.getText().toString().trim());
                    immunologicEt.setText("");
                }
            }
        });

        submitReviewSystemTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                putDataIntoDatabaseAndSync(viewToJson());
            }
        });


    }


   
    /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getReviewsOfSystems() {
        SmartUtils.showLoadingDialog(ReviewsOfSystems.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetReviewofSystem");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, ReviewsOfSystems.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_REVIEW_SYSTEMS,
                                OTHER_CONSTITUTIONAL,
                                OTHER_HEENT,
                                OTHER_RESPIRATORY,
                                OTHER_CARDIOVASCULAR,
                                OTHER_GASTROINTESTINAL,
                                OTHER_GENITOURINARY,
                                OTHER_METABOLIC,
                                OTHER_NEUROLOGIC,
                                OTHER_MUSCULOSKELETA,
                                OTHER_HEMATOLOGIC,
                                OTHER_IMMUNOLOGIC);
                        String query = "SELECT * FROM " + TABLE_REVIEW_SYSTEMS + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_REVIEW_SYSTEMS, query).get(0);
                        Log.d("@@@ReviewsOfSystems", dataFromDatabase.getAsString(OTHER_CONSTITUTIONAL));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateReviewsOfSystems(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(ReviewsOfSystems.this);


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdateReviewOfSystem");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, ReviewsOfSystems.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    smartCaching.updateSyncStatus(TABLE_REVIEW_SYSTEMS, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(ReviewsOfSystems.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(ReviewsOfSystems.this, Home.class));
        }


        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }

        /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_REVIEW_SYSTEMS + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_REVIEW_SYSTEMS, query) != null && smartCaching.getDataFromCache(TABLE_REVIEW_SYSTEMS, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_REVIEW_SYSTEMS, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_REVIEW_SYSTEMS, getUserId())) {
                getReviewsOfSystems();
            } else {
                updateReviewsOfSystems(viewToJson(), false);
            }
        } else {
            getReviewsOfSystems();
        }
    }

    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
//            Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(OTHER_CONSTITUTIONAL, additionalFieldsAdapter1.getAdditionalFields());
            params.put(OTHER_HEENT, additionalFieldsAdapter2.getAdditionalFields());
            params.put(OTHER_RESPIRATORY, additionalFieldsAdapter3.getAdditionalFields());
            params.put(OTHER_CARDIOVASCULAR, additionalFieldsAdapter4.getAdditionalFields());
            params.put(OTHER_GASTROINTESTINAL, additionalFieldsAdapter5.getAdditionalFields());
            params.put(OTHER_GENITOURINARY, additionalFieldsAdapter6.getAdditionalFields());
            params.put(OTHER_METABOLIC, additionalFieldsAdapter7.getAdditionalFields());
            params.put(OTHER_NEUROLOGIC, additionalFieldsAdapter8.getAdditionalFields());
            params.put(OTHER_MUSCULOSKELETA, additionalFieldsAdapter9.getAdditionalFields());
            params.put(OTHER_HEMATOLOGIC, additionalFieldsAdapter10.getAdditionalFields());
            params.put(OTHER_IMMUNOLOGIC, additionalFieldsAdapter11.getAdditionalFields());

            params.put(FEVER, feverCb.isChecked());
            params.put(FATIGUE, fatigueCb.isChecked());
            params.put(NIGHT_SWEATS, nightsweatsCb.isChecked());
            params.put(WEIGHT_GAIN, weightgainCb.isChecked());
            params.put(WEIGHT_LOSS, weightlossCb.isChecked());
            params.put(HEADACHES, headachesCb.isChecked());
            params.put(HEARING_LOSS, hearinglossCb.isChecked());
            params.put(VISION_CHANGE, visionchangeCb.isChecked());
            params.put(COUGH, coughCb.isChecked());
            params.put(WHEEZING, wheezingCb.isChecked());
            params.put(CHEST_PAIN, chestpainCb.isChecked());
            params.put(IRREGULAR_HEAR_BEAT, irregularheartbeatCb.isChecked());
            params.put(PALPITATION, palpitationCb.isChecked());
            params.put(ABDONINAL_PAIN, abdominalpainCb.isChecked());
            params.put(BLOOD_IN_STOOL, bloodinthestoolCb.isChecked());
            params.put(CONSTIPATION, constipationCb.isChecked());
            params.put(DARK_STOOL, darkstoolCb.isChecked());
            params.put(DIARRHEA, diarrheaCb.isChecked());
            params.put(VOMITING, vomitingCb.isChecked());
            params.put(BLOOD_IN_URINE, bloodintheurineCb.isChecked());
            params.put(FREQUENT_URINATION, frequenturinationCb.isChecked());
            params.put(PAIN_WITH_URINATION, painwithurinationCb.isChecked());
            params.put(COLD_OR_HEAT_INTOLERANCE, coldorheatintoleranceCb.isChecked());
            params.put(FREQUENT_THIRST_HUNGER, frequentthirstorhungerCb.isChecked());
            params.put(DEPRESSION, depressionCb.isChecked());
            params.put(DIZZINESS, dizzinesCb.isChecked());
            params.put(EMOTIONAL_DISTURBANCES, emotionaldisturbancesCb.isChecked());
            params.put(JOINT_PAIN, jointpainCb.isChecked());
            params.put(JOINT_SWELLING, jointswellingCb.isChecked());
            params.put(WEEKNESS, weaknessCb.isChecked());
            params.put(BLEEDING, bleedingCb.isChecked());
            params.put(BRUSING, bruisingCb.isChecked());
            params.put(BEE_STRING_ALLERGY, beestingallergyCb.isChecked());
            params.put(ENVIRONMENTAL_ALLERGIES, environmentalallergiesCb.isChecked());
            params.put(FOOD_ALLERGIES, foodallergiesCb.isChecked());
            params.put(SEASONAL_ALLERGIES, seasonalallergiesCb.isChecked());

            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_REVIEW_SYSTEMS,
                OTHER_CONSTITUTIONAL,
                OTHER_HEENT,
                OTHER_RESPIRATORY,
                OTHER_CARDIOVASCULAR,
                OTHER_GASTROINTESTINAL,
                OTHER_GENITOURINARY,
                OTHER_METABOLIC,
                OTHER_NEUROLOGIC,
                OTHER_MUSCULOSKELETA,
                OTHER_HEMATOLOGIC,
                OTHER_IMMUNOLOGIC);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_REVIEW_SYSTEMS, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_REVIEW_SYSTEMS, getUserId())) {
            updateReviewsOfSystems(data, true);
        }
    }


    private void setData(ContentValues data) {
        otherConstitutionaData.clear();
        otherHeentData.clear();
        otherRespiratoryData.clear();
        otherCardivascularData.clear();
        otherGastrointestinalData.clear();
        otherGenitourinaryData.clear();
        otherMetabolicData.clear();
        otherNeurologicData.clear();
        otherMusculoskeletaData.clear();
        otherHematologicData.clear();
        otherImmunologicData.clear();

        feverCb.setChecked(Boolean.parseBoolean(data.getAsString(FEVER)));
        fatigueCb.setChecked(Boolean.parseBoolean(data.getAsString(FATIGUE)));
        nightsweatsCb.setChecked(Boolean.parseBoolean(data.getAsString(NIGHT_SWEATS)));
        weightgainCb.setChecked(Boolean.parseBoolean(data.getAsString(WEIGHT_GAIN)));
        weightlossCb.setChecked(Boolean.parseBoolean(data.getAsString(WEIGHT_LOSS)));
        headachesCb.setChecked(Boolean.parseBoolean(data.getAsString(HEADACHES)));
        hearinglossCb.setChecked(Boolean.parseBoolean(data.getAsString(HEARING_LOSS)));
        visionchangeCb.setChecked(Boolean.parseBoolean(data.getAsString(VISION_CHANGE)));
        coughCb.setChecked(Boolean.parseBoolean(data.getAsString(COUGH)));
        wheezingCb.setChecked(Boolean.parseBoolean(data.getAsString(WHEEZING)));
        chestpainCb.setChecked(Boolean.parseBoolean(data.getAsString(CHEST_PAIN)));
        irregularheartbeatCb.setChecked(Boolean.parseBoolean(data.getAsString(IRREGULAR_HEAR_BEAT)));
        palpitationCb.setChecked(Boolean.parseBoolean(data.getAsString(PALPITATION)));
        abdominalpainCb.setChecked(Boolean.parseBoolean(data.getAsString(ABDONINAL_PAIN)));
        bloodinthestoolCb.setChecked(Boolean.parseBoolean(data.getAsString(BLOOD_IN_STOOL)));
        constipationCb.setChecked(Boolean.parseBoolean(data.getAsString(CONSTIPATION)));
        darkstoolCb.setChecked(Boolean.parseBoolean(data.getAsString(DARK_STOOL)));
        diarrheaCb.setChecked(Boolean.parseBoolean(data.getAsString(DIARRHEA)));
        vomitingCb.setChecked(Boolean.parseBoolean(data.getAsString(VOMITING)));
        bloodintheurineCb.setChecked(Boolean.parseBoolean(data.getAsString(BLOOD_IN_URINE)));
        frequenturinationCb.setChecked(Boolean.parseBoolean(data.getAsString(FREQUENT_URINATION)));
        painwithurinationCb.setChecked(Boolean.parseBoolean(data.getAsString(PAIN_WITH_URINATION)));
        coldorheatintoleranceCb.setChecked(Boolean.parseBoolean(data.getAsString(COLD_OR_HEAT_INTOLERANCE)));
        frequenturinationCb.setChecked(Boolean.parseBoolean(data.getAsString(FREQUENT_THIRST_HUNGER)));
        depressionCb.setChecked(Boolean.parseBoolean(data.getAsString(DEPRESSION)));
        dizzinesCb.setChecked(Boolean.parseBoolean(data.getAsString(DIZZINESS)));
        emotionaldisturbancesCb.setChecked(Boolean.parseBoolean(data.getAsString(EMOTIONAL_DISTURBANCES)));
        jointpainCb.setChecked(Boolean.parseBoolean(data.getAsString(JOINT_PAIN)));
        jointswellingCb.setChecked(Boolean.parseBoolean(data.getAsString(JOINT_SWELLING)));
        weaknessCb.setChecked(Boolean.parseBoolean(data.getAsString(WEEKNESS)));
        bleedingCb.setChecked(Boolean.parseBoolean(data.getAsString(BLEEDING)));
        bruisingCb.setChecked(Boolean.parseBoolean(data.getAsString(BRUSING)));
        beestingallergyCb.setChecked(Boolean.parseBoolean(data.getAsString(BEE_STRING_ALLERGY)));
        environmentalallergiesCb.setChecked(Boolean.parseBoolean(data.getAsString(ENVIRONMENTAL_ALLERGIES)));
        foodallergiesCb.setChecked(Boolean.parseBoolean(data.getAsString(FOOD_ALLERGIES)));
        seasonalallergiesCb.setChecked(Boolean.parseBoolean(data.getAsString(SEASONAL_ALLERGIES)));


        try {
            JSONArray otherConstitutionaJson = new JSONArray(data.getAsString(OTHER_CONSTITUTIONAL));
            JSONArray otherHeentJson = new JSONArray(data.getAsString(OTHER_HEENT));
            JSONArray otherRespiratoryJson = new JSONArray(data.getAsString(OTHER_RESPIRATORY));
            JSONArray otherCardivascularJson = new JSONArray(data.getAsString(OTHER_CARDIOVASCULAR));
            JSONArray otherGastrointestinalJson = new JSONArray(data.getAsString(OTHER_GASTROINTESTINAL));
            JSONArray otherGenitourinaryJson = new JSONArray(data.getAsString(OTHER_GENITOURINARY));
            JSONArray otherMetabolicJson = new JSONArray(data.getAsString(OTHER_METABOLIC));
            JSONArray otherNeurologicJson = new JSONArray(data.getAsString(OTHER_NEUROLOGIC));
            JSONArray otherMusculoskeletaJson = new JSONArray(data.getAsString(OTHER_MUSCULOSKELETA));
            JSONArray otherHematologicJson = new JSONArray(data.getAsString(OTHER_HEMATOLOGIC));
            JSONArray otherImmunologicJson = new JSONArray(data.getAsString(OTHER_IMMUNOLOGIC));

            for (int i = 0; i < otherConstitutionaJson.length(); i++) {
                otherConstitutionaData.add(otherConstitutionaJson.getString(i));
            }
            for (int i = 0; i < otherHeentJson.length(); i++) {
                otherHeentData.add(otherHeentJson.getString(i));
            }
            for (int i = 0; i < otherRespiratoryJson.length(); i++) {
                otherRespiratoryData.add(otherRespiratoryJson.getString(i));
            }
            for (int i = 0; i < otherCardivascularJson.length(); i++) {
                otherCardivascularData.add(otherCardivascularJson.getString(i));
            }
            for (int i = 0; i < otherGastrointestinalJson.length(); i++) {
                otherGastrointestinalData.add(otherGastrointestinalJson.getString(i));
            }
            for (int i = 0; i < otherGenitourinaryJson.length(); i++) {
                otherGenitourinaryData.add(otherGenitourinaryJson.getString(i));
            }
            for (int i = 0; i < otherMetabolicJson.length(); i++) {
                otherMetabolicData.add(otherMetabolicJson.getString(i));
            }
            for (int i = 0; i < otherNeurologicJson.length(); i++) {
                otherNeurologicData.add(otherNeurologicJson.getString(i));
            }
            for (int i = 0; i < otherMusculoskeletaJson.length(); i++) {
                otherMusculoskeletaData.add(otherMusculoskeletaJson.getString(i));
            }
            for (int i = 0; i < otherHematologicJson.length(); i++) {
                otherHematologicData.add(otherHematologicJson.getString(i));
            }
            for (int i = 0; i < otherImmunologicJson.length(); i++) {
                otherImmunologicData.add(otherImmunologicJson.getString(i));
            }

            additionalFieldsAdapter1.notifyDataSetChanged();
            additionalFieldsAdapter2.notifyDataSetChanged();
            additionalFieldsAdapter3.notifyDataSetChanged();
            additionalFieldsAdapter4.notifyDataSetChanged();
            additionalFieldsAdapter5.notifyDataSetChanged();
            additionalFieldsAdapter6.notifyDataSetChanged();
            additionalFieldsAdapter7.notifyDataSetChanged();
            additionalFieldsAdapter8.notifyDataSetChanged();
            additionalFieldsAdapter9.notifyDataSetChanged();
            additionalFieldsAdapter10.notifyDataSetChanged();
            additionalFieldsAdapter11.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
