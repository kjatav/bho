package com.bho.bho.Activities.LandingScreens;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.util.Log;
import android.view.View;

import com.bho.R;
import com.bho.bho.LandingBaseActivity;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class AboutUsLanding extends LandingBaseActivity {

    SmartTextView termsTv;

    @Override
    public int getDrawerLayoutID() {
        if (getIntent().getBooleanExtra(FROM_NAV, true)) {
            drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));
            return 0;
        } else {
            return super.getDrawerLayoutID();
        }
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_terms_and_conditions;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("About Us");
        termsTv = findViewById(R.id.terms_tv);
        getAboutUs();
    }

    private void getAboutUs() {
        SmartUtils.showLoadingDialog(AboutUsLanding.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "CMS");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, AboutUsLanding.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        termsTv.setText(Html.fromHtml(jsonObject.getString("about")));


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
