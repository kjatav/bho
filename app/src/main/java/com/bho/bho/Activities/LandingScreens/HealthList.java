package com.bho.bho.Activities.LandingScreens;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;

import com.bho.R;
import com.bho.bho.Adapters.HealthListAdapterMain;
import com.bho.bho.LandingBaseActivity;
import com.bho.bho.POJO.HealthListObject;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class HealthList extends LandingBaseActivity {

    private RecyclerView healthProfessionalRv;
    private HealthListAdapterMain healthListAdapterMain;
    public boolean isConsumer;

    @Override
    public int getLayoutID() {
        return R.layout.activity_health_professionals;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        if (getIntent().getBooleanExtra(IS_CONSUMER, true)) {
            setHeaderToolbar("Healthcare Consumers");
            isConsumer = true;
        } else {
            setHeaderToolbar("Health Professionals");
            isConsumer = false;
        }
        healthProfessionalRv = findViewById(R.id.health_professional_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(HealthList.this);
        healthProfessionalRv.setLayoutManager(linearLayoutManager);

        getHealthList();
    }

    private void getHealthList() {
        SmartUtils.showLoadingDialog(HealthList.this);
        final JSONObject params = new JSONObject();
        try {
            if (getIntent().getBooleanExtra(IS_CONSUMER, true)) {
                params.put(PAGENAME, "Healthcare Consumers");
            } else {
                params.put(PAGENAME, "Health Professionals");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "HealthCareList");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, HealthList.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        JSONArray healthListJson = response.getJSONArray(RESULTS);
                        ArrayList<HealthListObject> healthListObjects = new ArrayList<>();
                        for (int i = 0; i < healthListJson.length(); i++) {
                            HealthListObject healthListObject = new HealthListObject(HealthList.this, false, healthListJson.getJSONObject(i).getString("SubjectName"), healthListJson.getJSONObject(i).getJSONArray("ContentSubjectList"));
                            healthListObjects.add(healthListObject);
                        }
                        healthListAdapterMain = new HealthListAdapterMain(HealthList.this, healthListObjects, healthProfessionalRv, isConsumer);
                        healthProfessionalRv.setAdapter(healthListAdapterMain);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(HealthList.this).addToRequestQueue(requestParams, false);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
