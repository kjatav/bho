package com.bho.bho.Activities.LandingScreens;

import android.support.v4.widget.NestedScrollView;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.bho.R;
import com.bho.bho.LandingBaseActivity;
import com.bho.smart.customViews.SmartTextView;

public class OurTeam extends LandingBaseActivity {

    private NestedScrollView descriptionScroll;
    private SmartTextView topTextTv;
    private LinearLayout serviceTabsLl;
    private LinearLayout team1Ll;
    private SmartTextView team1NameTv;
    private SmartTextView team1RoleTv;
    private LinearLayout team2Ll;
    private SmartTextView team2NameTv;
    private SmartTextView team2RoleTv;
    private LinearLayout team3Ll;
    private SmartTextView team3NameTv;
    private SmartTextView team3RoleTv;
    private LinearLayout team4Ll;
    private SmartTextView team4NameTv;
    private SmartTextView team4RoleTv;
    private SmartTextView backgroundTextTv;
    private SmartTextView experienceTextTv;


    @Override
    public int getLayoutID() {
        return R.layout.activity_our_team;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Our Team");

        descriptionScroll = findViewById(R.id.description_scroll);
        topTextTv = findViewById(R.id.top_text_tv);
        serviceTabsLl = findViewById(R.id.service_tabs_ll);
        team1Ll = findViewById(R.id.team_1_ll);
        team1NameTv = findViewById(R.id.team_1_name_tv);
        team1RoleTv = findViewById(R.id.team_1_role_tv);
        team2Ll = findViewById(R.id.team_2_ll);
        team2NameTv = findViewById(R.id.team_2_name_tv);
        team2RoleTv = findViewById(R.id.team_2_role_tv);
        team3Ll = findViewById(R.id.team_3_ll);
        team3NameTv = findViewById(R.id.team_3_name_tv);
        team3RoleTv = findViewById(R.id.team_3_role_tv);
        team4Ll = findViewById(R.id.team_4_ll);
        team4NameTv = findViewById(R.id.team_4_name_tv);
        team4RoleTv = findViewById(R.id.team_4_role_tv);
        backgroundTextTv = findViewById(R.id.background_text_tv);
        experienceTextTv = findViewById(R.id.experience_text_tv);

        topTextTv.setText("We value your trust above all else and we are proud to introduce you to BHO’s team of experts: the people who ensure that you receive the most professional online medical advice possible, and that everything BHO publishes adheres to the highest standards for accuracy, integrity, and balance. We always have your best interests at heart and we will work very hard all the time to provide you with highly relevant information that is accessible, actionable, and authoritative.");

        setTab(team1Ll, team1NameTv, team1RoleTv,
                "Dr. Okoronkwo Ogan obtained his medical degree from College of Medicine, University of Nigeria, Enugu Campus in 1982. He completed three years of Internal Medicine Residency Training at Howard University Hospital in Washington DC, where he received the Daniel Hale Williams award for being the hospital-wide outstanding post-graduate year one trainee in 1989. He was Chief Resident of the Internal Medicine Residency Training Program in his third year of training.",
                "After graduating from the Internal Medicine program in 1991, Dr. Ok Ogan began an Anesthesiology Residency Training program at the Medical College of Georgia in Augusta, Georgia. The Anesthesiology Residency Training program was completed in 1993. Dr. Ogan was certified by the American Board of Internal Medicine in 1991, and by the American Board of Anesthesiology in 1994.\n" +
                        "\n" +
                        "Dr. Ok Ogan completed a Fellowship Training program in Critical Care Medicine at the Mayo Clinic in Rochester, Minnesota, and is also Board Certified in Critical Care Medicine.\n" +
                        "\n" +
                        "Dr. Ok Ogan is currently an Assistant Professor of Anesthesiology and Critical Care Medicine at Duke University Medical Center in Durham, North Carolina.");
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();


        team1Ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(team1Ll, team1NameTv, team1RoleTv,
                        "Dr. Okoronkwo Ogan obtained his medical degree from College of Medicine, University of Nigeria, Enugu Campus in 1982. He completed three years of Internal Medicine Residency Training at Howard University Hospital in Washington DC, where he received the Daniel Hale Williams award for being the hospital-wide outstanding post-graduate year one trainee in 1989. He was Chief Resident of the Internal Medicine Residency Training Program in his third year of training.",
                        "After graduating from the Internal Medicine program in 1991, Dr. Ok Ogan began an Anesthesiology Residency Training program at the Medical College of Georgia in Augusta, Georgia. The Anesthesiology Residency Training program was completed in 1993. Dr. Ogan was certified by the American Board of Internal Medicine in 1991, and by the American Board of Anesthesiology in 1994.\n" +
                                "\n" +
                                "Dr. Ok Ogan completed a Fellowship Training program in Critical Care Medicine at the Mayo Clinic in Rochester, Minnesota, and is also Board Certified in Critical Care Medicine.\n" +
                                "\n" +
                                "Dr. Ok Ogan is currently an Assistant Professor of Anesthesiology and Critical Care Medicine at Duke University Medical Center in Durham, North Carolina.");
            }
        });

        team2Ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(team2Ll, team2NameTv, team2RoleTv,
                        "Dr. Onuigbo is an Associate Professor of Medicine, The Robert Larner, M.D. College of Medicine, University of Vermont, Burlington, VT, USA. He is a retired Associate Professor of Medicine, College of Medicine, Mayo Clinic, Rochester, USA, and formerly a Consultant Nephrologist/Hypertension Specialist/Transplant Physician, Mayo Clinic Health System, Eau Claire, WI, USA. He is American Board of Internal Medicine (ABIM) board certified in Internal Medicine and Nephrology, a Fellow of the American Society of Nephrology (FASN), a Mayo Clinic MacMillan Scholar (2009-2011), a Mayo SOAR Research Grant Recipient, and a Fellow of the West African College of Physicians (FWACP, Nephrology, 1989). Before leaving Nigeria in 1994, Dr. Onuigbo had served as a Consultant Physician/Attending Nephrologist at the University of Nigeria Teaching Hospital (1989-1994) and a Senior Lecturer, Department of Medicine, College of Medicine, University of Nigeria, Enugu Campus, Nigeria, 1991-1994. Dr. Onuigbo had a brief stint as Honorary Renal Registrar, Renal Unit, Queen Elizabeth Hospital, in 1988 as a British Commonwealth Scholar. In medical school, at the College of Medicine, University of Nigeria, Enugu Campus, Enugu, Nigeria, Dr. Onuigbo was the best graduating student and scored distinctions in anatomy, physiology, medical biochemistry at the 2nd MB examinations, the best graduating student and scored distinctions in pharmacology and pathology at the 3rd MB examinations. He was the best graduating student of the 1981 class with the best results in the final MBBS examinations in Medicine and Surgery. Dr. Onuigbo has completed the following three Nephrology Fellowships: The West African College of Physicians Fellowship in Clinical Nephrology at the University of Nigeria Teaching Hospital, Enugu, Nigeria (1989), a Research Nephrology Fellowship at the University of Texas Health Science Center, Houston, TX under the tutelage of Professor Thomas DuBose (1996), and a Clinical Nephrology Fellowship with emphasis on Transplantation Medicine at the University of Maryland Medical School, Baltimore, MD (2002).",
                        "Dr. Onuigbo has over 120 peer-reviewed journal publications including Editorials, over 150 published conference abstracts, 18 book chapters and has published 3 specialty nephrology books. He has introduced several new concepts into the modern medicine/nephrology literature including the “vanishing vertebra syndrome” in sickle cell disease (1990) and in Pott’s disease (1993), respectively, Cytomegalovirus-induced glomerulopathy in renal allografts (2002), the syndrome of late onset renal failure from angiotensin blockade (LORFFAB; 2005), the concept of Renoprevention (2009), the syndrome of rapid onset end-stage renal disease (SORO-ESRD; 2010), “Physician Cognitive Drift” from the electronic medical record (2012), “Ethicomedicinomics” (2012), and “Quadruple Whammy” (2013). Dr. Onuigbo recently edited a major reference textbook on ACE Inhibitors in December 2013 that included 36 chapters, published in two volumes, drawing top world experts as contributors from the USA, South America, Europe including the United Kingdom, Asia including New Zealand, and Africa. Dr. Onuigbo’s major research and other awards include the Mayo Clinic/Mayo Health System MacMillan Management Scholarship Award (2009-2011) and the Mayo Clinic/Mayo Health System System-Oriented-Application-Research (SOAR) Grant (2010-2011). Most recently, Dr. Onuigbo received the Mayo Clinic Division of Nephrology & Hypertension Integration Award for 2014.\n" +
                                "\n" +
                                "Dr. Onuigbo has served on the editorial boards of many nephrology specialty journals including the Quarterly Journal of Medicine and the International Journal of Clinical Practice, and serves as a reviewer for several general medicine, specialty and multi-specialty journals.\n" +
                                "\n" +
                                "Furthermore, Dr. Onuigbo completed an MBA with Healthcare Emphasis in June 2012 from the University of Wisconsin Consortium. Dr. Onuigbo was awarded the 2012 University of Wisconsin MBA Consortium Most Outstanding MBA Graduate. He has completed recent published works in healthcare economics, medical bioinformatics and healthcare IT, non-dialytic therapy for ESRD patients, renoprevention, cost-reduction paradigms in healthcare, end-of-life care and other related healthcare fields.");
            }
        });

        team3Ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(team3Ll, team3NameTv, team3RoleTv,
                        "Dr Okoye obtained his medical degree from the University of Nigeria Teaching Hospital, Enugu campus, in 1982. He completed a three-year Family Medicine residency training in 1990 at Howard University Hospital in Washington D.C.; where he was the recipient of the \"Outstanding Third Year Award in Family Medicine\". His first year In-training examination score was the highest all three years combined. In 1991, he became certified by the American Board of Family Medicine. Subsequently, he completed a one year Physician Executive Fellowship (an Advanced Healthcare Business Management Course) at the University of North Carolina, Chapel Hill.",
                        "In 1993, he completed his residency training in Anesthesiology at the Medical College of Georgia in Augusta. In his final year, he received the \"Chairman's Accolade for Outstanding Performance in Anesthesiology as well as scoring in the ninety-nine percentile in the nationwide in training examination\". In addition, he is board certified in Anesthesiology as well as Emergency Medicine.\n" +
                                "\n" +
                                "Dr Okoye was a Clinical Instructor of Internal Medicine for one year at Morehouse School of Medicine, Atlanta. He also did work on Hypertension and Biofeedback at the Columbia University College of Physicians and Surgeons, Harlem Hospital Center, New York.\n" +
                                "\n" +
                                "Dr Okoye is currently in private practice as well as an Assistant Clinical Professor of Emergency Medicine, Philadelphia College of Osteopathic Medicine, Georgia Campus.");
            }
        });

        team4Ll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(team4Ll, team4NameTv, team4RoleTv,
                        "Dr. Charles Okorie is the Founder and Medical Director of two acute care centers in the US, Fair Lakes Urgent Care Center, located in Fairfax, Virginia, and VA Gateway Urgent Care Center, located in Gainesville, Virginia. These two centers were recently acquired in January 2015 by American CareSource, a publicly traded company. Dr. Okorie has been retained by the company as a Medical Director and is expected to play an active role in the Company's strategy to become the high-quality, patient-centric provider of convenient, affordable urgent care in the northern Virginia market. Dr. Okorie is also on the faculty of the world renowned George Washington University School of Medicine, in Washington DC, USA, as a Clinical Instructor of Medicine.",
                        "Dr. Okorie has been a certified diplomate of the American Board of Internal Medicine since September 1992. Dr. Okorie graduated from the University of Nigeria Teaching Hospital, Enugu, Nigeria, in 1986. In medical school, at the College of Medicine, University of Nigeria, Enugu Campus, Enugu, Nigeria, Dr. Okorie scored distinction in Medical Biochemistry at the 2nd MB examinations and also won Professor Onuaguluchi's prize for outstanding performance in Medical Pharmacology at the 3rd MB examinations. He completed his rotating internship at the University of Port Harcourt Teaching Hospital between 1986-1987. Thereafter, he worked as a staff physician at the Mobil Medical Clinic, Eket, Nigeria, between 1987-1988. In the summer of 1988, he left Nigeria for the US to pursue post-graduate medical education.\n" +
                                "\n" +
                                "Dr. Okorie completed Internal Medicine residency in 1992, at Howard University Hospital, Washington, D.C., where he also served as Chief Resident of Internal Medicine. He subsequently was accepted at the world renowned Critical Care Training Institute, University of Pittsburgh Medical Center where he did one year of clinical fellowship in Critical Care Medicine. Following this training, Dr. Okorie practiced Critical Care Medicine at the Medical/Surgical ICU, Doctors Community Hospital, Lanham, MD (1991-1999) and Cardiothoracic ICU, Washington Hospital, Washington PA (1993-1995). Not losing sight of his overarching mission in life, which has been to bring world-class medical care to Nigeria, he has since 1995 devoted his time to learning how to develop, operate and successfully run urgent care centers in the US.\n" +
                                "\n" +
                                "Dr. Okorie is also a designated Senior Aviation Medical Examiner (AME) by the United States Federal Aviation Authority (FAA). This designation followed his successful completion of the Aviation Medicine Course and subsequent certification by the Civil AeroSpace Medical Institute of the FAA. With this certification and designation, he joins a handful of physicians in the US who are authorized to perform medical examinations for pilots and air traffic controllers as required by the FAA. Dr. Okorie is a designated Civil Surgeon for the U.S. Department of Justice - Bureau of Citizenship and Immigration Services (BCIS). By this designation, he joins the list of BCIS approved licensed physicians who are authorized to perform medical examinations for aliens applying for Permanent Resident Status (Green Card).");
            }
        });

    }

    private void setTab(LinearLayout memberLinearLayout, SmartTextView nameTextView, SmartTextView roleTextView, String memberBackground, String memberExperience) {

        descriptionScroll.fullScroll(View.SCROLL_INDICATOR_TOP);
        descriptionScroll.scrollTo(0,0);

        team1NameTv.setTextColor(getResources().getColor(R.color.black));
        team2NameTv.setTextColor(getResources().getColor(R.color.black));
        team3NameTv.setTextColor(getResources().getColor(R.color.black));
        team4NameTv.setTextColor(getResources().getColor(R.color.black));

        team1RoleTv.setTextColor(getResources().getColor(R.color.black));
        team2RoleTv.setTextColor(getResources().getColor(R.color.black));
        team3RoleTv.setTextColor(getResources().getColor(R.color.black));
        team4RoleTv.setTextColor(getResources().getColor(R.color.black));

        team1Ll.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        team2Ll.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        team3Ll.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        team4Ll.setBackground(getResources().getDrawable(R.drawable.trasparent_background));

        nameTextView.setTextColor(getResources().getColor(R.color.white));
        roleTextView.setTextColor(getResources().getColor(R.color.white));
        memberLinearLayout.setBackground(getResources().getDrawable(R.drawable.bg_blue_tab));

        backgroundTextTv.setText(memberBackground);
        experienceTextTv.setText(memberExperience);

    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
