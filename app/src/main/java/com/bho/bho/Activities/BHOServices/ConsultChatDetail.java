package com.bho.bho.Activities.BHOServices;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bho.R;
import com.bho.bho.Activities.SideMenuOptions.Testimonial.AddTestimonial;
import com.bho.bho.MasterActivity;
import com.bho.smart.common.Object_Image;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import static com.bho.smart.framework.SmartUtils.focusEditTextRed;
import static com.bho.smart.framework.SmartUtils.getFileNameFromUrlSmart;
import static com.bho.smart.framework.SmartUtils.getImageFileUri;
import static com.bho.smart.framework.SmartUtils.getUserId;
import static com.bho.smart.framework.SmartUtils.hideSoftKeyboard;
import static com.bho.smart.framework.SmartUtils.showSoftKeyboard;

public class ConsultChatDetail extends MasterActivity {

    JSONArray chatModelJson = new JSONArray();
    QuickConsultDetailAdapter quickConsultDetailAdapter;
    FilesConsultDetailAdapter filesConsultDetailAdapter;

    private RelativeLayout consultChatRl;
    private View layoutNoChats;
    private RecyclerView chatRv;
    private LinearLayout consultChatLl;
    private SmartEditText subjectConsultChatEt;
    private SmartEditText descConsultChatEt;
    private ImageView fileConsultChatEt;
    private ImageView sendConsultChatEt;

    private ArrayList<Image> imageFiles = new ArrayList<>();


    private RecyclerView filesConsultChatRv;

    private Uri currentImageUri;
    private String cameraImageName;

    private boolean isQcConsult = false;
    private boolean isResponded = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return (R.layout.activity_quick_consult_detail);
    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void initComponents() {
        super.initComponents();

        isQcConsult = getIntent().getStringExtra(CONSULT_TYPE).equalsIgnoreCase(QC);
        if (isQcConsult) {
            setHeaderToolbar("Quick Consult Detail");
        } else {
            setHeaderToolbar("Expanded Consult Detail");
        }


        consultChatRl = findViewById(R.id.consult_chat_rl);
        layoutNoChats = findViewById(R.id.layout_no_chats);
        chatRv = findViewById(R.id.chat_rv);
        consultChatLl = findViewById(R.id.consult_chat_ll);
        subjectConsultChatEt = findViewById(R.id.subject_consult_chat_et);
        descConsultChatEt = findViewById(R.id.desc_consult_chat_et);
        fileConsultChatEt = findViewById(R.id.file_consult_chat_et);
        sendConsultChatEt = findViewById(R.id.send_consult_chat_et);

        filesConsultChatRv = findViewById(R.id.files_consult_chat_rv);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 2);
        filesConsultChatRv.setLayoutManager(gridLayoutManager);
        filesConsultDetailAdapter = new FilesConsultDetailAdapter();
        filesConsultChatRv.setAdapter(filesConsultDetailAdapter);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ConsultChatDetail.this);
        chatRv.setLayoutManager(linearLayoutManager);

    }

    @Override
    public void prepareViews() {
        super.prepareViews();
        quickConsultDetailAdapter = new QuickConsultDetailAdapter();
        chatRv.setAdapter(quickConsultDetailAdapter);

        isResponded = getIntent().getBooleanExtra(IS_RESPONDED, false);

        if (isResponded) {
            consultChatLl.setVisibility(View.GONE);
        }
        getQcDetails();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        fileConsultChatEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                addFiles();
            }
        });

        sendConsultChatEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    sendDetails();
                }
            }
        });

        subjectConsultChatEt.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) &&
                        (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    // Perform action on key press
                    hideSoftKeyboard(ConsultChatDetail.this);
                    descConsultChatEt.requestFocus();
                    //showSoftKeyboard(ConsultChatDetail.this);
                    return true;
                }
                return false;
            }
        });
    }


    private boolean check() {
        boolean isValid = true;
        if (TextUtils.isEmpty(subjectConsultChatEt.getText().toString().trim())) {
            focusEditTextRed(ConsultChatDetail.this, subjectConsultChatEt, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(descConsultChatEt.getText().toString().trim())) {
            isValid = false;
            focusEditTextRed(ConsultChatDetail.this, descConsultChatEt, true, null, 0);
        }
        return isValid;
    }


    private void sendDetails() {
        SmartUtils.showLoadingDialog(ConsultChatDetail.this);

        Map<String, String> params = new HashMap<>();
        params.put(USER_ID, getUserId());
        params.put(TITLE, subjectConsultChatEt.getText().toString().trim());
        params.put(CHAT_DESC, descConsultChatEt.getText().toString().trim());
        params.put(SERVICE_ID, getIntent().getStringExtra(CONSULT_CHAT_ID));
        if (isQcConsult) {
            params.put(SERVICE_TYPE, QC);
        } else {
            params.put(SERVICE_TYPE, EC);
        }

        HashMap<String, String> imageFilesMap = new HashMap<>();
        for (int i = 0; i < imageFiles.size(); i++) {
            imageFilesMap.put("" + i, imageFiles.get(i).path);
        }


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "AddChat");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, ConsultChatDetail.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    subjectConsultChatEt.setText("");
                    descConsultChatEt.setText("");
                    imageFiles.clear();
                    filesConsultDetailAdapter.notifyDataSetChanged();
                    getQcDetails();
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueueMultipart(requestParams, imageFilesMap, FILES, true);
    }


    private void addFiles() {
        final Dialog dialogImageSource = new Dialog(ConsultChatDetail.this);
        dialogImageSource.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImageSource.setContentView(R.layout.dialog_imagesource);

        SmartTextView cameraBtn = dialogImageSource.findViewById(R.id.camera_btn);
        SmartTextView galleryBtn = dialogImageSource.findViewById(R.id.gallery_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Object_Image object_image = getImageFileUri(ConsultChatDetail.this);
                currentImageUri = object_image.getImageUri();
                cameraImageName = object_image.getImageName();

                Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri); // set the image file name
                startActivityForResult(intentPicture, CAMERA_PROFILE_SOURCE);  // 1 for REQUEST_CAMERA and 2 for REQUEST_CAMERA_ATT
                dialogImageSource.dismiss();
            }
        });

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ConsultChatDetail.this, AlbumSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 4);
                startActivityForResult(intent, GALLERY_PROFILE_SOURCE);
                dialogImageSource.dismiss();
            }
        });
        dialogImageSource.show();
    }


    private void getQcDetails() {
        SmartUtils.showLoadingDialog(ConsultChatDetail.this);
        final JSONObject params = new JSONObject();
        try {
            params.put("Id", getIntent().getStringExtra(CONSULT_CHAT_ID));
//            params.put("Id", "2");
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        if (isQcConsult) {
            requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "QcDetail");
        } else {
            requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "EcDetail");
        }
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, ConsultChatDetail.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        chatModelJson = response.getJSONObject(RESULTS).getJSONArray(CHAT_MODEL);
                        quickConsultDetailAdapter.notifyDataSetChanged();
                        if (chatModelJson.length() > 0) {
                            layoutNoChats.setVisibility(View.GONE);
                        } else {
                            layoutNoChats.setVisibility(View.VISIBLE);
                        }
                        chatRv.scrollToPosition(chatModelJson.length() - 1);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {
                SmartUtils.hideLoadingDialog();
                layoutNoChats.setVisibility(View.VISIBLE);
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(ConsultChatDetail.this).addToRequestQueue(requestParams, false);
    }


    public class QuickConsultDetailAdapter extends RecyclerView.Adapter<QuickConsultDetailAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_consult_detail, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ConsultChatDetail.QuickConsultDetailAdapter.ViewHolder holder, final int position) {
            try {
                //   chatModelJson.getJSONObject(position).put(CHAT_DESC, "1sadfasdfsagasv asdfasdf asdfsadf asdf asf ssadfas asdf asdfadfasdf asdfa");

                /*if (position % 2 == 0) {
                    chatModelJson.getJSONObject(position).put(CREATE_USER_ID, "1");
                } else {
                    chatModelJson.getJSONObject(position).put(CREATE_USER_ID, "6");
                }*/

                if (chatModelJson.getJSONObject(position).getString(CREATE_USER_ID).equalsIgnoreCase(getUserId())) {
                    holder.chatConsultLl.setGravity(Gravity.END);
                    holder.chatConsultLl.setPadding(150, 0, 0, 0);
                    holder.dateConsultTv.setGravity(Gravity.END);
                } else {
                    holder.chatConsultLl.setGravity(Gravity.START);
                    holder.chatConsultLl.setPadding(0, 0, 150, 0);
                }
                holder.subjectConsultTv.setText(chatModelJson.getJSONObject(position).getString(TITLE));
                holder.descConsultTv.setText(chatModelJson.getJSONObject(position).getString(CHAT_DESC));

                if (chatModelJson.getJSONObject(position).getJSONArray(FILES).length() > 0) {
                    holder.filesConsultTv.setTextColor(Color.parseColor("#000000"));
                    StringBuilder filesNames = new StringBuilder();
                    for (int i = 0; i < chatModelJson.getJSONObject(position).getJSONArray(FILES).length(); i++) {
                        filesNames.append(getFileNameFromUrlSmart(chatModelJson.getJSONObject(position).getJSONArray(FILES).getString(i))).append("\n");
                    }

                    holder.filesConsultTv.setText(filesNames.substring(0, filesNames.length() - 1));
                } else {
                    holder.filesConsultTv.setTextColor(Color.parseColor("#f28080"));
                    holder.filesConsultTv.setText("No files added");
                }

                holder.dateConsultTv.setText(chatModelJson.getJSONObject(position).getString(CHAT_DATE));

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return chatModelJson.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private LinearLayout chatConsultLl;
            private SmartTextView subjectConsultTv;
            private SmartTextView descConsultTv;
            private SmartTextView filesConsultTv;
            private SmartTextView dateConsultTv;


            public ViewHolder(View itemView) {
                super(itemView);
                chatConsultLl = itemView.findViewById(R.id.chat_consult_ll);
                subjectConsultTv = itemView.findViewById(R.id.subject_consult_tv);
                descConsultTv = itemView.findViewById(R.id.desc_consult_tv);
                filesConsultTv = itemView.findViewById(R.id.files_consult_tv);
                dateConsultTv = itemView.findViewById(R.id.date_consult_tv);
            }
        }
    }


    public class FilesConsultDetailAdapter extends RecyclerView.Adapter<FilesConsultDetailAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_files, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            if (!TextUtils.isEmpty(imageFiles.get(position).name))
                Picasso.with(ConsultChatDetail.this).load(new File(imageFiles.get(position).path)).placeholder(R.drawable.ic_upload_img).into(holder.imageFilesIv);
            else {
                holder.imageFilesIv.setImageResource(R.drawable.ic_upload_img);
            }
            holder.nameFilesTv.setText(imageFiles.get(position).name);
            holder.deleteFilesIv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    imageFiles.remove(position);
                    notifyDataSetChanged();
                }
            });
        }


        @Override
        public int getItemCount() {
            return imageFiles.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private ImageView imageFilesIv;
            private SmartTextView nameFilesTv;
            private ImageView deleteFilesIv;


            public ViewHolder(View itemView) {
                super(itemView);
                imageFilesIv = itemView.findViewById(R.id.image_files_iv);
                nameFilesTv = itemView.findViewById(R.id.name_files_tv);
                deleteFilesIv = itemView.findViewById(R.id.delete_files_iv);
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PROFILE_SOURCE) {
                imageFiles.add(new Image(0, cameraImageName, (Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + cameraImageName), false));
                filesConsultDetailAdapter.notifyDataSetChanged();
            } else if (requestCode == GALLERY_PROFILE_SOURCE) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                imageFiles.addAll(images);
                filesConsultDetailAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
