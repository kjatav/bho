package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.Adapters.MedicalItemsAdapters;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;

public class PastMedicalHistory extends MasterActivity {

    private RecyclerView medicalHistoryRv;
    private RecyclerView fieldsMedicalHistoryRv;
    private SmartEditText addFieldsMedicalHistoryEt;
    private SmartTextView addFieldsMedicalHistoryTv;
    private SmartTextView submitMedicalHistoryTv;


    private MedicalItemsAdapters medicalItemsAdapters;
    private AdditionalFieldsAdapter additionalFieldsAdapter;

    private SmartCaching smartCaching;

    private ArrayList<String> medicalExtraHistoryData = new ArrayList<>();
    private ArrayList<JSONObject> medicalHistoryData = new ArrayList<>();

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_past_medical_history;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Past Medical History");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        medicalHistoryRv = findViewById(R.id.medical_history_rv);
        fieldsMedicalHistoryRv = findViewById(R.id.fields_medical_history_rv);
        addFieldsMedicalHistoryEt = findViewById(R.id.add_fields_medical_history_et);
        addFieldsMedicalHistoryTv = findViewById(R.id.add_fields_medical_history_tv);
        submitMedicalHistoryTv = findViewById(R.id.submit_medical_history_tv);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(PastMedicalHistory.this, 2);
        medicalHistoryRv.setLayoutManager(gridLayoutManager);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PastMedicalHistory.this);
        fieldsMedicalHistoryRv.setLayoutManager(linearLayoutManager);

        smartCaching = new SmartCaching(this);

        medicalItemsAdapters = new MedicalItemsAdapters(PastMedicalHistory.this, medicalHistoryData);
        additionalFieldsAdapter = new AdditionalFieldsAdapter(PastMedicalHistory.this, medicalExtraHistoryData);

        medicalHistoryRv.setAdapter(medicalItemsAdapters);
        fieldsMedicalHistoryRv.setAdapter(additionalFieldsAdapter);

        getDataFromDataBase();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addFieldsMedicalHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(addFieldsMedicalHistoryEt.getText().toString().trim())) {
                    additionalFieldsAdapter.addAdditionalFields(addFieldsMedicalHistoryEt.getText().toString().trim());
                    addFieldsMedicalHistoryEt.setText("");
                    isUpdated = true;
                }
            }
        });

        submitMedicalHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdated || additionalFieldsAdapter.giveUpadateStautus() || medicalItemsAdapters.giveMedicalItemUpdateStatus()) {
                    putDataIntoDatabaseAndSync(viewToJson());
                } else {
                    startActivity(new Intent(PastMedicalHistory.this, Home.class));
                }
            }
        });
    }

    /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getMedicalPastHistory() {
        SmartUtils.showLoadingDialog(PastMedicalHistory.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetPastMedicalHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PastMedicalHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_MEDICAL_HISTORY, OTHER_HISTORY, HISTORY);
                        String query = "SELECT * FROM " + TABLE_MEDICAL_HISTORY + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_MEDICAL_HISTORY, query).get(0);
                        Log.d("@@@MedicalHistory_data", dataFromDatabase.getAsString("otherHistory"));
                        Log.d("@@@MedicalHistory_data2", dataFromDatabase.getAsString("History"));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateMedicalPastHistory(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(PastMedicalHistory.this);
        try {
            params.put(HISTORY, medicalItemsAdapters.getCheckedItemsIds());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdatePastMedicalHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PastMedicalHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    smartCaching.updateSyncStatus(TABLE_MEDICAL_HISTORY, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(PastMedicalHistory.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(PastMedicalHistory.this, Home.class));
        }
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


        /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_MEDICAL_HISTORY + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_MEDICAL_HISTORY, query) != null && smartCaching.getDataFromCache(TABLE_MEDICAL_HISTORY, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_MEDICAL_HISTORY, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_MEDICAL_HISTORY, getUserId())) {
                getMedicalPastHistory();
            } else {
                updateMedicalPastHistory(viewToJson(), false);
            }
        } else {
            getMedicalPastHistory();
        }
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            Log.d("@@CHECKED_ITEMS_IDS", medicalItemsAdapters.getCheckedItemsIds().toString());
            Log.d("@@CHECKED_ITEMS", medicalItemsAdapters.getCheckedItems());
            Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(HISTORY, medicalItemsAdapters.getCheckedItems());
            params.put(OTHER_HISTORY, additionalFieldsAdapter.getAdditionalFields());
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_MEDICAL_HISTORY, HISTORY, OTHER_HISTORY);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_MEDICAL_HISTORY, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_MEDICAL_HISTORY, getUserId())) {
            updateMedicalPastHistory(data, true);
        }
    }


    private void setData(ContentValues data) {
        medicalHistoryData.clear();
        medicalExtraHistoryData.clear();
        try {
            JSONArray medicalHistoryJson = new JSONArray(data.getAsString(HISTORY));
            JSONArray otherHistoryJson = new JSONArray(data.getAsString(OTHER_HISTORY));

            for (int i = 0; i < medicalHistoryJson.length(); i++) {
                medicalHistoryData.add(medicalHistoryJson.getJSONObject(i));
            }
            for (int i = 0; i < otherHistoryJson.length(); i++) {
                medicalExtraHistoryData.add(otherHistoryJson.getString(i));
            }
            additionalFieldsAdapter.notifyDataSetChanged();
            medicalItemsAdapters.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
