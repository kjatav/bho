package com.bho.bho.Activities;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;


import com.bho.R;
import com.bho.bho.Activities.LandingScreens.AboutUsLanding;
import com.bho.bho.Activities.LandingScreens.HomeLanding;
import com.bho.bho.CoreMaster;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class Splash extends CoreMaster {

    private SmartCaching smartCaching;

    @Override
    public void prepareViews() {

    }

    @Override
    public void setActionListeners() {

    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_splash;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        smartCaching = new SmartCaching(this);

        getCountryData();

        Handler handler = new Handler();
//        Runnable runnable = new Runnable() {
//            @Override
//            public void run() {
//                autoLogin();
//            }
//        };
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(Splash.this, HomeLanding.class));
                supportFinishAfterTransition();
            }
        };
        handler.postDelayed(runnable, 1500);
    }


    private void getCountryData() {
        JSONObject params = new JSONObject();
        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Country");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Splash.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                if (responseCode == 200) {
                    Log.d("@@@country_success", ": true");
                    try {
                        JSONArray countriesJsonArray = response.getJSONArray(RESULTS);
                        smartCaching.cacheResponse(countriesJsonArray, TABLE_COUNTRY_DATA, false);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


}
