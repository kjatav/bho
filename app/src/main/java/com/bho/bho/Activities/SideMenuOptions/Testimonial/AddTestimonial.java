package com.bho.bho.Activities.SideMenuOptions.Testimonial;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.DatePicker;
import android.widget.ImageView;

import com.bho.R;
import com.bho.bho.MasterActivity;
import com.bho.smart.common.Object_Image;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.Map;

import static com.bho.smart.framework.SmartUtils.focusEditTextRed;
import static com.bho.smart.framework.SmartUtils.getImageFileUri;
import static com.bho.smart.framework.SmartUtils.getUserId;

public class AddTestimonial extends MasterActivity {

    private SmartEditText titleAddTestiEt;
    private SmartEditText dateAddTestiEt;
    private ImageView calendarAddTestiIv;
    private SmartEditText shortContentAddTestiEt;
    private SmartEditText pageContentAddTestiEt;
    private ImageView uploadAddTestiIv;
    private SmartTextView uploadAddTestiTv;
    private SmartEditText firstNameAddTestiEt;
    private SmartEditText lastNameAddTestiEt;
    private SmartEditText emailNameAddTestiEt;
    private SmartTextView saveNameAddTestiTv;
    private SmartTextView cancelNameAddTestiTv;
    private SmartTextView fileNameTv;

    private Uri currentImageUri;
    private String cameraImageName;

    private String selectedImagePath = "";
    private boolean isEdit;

    private int SELECT_PICTURE = 101;

    int year;
    int month;
    int day;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_add_testimonial;
    }

    @Override
    public void initComponents() {
        super.initComponents();

        isEdit = getIntent().getBooleanExtra(IS_EDIT, false);

        if (!isEdit) {
            setHeaderToolbar("Add Testimonial");
        } else {
            setHeaderToolbar("Edit Testimonial");
        }
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));


        titleAddTestiEt = findViewById(R.id.title_add_testi_et);
        dateAddTestiEt = findViewById(R.id.date_add_testi_et);
        calendarAddTestiIv = findViewById(R.id.calendar_add_testi_iv);
        shortContentAddTestiEt = findViewById(R.id.short_content_add_testi_et);
        pageContentAddTestiEt = findViewById(R.id.page_content_add_testi_et);
        uploadAddTestiIv = findViewById(R.id.upload_add_testi_iv);
        uploadAddTestiTv = findViewById(R.id.upload_add_testi_tv);
        fileNameTv = findViewById(R.id.file_name_tv);
        firstNameAddTestiEt = findViewById(R.id.first_name_add_testi_et);
        lastNameAddTestiEt = findViewById(R.id.last_name_add_testi_et);
        emailNameAddTestiEt = findViewById(R.id.email_name_add_testi_et);
        saveNameAddTestiTv = findViewById(R.id.save_name_add_testi_tv);
        cancelNameAddTestiTv = findViewById(R.id.cancel_name_add_testi_tv);
        CheckPermissionForWriteStorage();

        if (isEdit) {
            getTestimonialDetails();
        }

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        dateAddTestiEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();
            }
        });

        calendarAddTestiIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getDate();
            }
        });

        uploadAddTestiTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDialog();
            }
        });

        saveNameAddTestiTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    addTestimonial();
                }
            }
        });

        cancelNameAddTestiTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               onBackPressed();
            }
        });
    }


    private boolean check() {
        boolean isValid = true;
        if (TextUtils.isEmpty(titleAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, titleAddTestiEt, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(dateAddTestiEt.getText().toString().trim())) {
            isValid = false;
            focusEditTextRed(AddTestimonial.this, dateAddTestiEt, true, null, 0);
        } else if (TextUtils.isEmpty(shortContentAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, shortContentAddTestiEt, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(firstNameAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, firstNameAddTestiEt, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(lastNameAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, lastNameAddTestiEt, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(emailNameAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, emailNameAddTestiEt, true, null, 0);
            isValid = false;
        } else if (!SmartUtils.emailValidator(emailNameAddTestiEt.getText().toString().trim())) {
            focusEditTextRed(AddTestimonial.this, emailNameAddTestiEt, true, "Please enter a valid email address", 0);
            isValid = false;
        }
        return isValid;
    }


    private void getTestimonialDetails() {
        SmartUtils.showLoadingDialog(AddTestimonial.this);

        JSONObject params = new JSONObject();
        try {
            params.put(TESTI_ID, getIntent().getStringExtra(TESTI_ID));
        } catch (JSONException e) {
            e.printStackTrace();
        }


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetTestimonialDetail");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, AddTestimonial.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        Picasso.with(AddTestimonial.this).load(response.getJSONObject(RESULTS).getString(FILE_PATH)).placeholder(R.drawable.upload_placeholder).into(uploadAddTestiIv);
                        titleAddTestiEt.setText(response.getJSONObject(RESULTS).getString(TITLE));
                        dateAddTestiEt.setText(response.getJSONObject(RESULTS).getString(PUBLISH_DATE));
                        shortContentAddTestiEt.setText(response.getJSONObject(RESULTS).getString(SHORT_CONTENT));
                        pageContentAddTestiEt.setText(response.getJSONObject(RESULTS).getString(PAGE_CONTENT));
                        firstNameAddTestiEt.setText(response.getJSONObject(RESULTS).getString(FIRST_NAME));
                        lastNameAddTestiEt.setText(response.getJSONObject(RESULTS).getString(LAST_NAME));
                        emailNameAddTestiEt.setText(response.getJSONObject(RESULTS).getString(EMAIL));
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }


    private void addTestimonial() {
        SmartUtils.showLoadingDialog(AddTestimonial.this);

        Map<String, String> params = new HashMap<>();
        if (isEdit) {
            params.put(TESTI_ID, getIntent().getStringExtra(TESTI_ID));
        }
        params.put(USER_ID, getUserId());
        params.put(TITLE, titleAddTestiEt.getText().toString().trim());
        params.put(PUBLISH_DATE, dateAddTestiEt.getText().toString().trim());
        params.put(SHORT_CONTENT, shortContentAddTestiEt.getText().toString().trim());
        params.put(PAGE_CONTENT, pageContentAddTestiEt.getText().toString().trim());
        params.put(FIRST_NAME, firstNameAddTestiEt.getText().toString().trim());
        params.put(LAST_NAME, lastNameAddTestiEt.getText().toString().trim());
        params.put(EMAIL, emailNameAddTestiEt.getText().toString().trim());


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "ManageTestimonial");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, AddTestimonial.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    setResult(RESULT_OK);
                    supportFinishAfterTransition();
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueueMultipart(requestParams, selectedImagePath, true);
    }


    private void showDialog() {
        final Dialog dialogImageSource = new Dialog(AddTestimonial.this);
        dialogImageSource.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImageSource.setContentView(R.layout.dialog_imagesource);

        SmartTextView cameraBtn = dialogImageSource.findViewById(R.id.camera_btn);
        SmartTextView galleryBtn = dialogImageSource.findViewById(R.id.gallery_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Object_Image object_image = getImageFileUri(AddTestimonial.this);
                currentImageUri = object_image.getImageUri();
                cameraImageName = object_image.getImageName();

                Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri); // set the image file name
                startActivityForResult(intentPicture, CAMERA_PROFILE_SOURCE);  // 1 for REQUEST_CAMERA and 2 for REQUEST_CAMERA_ATT
                dialogImageSource.dismiss();
            }
        });

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AddTestimonial.this, AlbumSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                startActivityForResult(intent, GALLERY_PROFILE_SOURCE);
                dialogImageSource.dismiss();
            }
        });
        dialogImageSource.show();
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PROFILE_SOURCE) {
                selectedImagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + cameraImageName;
                Picasso.with(AddTestimonial.this).load(new File(selectedImagePath)).placeholder(R.drawable.upload_placeholder).into(uploadAddTestiIv);
            } else if (requestCode == GALLERY_PROFILE_SOURCE) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                selectedImagePath = images.get(0).path;
                Picasso.with(AddTestimonial.this).load(new File(selectedImagePath)).placeholder(R.drawable.upload_placeholder).into(uploadAddTestiIv);
            }
        }
    }

    @SuppressLint("ValidFragment")
    public class DatePickerFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

        int yearToSet, monthToSet, dayToSet;

        public DatePickerFragment(int yearToSet, int monthToSet, int dayToSet) {
            this.yearToSet = yearToSet;
            this.monthToSet = monthToSet;
            this.dayToSet = dayToSet;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState) {


            DatePickerDialog datepickerdialog = new DatePickerDialog(getActivity(),
                    AlertDialog.THEME_HOLO_DARK, this, yearToSet, monthToSet, dayToSet);
            Calendar calendar = Calendar.getInstance();
            datepickerdialog.getDatePicker().setMinDate(calendar.getTimeInMillis() - 10000);
            return datepickerdialog;
        }

        public void onDateSet(DatePicker view, int year, int month, int day) {
            dateAddTestiEt.setText(day + "-" + (month + 1) + "-" + year);
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }


    public void getDate() {
        if (TextUtils.isEmpty(dateAddTestiEt.getText().toString().trim())) {
            final Calendar calendar = Calendar.getInstance();
            year = calendar.get(Calendar.YEAR);
            month = calendar.get(Calendar.MONTH);
            day = calendar.get(Calendar.DAY_OF_MONTH);
        } else {
            String[] dateParts = dateAddTestiEt.getText().toString().trim().split("-");
            year = Integer.parseInt(dateParts[2]);
            month = Integer.parseInt(dateParts[1]) - 1;
            day = Integer.parseInt(dateParts[0]);
        }
        DatePickerFragment fragment = new DatePickerFragment(year, month, day);

        fragment.show(getFragmentManager(), "Date dialog");
    }
}
