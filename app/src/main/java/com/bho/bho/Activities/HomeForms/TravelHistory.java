package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.Adapters.MedicalItemsAdapters;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;

public class TravelHistory extends MasterActivity {

    private RadioGroup travelHistoryRg;

    private RadioButton yesTravelHistoryRb;
    private RadioButton noTravelHistoryRb;

    private LinearLayout fieldsTravelHistoryLl;
    private RecyclerView fieldsTravelHistoryRv;

    private SmartEditText addTravelHistoryEt;
    private SmartTextView addTravelHistoryTv;
    private SmartTextView submitTravelHistoryTv;

    private AdditionalFieldsAdapter additionalFieldsAdapter;

    private SmartCaching smartCaching;

    private ArrayList<String> travelExtraHistoryData = new ArrayList<>();

    private String hasTravelHistroy;

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_travel_history;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Travel History");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        travelHistoryRg = findViewById(R.id.travel_history_rg);

        yesTravelHistoryRb = findViewById(R.id.yes_travel_history_rb);
        noTravelHistoryRb = findViewById(R.id.no_travel_history_rb);

        fieldsTravelHistoryLl = findViewById(R.id.fields_travel_history_ll);
        fieldsTravelHistoryRv = findViewById(R.id.fields_travel_history_rv);

        addTravelHistoryEt = findViewById(R.id.add_travel_history_et);

        addTravelHistoryTv = findViewById(R.id.add_travel_history_tv);
        submitTravelHistoryTv = findViewById(R.id.submit_travel_history_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(TravelHistory.this);
        fieldsTravelHistoryRv.setLayoutManager(linearLayoutManager);

        additionalFieldsAdapter = new AdditionalFieldsAdapter(TravelHistory.this, travelExtraHistoryData);
        fieldsTravelHistoryRv.setAdapter(additionalFieldsAdapter);


        smartCaching = new SmartCaching(this);

        getDataFromDataBase();

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        travelHistoryRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                isUpdated = true;
                switch (checkedId) {
                    case R.id.yes_travel_history_rb:
                        hasTravelHistroy = YES;
                        fieldsTravelHistoryLl.setVisibility(View.VISIBLE);
                        break;
                    case R.id.no_travel_history_rb:
                        hasTravelHistroy = NO;
                        fieldsTravelHistoryLl.setVisibility(View.GONE);
                        break;
                }
            }
        });

        addTravelHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(addTravelHistoryEt.getText().toString().trim())) {
                    isUpdated = true;
                    additionalFieldsAdapter.addAdditionalFields(addTravelHistoryEt.getText().toString().trim());
                    addTravelHistoryEt.setText("");
                }
            }
        });

        submitTravelHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdated || additionalFieldsAdapter.giveUpadateStautus()) {
                    putDataIntoDatabaseAndSync(viewToJson());
                } else {
                    startActivity(new Intent(TravelHistory.this, Home.class));
                }
            }
        });
    }
    
    
    
         /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getTravelHistory() {
        SmartUtils.showLoadingDialog(TravelHistory.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetTravelHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, TravelHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_TRAVEL_HISTORY, OTHER_TRAVEL_HISTORY);
                        String query = "SELECT * FROM " + TABLE_TRAVEL_HISTORY + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_TRAVEL_HISTORY, query).get(0);
                        Log.d("@@@TravelHistory_data", dataFromDatabase.getAsString(OTHER_TRAVEL_HISTORY));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateTravelHistory(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(TravelHistory.this);


        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdateTravelHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, TravelHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    smartCaching.updateSyncStatus(TABLE_TRAVEL_HISTORY, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(TravelHistory.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(TravelHistory.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


        /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_TRAVEL_HISTORY + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_TRAVEL_HISTORY, query) != null && smartCaching.getDataFromCache(TABLE_TRAVEL_HISTORY, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_TRAVEL_HISTORY, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_TRAVEL_HISTORY, getUserId())) {
                getTravelHistory();
            } else {
                updateTravelHistory(viewToJson(), false);
            }
        } else {
            getTravelHistory();
        }
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(TRAVEL_HISTORY, hasTravelHistroy);
            params.put(OTHER_TRAVEL_HISTORY, additionalFieldsAdapter.getAdditionalFields());
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_TRAVEL_HISTORY, OTHER_TRAVEL_HISTORY);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_TRAVEL_HISTORY, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_TRAVEL_HISTORY, getUserId())) {
            updateTravelHistory(data, true);
        }
    }


    private void setData(ContentValues data) {
        travelExtraHistoryData.clear();

        if (data.getAsString(TRAVEL_HISTORY) != null) {
            switch (data.getAsString(TRAVEL_HISTORY)) {
                case YES:
                    hasTravelHistroy = YES;
                    fieldsTravelHistoryLl.setVisibility(View.VISIBLE);
                    yesTravelHistoryRb.setChecked(true);
                    break;

                case NO:
                    hasTravelHistroy = NO;
                    fieldsTravelHistoryLl.setVisibility(View.GONE);
                    noTravelHistoryRb.setChecked(true);
                    break;
            }
        } else {
            hasTravelHistroy = NO;
            noTravelHistoryRb.setChecked(true);
        }


        try {
            JSONArray otherHistoryJson = new JSONArray(data.getAsString(OTHER_TRAVEL_HISTORY));

            for (int i = 0; i < otherHistoryJson.length(); i++) {
                travelExtraHistoryData.add(otherHistoryJson.getString(i));
            }
            additionalFieldsAdapter.notifyDataSetChanged();

        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
