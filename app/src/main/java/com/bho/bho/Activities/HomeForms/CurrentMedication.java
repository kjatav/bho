package com.bho.bho.Activities.HomeForms;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ImageView;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static com.bho.smart.framework.SmartUtils.hideSoftKeyboard;
import static java.lang.Boolean.FALSE;

public class CurrentMedication extends MasterActivity {


    private RecyclerView currentMedicationRv;
    private SmartTextView addFieldsCurrentMedicationTv;
    private SmartTextView submitFieldsCurrentMedicationTv;

    private CurrentMedicationAdapter currentMedicationAdapter;

    private ArrayList<JSONObject> currentMedicationFields = new ArrayList<>();

    private Dialog addMedicationDialog;

    private SmartCaching smartCaching;

    private View currentMedicationNoData;

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_current_medication;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Current Medications");

        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));


        currentMedicationRv = findViewById(R.id.current_medication_rv);
        addFieldsCurrentMedicationTv = findViewById(R.id.add_fields_current_medication_tv);
        submitFieldsCurrentMedicationTv = findViewById(R.id.submit_fields_current_medication_tv);

        currentMedicationNoData = findViewById(R.id.current_medication_no_data);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(CurrentMedication.this);
        currentMedicationRv.setLayoutManager(linearLayoutManager);

        currentMedicationAdapter = new CurrentMedicationAdapter();
        currentMedicationRv.setAdapter(currentMedicationAdapter);


        smartCaching = new SmartCaching(this);

        getDataFromDataBase();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addFieldsCurrentMedicationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showAddMedicationDialog(false, 0);
            }
        });

        submitFieldsCurrentMedicationTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if (isUpdated) {
                    putDataIntoDatabaseAndSync(viewToJson());
                } else {
                    startActivity(new Intent(CurrentMedication.this, Home.class));
                }
            }
        });

    }
    
    
         /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getCurrentMedication() {
        SmartUtils.showLoadingDialog(CurrentMedication.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetCurrentMadication");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, CurrentMedication.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        response.put(USER_ID, getUserId());
                        response.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(response, TABLE_CURRENT_MEDICATIONS, RESULTS);
                        String query = "SELECT * FROM " + TABLE_CURRENT_MEDICATIONS + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_CURRENT_MEDICATIONS, query).get(0);
                        Log.d("@@@CurrentMedication", dataFromDatabase.getAsString(RESULTS));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateCurrentMedication(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(CurrentMedication.this);

        try {
            params.put(CURRENT_MEDICATION, new JSONArray(currentMedicationFields));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdateCurrentMadicines");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, CurrentMedication.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    smartCaching.updateSyncStatus(TABLE_CURRENT_MEDICATIONS, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(CurrentMedication.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(CurrentMedication.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }

         /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/

    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_CURRENT_MEDICATIONS + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_CURRENT_MEDICATIONS, query) != null && smartCaching.getDataFromCache(TABLE_CURRENT_MEDICATIONS, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_CURRENT_MEDICATIONS, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_CURRENT_MEDICATIONS, getUserId())) {
                getCurrentMedication();
            } else {
                updateCurrentMedication(viewToJson(), false);
            }
        } else {
            getCurrentMedication();
        }
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            //   Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(RESULTS, new JSONArray(currentMedicationFields));
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }

    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_CURRENT_MEDICATIONS, RESULTS);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_CURRENT_MEDICATIONS, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_CURRENT_MEDICATIONS, getUserId())) {
            updateCurrentMedication(data, true);
        }
    }


    private void setData(ContentValues data) {
        currentMedicationFields.clear();

        try {
            JSONArray otherHistoryJson = new JSONArray(data.getAsString(RESULTS));

            for (int i = 0; i < otherHistoryJson.length(); i++) {
                currentMedicationFields.add(otherHistoryJson.getJSONObject(i));
            }
            if (otherHistoryJson.length() > 0) {
                currentMedicationNoData.setVisibility(View.GONE);
            } else {
                currentMedicationNoData.setVisibility(View.VISIBLE);
            }
            currentMedicationAdapter.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void showAddMedicationDialog(boolean isEdit, final int position) {
        addMedicationDialog = new Dialog(CurrentMedication.this);
        addMedicationDialog.setContentView(R.layout.dialog_current_medication);

        SmartTextView currentMedicationTitleTv = addMedicationDialog.findViewById(R.id.current_medication_title_tv);
        ImageView closeIv = addMedicationDialog.findViewById(R.id.close_iv);
        final SmartEditText medicationDialogEt = addMedicationDialog.findViewById(R.id.medication_dialog_et);
        final SmartEditText doseDialogEt = addMedicationDialog.findViewById(R.id.dose_dialog_et);
        final SmartEditText frequencyDialogEt = addMedicationDialog.findViewById(R.id.frequency_dialog_et);
        final SmartEditText estimateDialogEt = addMedicationDialog.findViewById(R.id.estimate_dialog_et);
        SmartTextView addMedicationDialogTv = addMedicationDialog.findViewById(R.id.add_medication_dialog_tv);
        SmartTextView cancelMedicationDialogTv = addMedicationDialog.findViewById(R.id.cancel_medication_dialog_tv);

        if (isEdit) {
            currentMedicationTitleTv.setText("Edit Medication");
            addMedicationDialogTv.setText("Edit");

            try {
                medicationDialogEt.setText(currentMedicationFields.get(position).getString(MEDICATION));
                doseDialogEt.setText(currentMedicationFields.get(position).getString(DOSE));
                frequencyDialogEt.setText(currentMedicationFields.get(position).getString(FREQUENCY));
                estimateDialogEt.setText(currentMedicationFields.get(position).getString(ESTIMATE));

                medicationDialogEt.setSelection(medicationDialogEt.length());
                doseDialogEt.setSelection(doseDialogEt.length());
                frequencyDialogEt.setSelection(frequencyDialogEt.length());
                estimateDialogEt.setSelection(estimateDialogEt.length());
            } catch (JSONException e) {
                e.printStackTrace();
            }

            addMedicationDialogTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!TextUtils.isEmpty(medicationDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(doseDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(frequencyDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(estimateDialogEt.getText().toString().trim())) {
                        try {
                            currentMedicationFields.get(position).put(MEDICATION, medicationDialogEt.getText().toString().trim());
                            currentMedicationFields.get(position).put(DOSE, doseDialogEt.getText().toString().trim());
                            currentMedicationFields.get(position).put(FREQUENCY, frequencyDialogEt.getText().toString().trim());
                            currentMedicationFields.get(position).put(ESTIMATE, estimateDialogEt.getText().toString().trim());

                            currentMedicationAdapter.notifyDataSetChanged();
                            currentMedicationNoData.setVisibility(View.GONE);
                            hideKeyBoard(medicationDialogEt);
                            isUpdated = true;
                            addMedicationDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(CurrentMedication.this, "Please fill empty fields", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } else {
            currentMedicationTitleTv.setText("Add Medication");
            addMedicationDialogTv.setText("Add");

            addMedicationDialogTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    if (!TextUtils.isEmpty(medicationDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(doseDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(frequencyDialogEt.getText().toString().trim()) &&
                            !TextUtils.isEmpty(estimateDialogEt.getText().toString().trim())) {
                        try {
                            JSONObject newMedication = new JSONObject();
                            newMedication.put(MEDICATION, medicationDialogEt.getText().toString().trim());
                            newMedication.put(DOSE, doseDialogEt.getText().toString().trim());
                            newMedication.put(FREQUENCY, frequencyDialogEt.getText().toString().trim());
                            newMedication.put(ESTIMATE, estimateDialogEt.getText().toString().trim());

                            currentMedicationFields.add(newMedication);

                            currentMedicationAdapter.notifyDataSetChanged();
                            currentMedicationNoData.setVisibility(View.GONE);

                            hideKeyBoard(medicationDialogEt);

                            isUpdated = true;
                            addMedicationDialog.dismiss();
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        Toast.makeText(CurrentMedication.this, "Please fill empty fields", Toast.LENGTH_LONG).show();
                    }
                }
            });
        }

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard(medicationDialogEt);
                addMedicationDialog.dismiss();
            }
        });


        cancelMedicationDialogTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyBoard(medicationDialogEt);
                addMedicationDialog.dismiss();
            }
        });

        addMedicationDialog.show();
    }

    private void hideKeyBoard(SmartEditText et) {
        InputMethodManager in = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
        assert in != null;
        in.hideSoftInputFromWindow(et.getApplicationWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
    }

    public class CurrentMedicationAdapter extends RecyclerView.Adapter<CurrentMedicationAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_medications, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.medicationRowTv.setText(currentMedicationFields.get(position).getString(MEDICATION));
                holder.doseRowTv.setText(currentMedicationFields.get(position).getString(DOSE));
                holder.frequencyRowTv.setText(currentMedicationFields.get(position).getString(FREQUENCY));
                holder.estimateRowTv.setText(currentMedicationFields.get(position).getString(ESTIMATE));

                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showAddMedicationDialog(true, position);
                    }
                });

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return currentMedicationFields.size();
        }


        public class ViewHolder extends RecyclerView.ViewHolder {

            private SmartTextView medicationRowTv;
            private SmartTextView doseRowTv;
            private SmartTextView frequencyRowTv;
            private SmartTextView estimateRowTv;


            public ViewHolder(View itemView) {
                super(itemView);
                medicationRowTv = itemView.findViewById(R.id.medication_row_tv);
                doseRowTv = itemView.findViewById(R.id.dose_row_tv);
                frequencyRowTv = itemView.findViewById(R.id.frequency_row_tv);
                estimateRowTv = itemView.findViewById(R.id.estimate_row_tv);
            }
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
