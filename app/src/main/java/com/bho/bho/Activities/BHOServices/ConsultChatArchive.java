package com.bho.bho.Activities.BHOServices;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.bho.R;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.EntriesObject;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class ConsultChatArchive extends MasterActivity {

    JSONArray qcArchiveJson = new JSONArray();
    ArrayList<EntriesObject> qcArchiveDataArrayList = new ArrayList<>();
    ArrayList<JSONObject> qcArchiveDataSetArrayList = new ArrayList<>();


    QcArchiveAdapter qcArchiveAdapter;
    private SmartEditText searchQcArchiveEt;
    private RecyclerView qcArchiveListRv;
    private View qcArchiveNoData;
    private SmartTextView countQcArchiveTv;
    private SmartTextView previousQcArchiveTv;
    private SmartTextView nextQcArchiveTv;

    private boolean isQcConsult = false;

    private int totalDataCount = 0;

    private int pageNo = 0;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_quick_consult_archive;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        isQcConsult = getIntent().getStringExtra(CONSULT_TYPE).equalsIgnoreCase(QC);

        if (isQcConsult) {
            setHeaderToolbar("Quick Consult(QC) Archive");
        } else {
            setHeaderToolbar("Expanded Consult(EC) Archive");
        }

        qcArchiveAdapter = new QcArchiveAdapter();

        searchQcArchiveEt = findViewById(R.id.search_qc_archive_et);
        qcArchiveListRv = findViewById(R.id.qc_archive_list_rv);
        qcArchiveNoData = findViewById(R.id.qc_archive_no_data);
        countQcArchiveTv = findViewById(R.id.count_qc_archive_tv);
        previousQcArchiveTv = findViewById(R.id.previous_qc_archive_tv);
        nextQcArchiveTv = findViewById(R.id.next_qc_archive_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(ConsultChatArchive.this);
        qcArchiveListRv.setLayoutManager(linearLayoutManager);

        qcArchiveAdapter = new QcArchiveAdapter();
        qcArchiveListRv.setAdapter(qcArchiveAdapter);

        getPaymentHistory();
    }

    public void setActionListeners() {
        super.setActionListeners();
        nextQcArchiveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo < qcArchiveDataArrayList.size() - 1) {
                    pageNo = pageNo + 1;
                    setEntryData();
                }
            }
        });

        previousQcArchiveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo > 0) {
                    pageNo = pageNo - 1;
                    setEntryData();
                }
            }
        });

        searchQcArchiveEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                if (qcArchiveJson != null) {
                    try {
                        insertData(s.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void afterTextChanged(Editable s) {

            }
        });
    }


    private void getPaymentHistory() {
        SmartUtils.showLoadingDialog(ConsultChatArchive.this);
        final JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        if (isQcConsult) {
            requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetQcList");
        } else {
            requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetEC");
        }
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, ConsultChatArchive.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        qcArchiveJson = response.getJSONArray(RESULTS);
                        insertData("");
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(ConsultChatArchive.this).addToRequestQueue(requestParams, false);
    }


    public class QcArchiveAdapter extends RecyclerView.Adapter<QcArchiveAdapter.ViewHolder> {

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_qc_archive, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.qcArchiveStatusTv.setText(qcArchiveDataSetArrayList.get(position).getString(REQUEST_STATUS));
                holder.qcArchiveTimeTv.setText(qcArchiveDataSetArrayList.get(position).getString(REQUEST_TIME));
                holder.qcArchiveDescTv.setText(qcArchiveDataSetArrayList.get(position).getString(REQUEST_DESC));
                if (qcArchiveDataSetArrayList.get(position).getBoolean(REQUEST_RESPONSE)) {
                    holder.qcArchiveResponseIv.setImageResource(R.drawable.ic_approved_testi);
                } else {
                    holder.qcArchiveResponseIv.setImageResource(R.drawable.ic_unapproved_testi);
                }
                holder.qcArchiveDetailIv.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent i = new Intent(ConsultChatArchive.this, ConsultChatDetail.class);
                        try {
                            if (isQcConsult) {
                                i.putExtra(CONSULT_TYPE, QC);
                            } else {
                                i.putExtra(CONSULT_TYPE, EC);
                            }
                            i.putExtra(CONSULT_CHAT_ID, qcArchiveDataSetArrayList.get(position).getString("Id"));
                            i.putExtra(IS_RESPONDED, qcArchiveDataSetArrayList.get(position).getBoolean("responseReceived"));


                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        startActivity(i);
                    }
                });
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return qcArchiveDataSetArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {

            private ImageView qcArchiveDetailIv;
            private SmartTextView qcArchiveStatusTv;
            private SmartTextView qcArchiveTimeTv;
            private SmartTextView qcArchiveDescTv;
            private ImageView qcArchiveResponseIv;


            public ViewHolder(View itemView) {
                super(itemView);
                qcArchiveDetailIv = itemView.findViewById(R.id.qc_archive_detail_iv);
                qcArchiveStatusTv = itemView.findViewById(R.id.qc_archive_status_tv);
                qcArchiveTimeTv = itemView.findViewById(R.id.qc_archive_time_tv);
                qcArchiveDescTv = itemView.findViewById(R.id.qc_archive_desc_tv);
                qcArchiveResponseIv = itemView.findViewById(R.id.qc_archive_response_iv);
            }
        }
    }


    private void insertData(String filter) throws JSONException {
        qcArchiveDataArrayList.clear();
        qcArchiveDataSetArrayList.clear();
        pageNo = 0;
        totalDataCount = 0;
        ArrayList<JSONObject> jsonObjects = new ArrayList<>();
        ArrayList<JSONObject> dataSet = new ArrayList<>();
        for (int i = 1; i <= qcArchiveJson.length(); i++) {
            if (TextUtils.isEmpty(filter)) {
                totalDataCount = ++totalDataCount;
                jsonObjects.add(qcArchiveJson.getJSONObject(i - 1));
            } else {
                String statusFromJsonArray = qcArchiveJson.getJSONObject(i - 1).getString(REQUEST_STATUS);
                String timeFromJsonArray = qcArchiveJson.getJSONObject(i - 1).getString(REQUEST_TIME);
                String descFromJsonArray = qcArchiveJson.getJSONObject(i - 1).getString(REQUEST_DESC);
                filter = filter.toLowerCase();
                if (statusFromJsonArray.toLowerCase().contains(filter) ||
                        timeFromJsonArray.toLowerCase().contains(filter) ||
                        descFromJsonArray.toLowerCase().contains(filter)) {
                    totalDataCount = ++totalDataCount;
                    jsonObjects.add(qcArchiveJson.getJSONObject(i - 1));
                }
            }
        }
        Log.e("@@K_DATA", jsonObjects.toString());
        int firstEntry = 0, lastEntry;
        for (int m = 1; m <= jsonObjects.size(); m++) {

//            Log.e("@@Module_test", "" + (m % 10));
            int k = m % 10;
            dataSet.add(jsonObjects.get(m - 1));
            if (m == 1) {
                firstEntry = 1;
            } else if (k == 1) {
                firstEntry = m;
            }
            if (k == 0 || m == jsonObjects.size()) {
                lastEntry = m;
                qcArchiveDataArrayList.add(new EntriesObject(firstEntry, lastEntry, dataSet));
                Log.e("@@PAGE", qcArchiveDataArrayList.size() + "---" + dataSet);

                dataSet = new ArrayList<>();
            }
        }
        setEntryData();
    }


    @SuppressLint("SetTextI18n")
    private void setEntryData() {
        if (qcArchiveDataArrayList.size() != 0) {
            qcArchiveNoData.setVisibility(View.GONE);
            qcArchiveDataSetArrayList = qcArchiveDataArrayList.get(pageNo).getDataSet();
            countQcArchiveTv.setText("Showing " +
                    qcArchiveDataArrayList.get(pageNo).getFirstEntry()
                    + " to " +
                    qcArchiveDataArrayList.get(pageNo).getLastEntry()
                    + " of " +
                    totalDataCount
                    + " entries"
            );
        } else {
            qcArchiveDataSetArrayList.clear();
            qcArchiveNoData.setVisibility(View.VISIBLE);
        }
        qcArchiveAdapter.notifyDataSetChanged();
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
