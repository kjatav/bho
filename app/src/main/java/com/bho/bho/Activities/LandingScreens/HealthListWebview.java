package com.bho.bho.Activities.LandingScreens;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebView;

import com.bho.R;
import com.bho.bho.LandingBaseActivity;
import com.bho.smart.customViews.SmartTextView;

public class HealthListWebview extends LandingBaseActivity {

    WebView htmlWv;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_health_list_webview;
    }

    @Override
    public void initComponents() {
        super.initComponents();


        if (getIntent().getBooleanExtra(IS_CONSUMER,true)) {
            setHeaderToolbar("Healthcare Consumers");
        } else {
            setHeaderToolbar("Health Professionals");
        }

        htmlWv = findViewById(R.id.html_wv);

        final String mimeType = "text/html";
        final String encoding = "UTF-8";
        String html = getIntent().getStringExtra("html_data");


        htmlWv.loadDataWithBaseURL("", html, mimeType, encoding, "");
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
