package com.bho.bho.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.Spinner;

import com.bho.R;
import com.bho.bho.Activities.SideMenuOptions.TermsAndConditions;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.CountryData;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.focusEditTextRed;

public class Register extends MasterActivity implements AdapterView.OnItemSelectedListener {

    private ArrayList<CountryData> countryData = new ArrayList<>();
    private ArrayList<String> countryNames = new ArrayList<>();

    private ArrayList<String> dates = new ArrayList<>();
    private ArrayList<String> months = new ArrayList<>();
    private ArrayList<String> years = new ArrayList<>();

    private String countryId = "1";
    private String date = "1";
    private String month = "1";
    private String year = "1";

    private Spinner memberRegSpn;

    private SmartEditText firstNameRegTv;
    private SmartEditText lastNameRegTv;
    private SmartEditText emailNameRegTv;
    private SmartEditText passwordRegTv;
    private SmartEditText reenterPasswordRegTv;

    private Spinner countryRegSpn;
    private Spinner dateDobRegSpn;
    private Spinner monthDobRegSpn;
    private Spinner yearDobRegSpn;

    private CheckBox termsRegCb;

    private SmartTextView termsRegTv;
    private SmartTextView privacyRegTv;
    private SmartTextView registerRegTv;

    private Dialog setPasswordDialog;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_register;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));


        memberRegSpn = findViewById(R.id.member_reg_spn);

        firstNameRegTv = findViewById(R.id.first_name_reg_tv);
        lastNameRegTv = findViewById(R.id.last_name_reg_tv);
        emailNameRegTv = findViewById(R.id.email_name_reg_tv);
        passwordRegTv = findViewById(R.id.password_reg_tv);
        reenterPasswordRegTv = findViewById(R.id.reenter_password_reg_tv);

        countryRegSpn = findViewById(R.id.country_reg_spn);
        dateDobRegSpn = findViewById(R.id.date_dob_reg_spn);
        monthDobRegSpn = findViewById(R.id.month_dob_reg_spn);
        yearDobRegSpn = findViewById(R.id.year_dob_reg_spn);

        termsRegCb = findViewById(R.id.terms_reg_cb);

        termsRegTv = findViewById(R.id.terms_reg_tv);
        privacyRegTv = findViewById(R.id.privacy_reg_tv);

        registerRegTv = findViewById(R.id.register_reg_tv);

        memberRegSpn.setOnItemSelectedListener(this);
        countryRegSpn.setOnItemSelectedListener(this);
        dateDobRegSpn.setOnItemSelectedListener(this);
        monthDobRegSpn.setOnItemSelectedListener(this);
        yearDobRegSpn.setOnItemSelectedListener(this);

        for (int i = Calendar.getInstance().get(Calendar.YEAR); i > 1900; i--) {
            years.add(String.valueOf(i));
        }

        ArrayAdapter<String> yearAdapter = new ArrayAdapter<>(Register.this, android.R.layout.simple_spinner_item, years);
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearDobRegSpn.setAdapter(yearAdapter);

        getCountryData();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        registerRegTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    register();
                }
            }
        });

        termsRegTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, TermsAndConditions.class);
                startActivity(i);
            }
        });

        privacyRegTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(Register.this, PrivacyPolicy.class);
//                i.putExtra(IS_REGISTER, false);
                startActivity(i);
            }
        });

        passwordRegTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showSetPasswordDialog();
            }
        });
    }

    private void getCountryData() {
        SmartUtils.showLoadingDialog(Register.this);
        JSONObject params = new JSONObject();
        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Country");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Register.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@Register_success", ": true");
                    try {
                        JSONArray countriesJsonArray = response.getJSONArray(RESULTS);

                        countryData.add(new CountryData("0", "Country", "0"));
                        countryNames.add("Country");

                        for (int i = 0; i < countriesJsonArray.length(); i++) {
                            countryData.add(new CountryData(countriesJsonArray.getJSONObject(i).getString(COUNTRY_ID),
                                    countriesJsonArray.getJSONObject(i).getString(COUNTRY_NAME),
                                    countriesJsonArray.getJSONObject(i).getString(COUNTRY_CODE)));

                            countryNames.add(countriesJsonArray.getJSONObject(i).getString(COUNTRY_NAME));
                        }

                        ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(Register.this, android.R.layout.simple_spinner_item, countryNames);
                        dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        countryRegSpn.setAdapter(dataAdapter);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }


    private void register() {
        SmartUtils.showLoadingDialog(Register.this);
        JSONObject params = new JSONObject();
        try {
            params.put(FIRST_NAME, firstNameRegTv.getText().toString().trim());
            params.put(LAST_NAME, lastNameRegTv.getText().toString().trim());
            params.put(USER_NAME, emailNameRegTv.getText().toString().trim());
            params.put(PASSWORD, passwordRegTv.getText().toString().trim());
            params.put(DEVICE_TOKEN, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_FIREBASE_REGID, "no_id"));
            params.put(DEVICE_TYPE, ANDROID);
            params.put(COUNTRY_ID, countryId);
            params.put(DAY, date);
            params.put(MONTH, month);
            params.put(YEAR, year);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Register");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Register.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@Register_success", ": true");

                    final Dialog verifyDialog = new Dialog(Register.this);
                    verifyDialog.setCancelable(false);
                    verifyDialog.setContentView(R.layout.dialog_verify_acc);

                    final SmartTextView okayVerifyTv = verifyDialog.findViewById(R.id.okay_verify_tv);

                    okayVerifyTv.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            startActivity(new Intent(Register.this, Login.class));
                            verifyDialog.dismiss();
                        }
                    });

                    verifyDialog.show();

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }

    private void showSetPasswordDialog() {
        setPasswordDialog = new Dialog(Register.this);
        setPasswordDialog.setContentView(R.layout.dialog_set_password);

        SmartTextView currentMedicationTitleTv = setPasswordDialog.findViewById(R.id.current_medication_title_tv);
        ImageView closeIv = setPasswordDialog.findViewById(R.id.close_iv);
        final RadioButton sizeCharCb = setPasswordDialog.findViewById(R.id.size_char_cb);
        final RadioButton capitalCharCb = setPasswordDialog.findViewById(R.id.capital_char_cb);
        final RadioButton specialCharCb = setPasswordDialog.findViewById(R.id.special_char_cb);
        final RadioButton lowerCharCb = setPasswordDialog.findViewById(R.id.lower_char_cb);
        final RadioButton numericalCharCb = setPasswordDialog.findViewById(R.id.numerical_char_cb);
        final SmartEditText passwordDialogEt = setPasswordDialog.findViewById(R.id.password_dialog_et);
        SmartTextView donePasswordDialogTv = setPasswordDialog.findViewById(R.id.done_password_dialog_tv);

        donePasswordDialogTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (specialCharCb.isChecked() && sizeCharCb.isChecked() && capitalCharCb.isChecked() && numericalCharCb.isChecked() && lowerCharCb.isChecked()) {
                    passwordRegTv.setText(passwordDialogEt.getText().toString());
                    setPasswordDialog.dismiss();
                }
            }
        });
        String password = passwordRegTv.getText().toString();
        if (!TextUtils.isEmpty(passwordRegTv.getText().toString())) {
            specialCharCb.setChecked(SmartUtils.getSpecialCharacter(password));
            sizeCharCb.setChecked(SmartUtils.hasLength(password));
            capitalCharCb.setChecked(SmartUtils.hashUppercase(password));
            numericalCharCb.setChecked(SmartUtils.hashNumber(password));
            lowerCharCb.setChecked(SmartUtils.hashLowerCase(password));
        }

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                setPasswordDialog.dismiss();
            }
        });

        passwordDialogEt.setText(passwordRegTv.getText().toString());

        passwordDialogEt.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                specialCharCb.setChecked(SmartUtils.getSpecialCharacter(s.toString()));
                sizeCharCb.setChecked(SmartUtils.hasLength(s.toString()));
                capitalCharCb.setChecked(SmartUtils.hashUppercase(s.toString()));
                numericalCharCb.setChecked(SmartUtils.hashNumber(s.toString()));
                lowerCharCb.setChecked(SmartUtils.hashLowerCase(s.toString()));
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        setPasswordDialog.show();
    }


    private boolean check() {
        boolean isValid = true;

        if (TextUtils.isEmpty(firstNameRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, firstNameRegTv, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(lastNameRegTv.getText().toString().trim())) {
            isValid = false;
            focusEditTextRed(Register.this, lastNameRegTv, true, null, 0);
        } else if (TextUtils.isEmpty(emailNameRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, emailNameRegTv, true, null, 0);
            isValid = false;
        } else if (!SmartUtils.emailValidator(emailNameRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, emailNameRegTv, true, "Please enter a valid email address", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(passwordRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, passwordRegTv, true, null, 0);
            isValid = false;
        } else if (TextUtils.isEmpty(reenterPasswordRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, reenterPasswordRegTv, true, null, 0);
            isValid = false;
        } else if (!reenterPasswordRegTv.getText().toString().trim().equals(passwordRegTv.getText().toString().trim())) {
            focusEditTextRed(Register.this, passwordRegTv, false, null, 0);
            focusEditTextRed(Register.this, reenterPasswordRegTv, true, "Password Mismatch", 0);
            reenterPasswordRegTv.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    passwordRegTv.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, 0);
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            isValid = false;
        } else if (passwordRegTv.getText().toString().trim().length() < 8) {
            focusEditTextRed(Register.this, passwordRegTv, true, "Password should be atleast 8 characters long", 0);
            isValid = false;
        } else if (countryId.equalsIgnoreCase("0")) {
            isValid = false;
            SmartUtils.showSnackBar(Register.this, "Please select your country", Snackbar.LENGTH_LONG);
        } else if (!termsRegCb.isChecked()) {
            isValid = false;
            SmartUtils.showSnackBar(Register.this, "Please agree to the Terms of Service & Privacy Policy", Snackbar.LENGTH_LONG);
        }

        return isValid;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == R.id.member_reg_spn) {

        } else if (parent.getId() == R.id.country_reg_spn) {
            countryId = countryData.get(position).getCountryId();
            //   Toast.makeText(parent.getContext(), "Selected: " + countryData.get(position).getCountryName() + "-----" + countryId, Toast.LENGTH_LONG).show();
        } else if (parent.getId() == R.id.date_dob_reg_spn) {
            date = String.valueOf(position + 1);
        } else if (parent.getId() == R.id.month_dob_reg_spn) {
            month = String.valueOf(position + 1);
            setDateDays();
        } else if (parent.getId() == R.id.year_dob_reg_spn) {
            year = parent.getItemAtPosition(position).toString();
            setDateDays();
        }
    }

    public void onNothingSelected(AdapterView<?> arg0) {
        // TODO Auto-generated method stub
    }

    private void setDateDays() {
        dates.clear();
        Calendar mycal = new GregorianCalendar(Integer.valueOf(year), Integer.valueOf(month) - 1, 1);
        //   Log.d("Days in month===", month + "----" + mycal.getActualMaximum(Calendar.DAY_OF_MONTH));

        for (int i = 1; i <= mycal.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            dates.add(String.valueOf(i));
        }

        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(Register.this, android.R.layout.simple_spinner_item, dates);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateDobRegSpn.setAdapter(dateAdapter);
        dateDobRegSpn.setSelection(Integer.valueOf(date) - 1);
    }
}
