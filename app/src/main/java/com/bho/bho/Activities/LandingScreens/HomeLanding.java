package com.bho.bho.Activities.LandingScreens;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.LinearSnapHelper;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SnapHelper;
import android.support.v7.widget.Toolbar;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.PartnersAdapter;
import com.bho.bho.Fragments.BhoService;
import com.bho.bho.Fragments.HealthProfile;
import com.bho.bho.Fragments.PaymentHistory;
import com.bho.bho.LandingBaseActivity;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartFragment;

import java.util.ArrayList;
import java.util.List;

public class HomeLanding extends LandingBaseActivity {

    private RecyclerView partnersRv;
    private ImageView leftPartnerIv;
    private ImageView rightPartnerIv;
    private PartnersAdapter partnersAdapter;
    private int imagePosition = 0;
    private ViewPager viewpagerImages;
    private SmartTextView titleBhoTv;
    private LinearLayout videoHomeBtn;

    ArrayList<Integer> drawables = new ArrayList<>();

    @Override
    public int getLayoutID() {
        return R.layout.activity_home_landing;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Home");

        titleBhoTv = findViewById(R.id.title_bho_tv);
        titleBhoTv.setText(Html.fromHtml("ACCESS A BHO DOCTOR ANYTIME <b><font color=#84543E>FROM ANYWHERE!</font></b>"));

        viewpagerImages = findViewById(R.id.viewpager_images);

        leftPartnerIv = findViewById(R.id.left_partner_iv);
        rightPartnerIv = findViewById(R.id.right_partner_iv);
        videoHomeBtn = findViewById(R.id.video_home_btn);

        drawables.add(R.drawable.who_image);
        drawables.add(R.drawable.partners_2);
        drawables.add(R.drawable.who_image);
        drawables.add(R.drawable.partners_2);

        setupViewPager(viewpagerImages);

    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        videoHomeBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                goToUrl("https://www.youtube.com/watch?v=9Hjrg9jaIfQ&feature=youtu.be");
            }
        });

        leftPartnerIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpagerImages.setCurrentItem(viewpagerImages.getCurrentItem() - 1);
            }
        });

        rightPartnerIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                viewpagerImages.setCurrentItem(viewpagerImages.getCurrentItem() + 1);
            }
        });
    }

    @SuppressLint("SetJavaScriptEnabled")
    private void goToUrl(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        for (int i = 0; i < drawables.size(); i++) {
            Bundle b = new Bundle();
            b.putInt("image", drawables.get(i));
            PartnerImages partnerImages = new PartnerImages();
            partnerImages.setArguments(b);
            adapter.addFragment(partnerImages, "");
        }
        viewPager.setAdapter(adapter);
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    public static class PartnerImages extends SmartFragment {
        ImageView partnersIv;

        @Override
        public int setLayoutId() {
            return R.layout.row_partners;
        }

        @Override
        public View setLayoutView() {
            return null;
        }

        @Override
        public void initComponents(View currentView) {
            partnersIv = currentView.findViewById(R.id.partners_iv);
            partnersIv.setImageDrawable(getResources().getDrawable(getArguments().getInt("image")));

        }

        @Override
        public void prepareViews(View currentView) {

        }

        @Override
        public void setActionListeners(View currentView) {

        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
