package com.bho.bho.Activities.SideMenuOptions;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Activities.Login;
import com.bho.bho.MasterActivity;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.PaymentMethodNonce;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;

import static com.bho.smart.framework.SmartUtils.getIsMember;
import static com.bho.smart.framework.SmartUtils.getUserId;
import static com.bho.smart.framework.SmartUtils.showLoadingDialog;

public class Payment extends MasterActivity {

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_payment;
    }

    private SmartTextView ugTextTv;
    private RadioGroup serviceTypeRg;
    private RadioButton serviceGoldRb;
    private RadioButton serviceSilverRb;
    private Spinner subscriptionYearsSpn;
    private SmartTextView servicNameTv;
    private SmartTextView subscriptionRateTv;
    private SmartTextView totalAmountTv;
    private ImageView paypalIv;
    private LinearLayout ugPaymentLl;

    private String serviceType;
    private String memberShip;

    private String subcriptionYear;

    private int goldRate;
    private int silverRate;

    private int consultRate;
    private int subcriptionRate;
    private int totalAmount;

    int REQUEST_CODE = 508;
    String token = "no_token";


    private JSONObject serviceData = new JSONObject();

    Map<String, String> params = new HashMap<>();
    HashMap<String, String> imageFilesMap = new HashMap<>();


    @SuppressLint("SetTextI18n")
    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Payment");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        ugTextTv = findViewById(R.id.ug_text_tv);
        servicNameTv = findViewById(R.id.service_name_tv);
        serviceTypeRg = findViewById(R.id.service_type_rg);
        serviceGoldRb = findViewById(R.id.service_gold_rb);
        serviceSilverRb = findViewById(R.id.service_silver_rb);
        ugPaymentLl = findViewById(R.id.ug_payment_ll);
        subscriptionYearsSpn = findViewById(R.id.subscription_years_spn);
        subscriptionRateTv = findViewById(R.id.subscription_rate_tv);
        totalAmountTv = findViewById(R.id.total_amount_tv);
        paypalIv = findViewById(R.id.paypal_iv);

        serviceType = getIntent().getStringExtra(SERVICE_TYPE);

        serviceGoldRb.setChecked(true);


        if (getIsMember()) {
            serviceSilverRb.setVisibility(View.GONE);
            ugPaymentLl.setVisibility(View.GONE);
            ugTextTv.setVisibility(View.GONE);
        }

        switch (serviceType) {
            case QC:
                goldRate = Integer.parseInt(getString(R.string.qc_gold));
                silverRate = Integer.parseInt(getString(R.string.qc_silver));
                servicNameTv.setText("QuickConsult");
                serviceGoldRb.setText("QuickConsult $" + goldRate + "\n(Gold)");
                serviceSilverRb.setText("QuickConsult $" + silverRate + "\n(Silver)");
                break;
            case EC:
                goldRate = Integer.parseInt(getString(R.string.ec_gold));
                silverRate = Integer.parseInt(getString(R.string.ec_silver));
                servicNameTv.setText("ExpandedConsult");
                serviceGoldRb.setText("ExpandedConsult $" + goldRate + "\n(Gold)");
                serviceSilverRb.setText("ExpandedConsult $" + silverRate + "\n(Silver)");
                break;
            case MHT:
                goldRate = Integer.parseInt(getString(R.string.mht_gold));
                silverRate = Integer.parseInt(getString(R.string.mht_silver));
                servicNameTv.setText("MyHealthTrends");
                serviceGoldRb.setText("MyHealthTrends $" + goldRate + "\n(Gold)");
                serviceSilverRb.setText("MyHealthTrends $" + silverRate + "\n(Silver)");
                break;
            case UG:
                setPageForUg();
                break;
        }

        consultRate = goldRate;

        if (getIsMember()) {
            subcriptionRate = 0;
        } else {
            subcriptionRate = Integer.parseInt(getString(R.string.one_year_membership_rate));
            subcriptionYear = "1";
        }


        setTotalAmount();
    }


    @Override
    public void setActionListeners() {
        super.setActionListeners();

        serviceTypeRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.service_gold_rb:
                        if (!getIsMember()) {
                            ugPaymentLl.setVisibility(View.VISIBLE);
                            switch (subscriptionYearsSpn.getSelectedItemPosition()) {
                                case 0:
                                    subcriptionRate = Integer.parseInt(getString(R.string.one_year_membership_rate));
                                    break;
                                case 1:
                                    subcriptionRate = Integer.parseInt(getString(R.string.five_year_membership_rate));
                                    break;
                            }

                        }
                        consultRate = goldRate;
                        setTotalAmount();
                        break;

                    case R.id.service_silver_rb:
                        consultRate = silverRate;
                        subcriptionRate = 0;
                        setTotalAmount();
                        ugPaymentLl.setVisibility(View.GONE);
                        break;
                }
            }
        });

        subscriptionYearsSpn.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("1 year")) {
                    subcriptionRate = Integer.parseInt(getString(R.string.one_year_membership_rate));
                    subcriptionYear = "1";
                    subscriptionRateTv.setText("$" + subcriptionRate + " USD");
                    setTotalAmount();
                } else if (parent.getItemAtPosition(position).toString().equalsIgnoreCase("5 year")) {
                    subcriptionRate = Integer.parseInt(getString(R.string.five_year_membership_rate));
                    subcriptionYear = "5";
                    subscriptionRateTv.setText("$" + subcriptionRate + " USD");
                    setTotalAmount();
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        paypalIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                getTokenAndPay();
            }
        });
    }


    private void getQcData() {

        try {
            serviceData = new JSONObject(getIntent().getStringExtra(SERVICE_DATA));
            Log.d("@@SERVICE_DATA_PAY", serviceData.getString(TITLE));
            params.put(TITLE, serviceData.getString(TITLE));
            params.put(PAYMENT_DESCRIPTION, serviceData.getString(PAYMENT_DESCRIPTION));
            for (int i = 0; i < serviceData.getJSONArray(PAYMENT_FILE).length(); i++) {
                imageFilesMap.put("" + i, serviceData.getJSONArray(PAYMENT_FILE).getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }


    private void getEcData() {
        try {
            serviceData = new JSONObject(getIntent().getStringExtra(SERVICE_DATA));
            //   Log.d("@@SERVICE_DATA_PAY", serviceData.getString(TITLE));
            params.put(MEETING_DATE_1, serviceData.getString(MEETING_DATE_1));
            params.put(MEETING_DATE_2, serviceData.getString(MEETING_DATE_2));
            params.put(MEETING_DATE_3, serviceData.getString(MEETING_DATE_3));
            params.put(TIME_ZONE_1, serviceData.getString(TIME_ZONE_1));
            params.put(TIME_ZONE_2, serviceData.getString(TIME_ZONE_2));
            params.put(TIME_ZONE_3, serviceData.getString(TIME_ZONE_3));
            params.put(COM_MODE, serviceData.getString(COM_MODE));
            params.put(COUNTRY_CODE, serviceData.getString(COUNTRY_CODE));
            params.put(CONTACT_DETAIL, serviceData.getString(CONTACT_DETAIL));
            params.put(TITLE, serviceData.getString(TITLE));
            params.put(PAYMENT_DESCRIPTION, serviceData.getString(PAYMENT_DESCRIPTION));
            for (int i = 0; i < serviceData.getJSONArray(PAYMENT_FILE).length(); i++) {
                imageFilesMap.put("" + i, serviceData.getJSONArray(PAYMENT_FILE).getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void getMhtData() {
        try {
            serviceData = new JSONObject(getIntent().getStringExtra(SERVICE_DATA));
            //   Log.d("@@SERVICE_DATA_PAY", serviceData.getString(TITLE));
            params.put(FIRST_APPOINTMENT, serviceData.getString(FIRST_APPOINTMENT));
            params.put(PAYMENT_DESCRIPTION, serviceData.getString(PAYMENT_DESCRIPTION));
            for (int i = 0; i < serviceData.getJSONArray(PAYMENT_FILE).length(); i++) {
                imageFilesMap.put("" + i, serviceData.getJSONArray(PAYMENT_FILE).getString(i));
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    private void setPageForUg() {
        servicNameTv.setText("Upgrade Gold Membership");
        serviceTypeRg.setVisibility(View.GONE);
    }


    private void setTotalAmount() {
        totalAmount = consultRate + subcriptionRate;
        totalAmountTv.setText("$ " + totalAmount + " USD");
    }


    private void getTokenAndPay() {
        showLoadingDialog(Payment.this);
        JSONObject params = new JSONObject();

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Token");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Payment.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        token = response.getString(RESULTS);
                        DropInRequest dropInRequest = new DropInRequest()
                                .clientToken(token)
                                .amount(String.valueOf(totalAmount))
                                .requestThreeDSecureVerification(true)
                                .collectDeviceData(true);
                        startActivityForResult(dropInRequest.getIntent(Payment.this), REQUEST_CODE);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE1", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);

    }


    private void payForService() {
        showLoadingDialog(Payment.this);
        switch (serviceType) {
            case QC:
                getQcData();
                break;
            case EC:
                getEcData();
                break;
            case MHT:
                getMhtData();
                break;
        }

        params.put(USER_ID, getUserId());
        params.put(AMOUNT, String.valueOf(totalAmount));
        params.put(TOKEN, token);

        if (consultRate == goldRate && !getIsMember()) {
            params.put(YEAR, subcriptionYear);
            params.put(MEMBERSHIP, serviceType + "_Premium");
        } else {
            params.put(MEMBERSHIP, serviceType);
        }
        if (serviceType.equalsIgnoreCase(UG)) {
            params.put(MEMBERSHIP, "Premium");
        }

        Log.d("System.out:==", params.toString());
        Log.d("System.out:==", SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_FIREBASE_REGID, "no_id"));

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "PaymentProcess");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Payment.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        if (TextUtils.isEmpty(response.getString("ErrorMessage"))) {
                            saveDataAndRedirect();
                        } else {
                            Toast.makeText(Payment.this, response.getString("ErrorMessage"), Toast.LENGTH_LONG).show();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {
                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueueMultipart(requestParams, imageFilesMap, PAYMENT_FILE, true);

    }


    private void saveDataAndRedirect() {
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
            params.put(DEVICE_TOKEN, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_FIREBASE_REGID, "no_id"));
            params.put(DEVICE_TYPE, ANDROID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "AutoLogin");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Payment.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                if (responseCode == 200) {
                    try {
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_NAME, response.getJSONObject(RESULTS).getString(USER_NAME));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(IS_MEMBER, response.getJSONObject(RESULTS).getBoolean(IS_MEMBER));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, response.getJSONObject(RESULTS).toString());
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(HP_COMPLETED, response.getJSONObject(RESULTS).getBoolean(HP_COMPLETED));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(PROFILE_IMAGE, response.getJSONObject(RESULTS).getString(PROFILE_IMAGE));

                        Toast.makeText(Payment.this, "Payment Successful", Toast.LENGTH_LONG).show();
                        startActivity(new Intent(Payment.this, Home.class));

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onResponseError() {
                startActivity(new Intent(Payment.this, Login.class));
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE) {
            if (resultCode == Activity.RESULT_OK) {
                DropInResult result = data.getParcelableExtra(DropInResult.EXTRA_DROP_IN_RESULT);
                PaymentMethodNonce nonce = result.getPaymentMethodNonce();
                String stringNonce = nonce.getNonce();
                Log.i("XXXXXXXXXXX==", stringNonce);
                token = stringNonce;
                Log.i("@@PAYMENT_TOKEN", token);
                payForService();
            } else if (resultCode == Activity.RESULT_CANCELED) {
                // the user canceled
                Log.i("XXXXXXXXXXX", "CANCELLED");

            } else {
                // handle errors here, an exception may be available in
                Exception error = (Exception) data.getSerializableExtra(DropInActivity.EXTRA_ERROR);
                Log.i("XXXXXXXXXXX", error.getMessage());
                Toast.makeText(Payment.this, error.getMessage(), Toast.LENGTH_LONG).show();
            }
        }
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
