package com.bho.bho.Activities.SideMenuOptions;

import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bho.R;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.EntriesObject;
import com.bho.smart.customViews.RoundedImageView;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class Notifications extends MasterActivity {

    RecyclerView notificationsRv;
    NotificationsAdapter notificationsAdapter;
    JSONArray notificationJson = new JSONArray();

    @Override
    public int getLayoutID() {
        return R.layout.activity_notifications;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Notifications");

        notificationsRv = findViewById(R.id.notifications_rv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(Notifications.this);
        notificationsRv.setLayoutManager(linearLayoutManager);

        notificationsAdapter = new NotificationsAdapter();
        notificationsRv.setAdapter(notificationsAdapter);

        getNotification();
    }


    private void getNotification() {
        SmartUtils.showLoadingDialog(Notifications.this);
        final JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Notification");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Notifications.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        notificationJson = response.getJSONArray(RESULTS);
                        notificationsAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(Notifications.this).addToRequestQueue(requestParams, false);
    }


    public class NotificationsAdapter extends RecyclerView.Adapter<NotificationsAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_notification, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.titleNotificationIv.setText(notificationJson.getJSONObject(position).getString(TITLE));
                holder.descriptionNotificationIv.setText(notificationJson.getJSONObject(position).getString(NOTIFICATION_DESC));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        @Override
        public int getItemCount() {
            return notificationJson.length();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private RoundedImageView imageNotificationIv;
            private SmartTextView titleNotificationIv;
            private SmartTextView descriptionNotificationIv;


            public ViewHolder(View itemView) {
                super(itemView);
                imageNotificationIv = itemView.findViewById(R.id.image_notification_iv);
                titleNotificationIv = itemView.findViewById(R.id.title_notification_iv);
                descriptionNotificationIv = itemView.findViewById(R.id.description_notification_iv);

            }
        }
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
