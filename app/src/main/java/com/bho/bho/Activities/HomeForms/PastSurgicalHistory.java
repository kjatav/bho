package com.bho.bho.Activities.HomeForms;

import android.content.ContentValues;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Adapters.AdditionalFieldsAdapter;
import com.bho.bho.Adapters.MedicalItemsAdapters;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;
import static java.util.jar.Pack200.Unpacker.TRUE;

public class PastSurgicalHistory extends MasterActivity {

    private RecyclerView surgicalHistoryRv;
    private RecyclerView fieldsSurgicalHistoryRv;

    private SmartTextView addSurgicalHistoryTv;
    private SmartEditText addSurgicalHistoryEt;
    private SmartTextView submitSurgicalHistoryTv;

    private MedicalItemsAdapters medicalItemsAdapters;
    private AdditionalFieldsAdapter additionalFieldsAdapter;

    private SmartCaching smartCaching;

    private ArrayList<String> medicalExtraHistoryData = new ArrayList<>();
    private ArrayList<JSONObject> medicalHistoryData = new ArrayList<>();

    private boolean isUpdated = false;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_past_surgical_history;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Past Surgical History");
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        surgicalHistoryRv = findViewById(R.id.surgical_history_rv);
        fieldsSurgicalHistoryRv = findViewById(R.id.fields_surgical_history_rv);
        addSurgicalHistoryTv = findViewById(R.id.add_surgical_history_tv);
        addSurgicalHistoryEt = findViewById(R.id.add_surgical_history_et);
        submitSurgicalHistoryTv = findViewById(R.id.submit_surgical_history_tv);

        GridLayoutManager gridLayoutManager = new GridLayoutManager(PastSurgicalHistory.this, 2);
        surgicalHistoryRv.setLayoutManager(gridLayoutManager);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(PastSurgicalHistory.this);
        fieldsSurgicalHistoryRv.setLayoutManager(linearLayoutManager);


        medicalItemsAdapters = new MedicalItemsAdapters(PastSurgicalHistory.this, medicalHistoryData);
        additionalFieldsAdapter = new AdditionalFieldsAdapter(PastSurgicalHistory.this, medicalExtraHistoryData);

        surgicalHistoryRv.setAdapter(medicalItemsAdapters);
        fieldsSurgicalHistoryRv.setAdapter(additionalFieldsAdapter);

        smartCaching = new SmartCaching(this);
        getDataFromDataBase();
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        addSurgicalHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(addSurgicalHistoryEt.getText().toString().trim())) {
                    additionalFieldsAdapter.addAdditionalFields(addSurgicalHistoryEt.getText().toString().trim());
                    addSurgicalHistoryEt.setText("");
                    isUpdated = true;
                }
            }
        });

        submitSurgicalHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (isUpdated || additionalFieldsAdapter.giveUpadateStautus() || medicalItemsAdapters.giveMedicalItemUpdateStatus()) {
                    putDataIntoDatabaseAndSync(viewToJson());
                } else {
                    startActivity(new Intent(PastSurgicalHistory.this, Home.class));
                }
            }
        });
    }

      /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getPastSurgicalHistory() {
        SmartUtils.showLoadingDialog(PastSurgicalHistory.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetSurgicalHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PastSurgicalHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    try {
                        //   noDataFound.setVisibility(View.GONE);

                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(USER_ID, getUserId());
                        jsonObject.put(IS_SYNCED, Boolean.TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_SURGICAL_HISTORY, OTHER_HISTORY, HISTORY);
                        String query = "SELECT * FROM " + TABLE_SURGICAL_HISTORY + " WHERE userId = " + getUserId();

                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_SURGICAL_HISTORY, query).get(0);
                        Log.d("@@@MedicalHistory_data", dataFromDatabase.getAsString("otherHistory"));
                        Log.d("@@@MedicalHistory_data2", dataFromDatabase.getAsString("History"));


                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    private void updateSurgicalPastHistory(JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(PastSurgicalHistory.this);
        try {
            params.put(HISTORY, medicalItemsAdapters.getCheckedItemsIds());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdatePastSurgicalHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PastSurgicalHistory.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {

                    smartCaching.updateSyncStatus(TABLE_SURGICAL_HISTORY, getUserId(), true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(PastSurgicalHistory.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(PastSurgicalHistory.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


        /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_SURGICAL_HISTORY + " WHERE userId = " + getUserId();
        if (smartCaching.getDataFromCache(TABLE_SURGICAL_HISTORY, query) != null && smartCaching.getDataFromCache(TABLE_SURGICAL_HISTORY, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_SURGICAL_HISTORY, query).get(0);
            Log.d("@@@cachedData", dataFromDatabase.toString());
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_SURGICAL_HISTORY, getUserId())) {
                getPastSurgicalHistory();
            } else {
                updateSurgicalPastHistory(viewToJson(), false);
            }
        } else {
            getPastSurgicalHistory();
        }
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            Log.d("@@CHECKED_ITEMS_IDS", medicalItemsAdapters.getCheckedItemsIds().toString());
            Log.d("@@CHECKED_ITEMS", medicalItemsAdapters.getCheckedItems());
            Log.d("@@ADDITIONAL_ITEMS", additionalFieldsAdapter.getAdditionalFields().toString());

            params.put(USER_ID, getUserId());
            params.put(HISTORY, medicalItemsAdapters.getCheckedItems());
            params.put(OTHER_HISTORY, additionalFieldsAdapter.getAdditionalFields());
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_SURGICAL_HISTORY, HISTORY, OTHER_HISTORY);
//        Log.d("@@SYNC_STATUS", String.valueOf(smartCaching.getSyncStatus(TABLE_SURGICAL_HISTORY, getUserId())));
        if (!smartCaching.getSyncStatus(TABLE_SURGICAL_HISTORY, getUserId())) {
            updateSurgicalPastHistory(data, true);
        }
    }


    private void setData(ContentValues data) {
        medicalHistoryData.clear();
        medicalExtraHistoryData.clear();
        try {
            JSONArray medicalHistoryJson = new JSONArray(data.getAsString(HISTORY));
            JSONArray otherHistoryJson = new JSONArray(data.getAsString(OTHER_HISTORY));

            for (int i = 0; i < medicalHistoryJson.length(); i++) {
                medicalHistoryData.add(medicalHistoryJson.getJSONObject(i));
            }
            for (int i = 0; i < otherHistoryJson.length(); i++) {
                medicalExtraHistoryData.add(otherHistoryJson.getString(i));
            }
            additionalFieldsAdapter.notifyDataSetChanged();
            medicalItemsAdapters.notifyDataSetChanged();
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
