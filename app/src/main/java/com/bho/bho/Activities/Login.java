package com.bho.bho.Activities;

import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.Snackbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;

import com.bho.R;
import com.bho.bho.MasterActivity;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartActivity;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.focusEditTextRed;
import static com.bho.smart.framework.SmartUtils.focusEditTextRed3;
import static java.util.jar.Pack200.Packer.FALSE;
import static java.util.jar.Pack200.Unpacker.TRUE;

public class Login extends MasterActivity {

    private Dialog forgetEmailDialog, forgetPasswordDialog;

    private ImageView backLoginIv;
    private SmartEditText emailLoginEt;
    private SmartEditText passwordLoginEt;
    private SmartTextView loginTv;
    private CheckBox rememberMeCb;
    private SmartTextView registerLoginTv;
    private SmartTextView forgetPasswordTv;
    private SmartTextView forgetEmailTv;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_login;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        backLoginIv = findViewById(R.id.back_login_iv);
        emailLoginEt = findViewById(R.id.email_login_et);
        passwordLoginEt = findViewById(R.id.password_login_et);
        loginTv = findViewById(R.id.login_tv);
        rememberMeCb = findViewById(R.id.remember_me_cb);
        registerLoginTv = findViewById(R.id.register_login_tv);
        forgetPasswordTv = findViewById(R.id.forget_password_tv);
        forgetEmailTv = findViewById(R.id.forget_email_tv);

        if (SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(REMEMBER_ME, FALSE).equalsIgnoreCase(TRUE)) {
            emailLoginEt.setText(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_EMAIL, ""));
            passwordLoginEt.setText(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_PASSWORD, ""));
            rememberMeCb.setChecked(true);
        }
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        backLoginIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        loginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(emailLoginEt.getText().toString().trim())) {
                    focusEditTextRed3(Login.this, emailLoginEt, true, "Please enter your email");
                } else if (TextUtils.isEmpty(passwordLoginEt.getText().toString().trim())) {
                    focusEditTextRed(Login.this, passwordLoginEt, true, "Please enter your password", 0);
                } else {
                    login(emailLoginEt.getText().toString().trim(), passwordLoginEt.getText().toString().trim());
                }
            }
        });

        registerLoginTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(Login.this, Register.class));
            }
        });

        forgetPasswordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showforgetPasswordDialog();
            }
        });


        forgetEmailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showforgetEmailDialog();
            }
        });
    }


    private void showforgetEmailDialog() {
        forgetEmailDialog = new Dialog(Login.this);
        forgetEmailDialog.setContentView(R.layout.dialog_forgot_email);

        final SmartEditText recoveryEmailEt = forgetEmailDialog.findViewById(R.id.recovery_email_et);
        SmartTextView submitForgetEmailTv = forgetEmailDialog.findViewById(R.id.submit_forget_email_tv);
        ImageView closeForgetEmailIv = forgetEmailDialog.findViewById(R.id.close_forget_email_iv);

        submitForgetEmailTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(recoveryEmailEt.getText().toString().trim())) {
                    focusEditTextRed3(Login.this, recoveryEmailEt, true, "Please enter your recovery email address");
                } else {
                    resetEmail(recoveryEmailEt.getText().toString().trim());
                }
            }
        });

        closeForgetEmailIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetEmailDialog.dismiss();
            }
        });

        forgetEmailDialog.show();
    }


    private void showforgetPasswordDialog() {
        forgetPasswordDialog = new Dialog(Login.this);
        forgetPasswordDialog.setContentView(R.layout.dialog_forgot_password);


        final SmartEditText passwordForgetPasswordEt = forgetPasswordDialog.findViewById(R.id.password_forget_password_et);
        SmartTextView submitForgetPasswordTv = forgetPasswordDialog.findViewById(R.id.submit_forget_password_et);
        ImageView closeForgetEmailDialog = forgetPasswordDialog.findViewById(R.id.close_forget_password_iv);

        submitForgetPasswordTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TextUtils.isEmpty(passwordForgetPasswordEt.getText().toString().trim())) {
                    focusEditTextRed3(Login.this, passwordForgetPasswordEt, true, "Please enter your email address");
                } else {
                    resetPassword(passwordForgetPasswordEt.getText().toString().trim());
                }

            }
        });

        closeForgetEmailDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                forgetPasswordDialog.dismiss();
            }
        });

        forgetPasswordDialog.show();
    }


    private void login(String userName, final String password) {
        SmartUtils.showLoadingDialog(Login.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_NAME, userName);
            params.put(PASSWORD, password);
            params.put(DEVICE_TOKEN, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_FIREBASE_REGID, "no_id"));
            params.put(DEVICE_TYPE, ANDROID);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Login");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Login.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {


                    try {
                        if (rememberMeCb.isChecked()) {
                            SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(REMEMBER_ME, TRUE);
                            SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_EMAIL, emailLoginEt.getText().toString().trim());
                            SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_PASSWORD, passwordLoginEt.getText().toString().trim());
                        } else {
                            SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(REMEMBER_ME, FALSE);
                        }
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_NAME, response.getJSONObject(RESULTS).getString(USER_NAME));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_ID, response.getJSONObject(RESULTS).getString(USER_ID));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(IS_MEMBER, response.getJSONObject(RESULTS).getBoolean(IS_MEMBER));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(HP_COMPLETED, response.getJSONObject(RESULTS).getBoolean(HP_COMPLETED));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(PROFILE_IMAGE, response.getJSONObject(RESULTS).getString(PROFILE_IMAGE));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, response.getJSONObject(RESULTS).toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }


                    startActivity(new Intent(Login.this, Home.class));
                    supportFinishAfterTransition();


                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }


    private void resetEmail(String recoveryEmail) {
        SmartUtils.showLoadingDialog(Login.this);
        JSONObject params = new JSONObject();
        try {
            params.put(RECOVERY_NAME, recoveryEmail);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "ForgetUserName");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Login.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@EMAIL_RESET", "===success");
                    forgetEmailDialog.dismiss();
                    SmartUtils.showSnackBar(Login.this, "Check your recovery email to reset username", Snackbar.LENGTH_LONG);
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }


    private void resetPassword(String email) {
        SmartUtils.showLoadingDialog(Login.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_NAME, email);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "ForgetPassword");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Login.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@PASSWORD_RESET", "===success");
                    forgetPasswordDialog.dismiss();
                    SmartUtils.showSnackBar(Login.this, "Check your email to reset password", Snackbar.LENGTH_LONG);
                }
            }

            @Override
            public void onResponseError() {
                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);
    }

    public void setUser(View v) {
       /* emailLoginEt.setText("karan.ebiztrait@gmail.com");
        passwordLoginEt.setText("99999999");*/
    }


}
