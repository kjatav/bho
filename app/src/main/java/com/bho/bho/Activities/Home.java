package com.bho.bho.Activities;

import android.content.ContentValues;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;

import com.bho.R;
import com.bho.bho.Activities.HomeForms.PersonalInformation;
import com.bho.bho.Fragments.BhoService;
import com.bho.bho.Fragments.HealthProfile;
import com.bho.bho.Fragments.PaymentHistory;
import com.bho.bho.MasterActivity;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.AlertMagnatic;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class Home extends MasterActivity {


    private ViewPager mViewPager;
    private SmartTextView healthProfileTv;
    private SmartTextView bhoServiceTv;
    private SmartTextView paymentHistoryTv;
    private SmartCaching smartCaching;

    @Override
    public int getLayoutID() {
        return R.layout.activity_home;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        mViewPager = findViewById(R.id.home_viewpager);
        healthProfileTv = findViewById(R.id.health_profile_tv);
        bhoServiceTv = findViewById(R.id.bho_service_tv);
        paymentHistoryTv = findViewById(R.id.payment_history_tv);
        FirebaseApp.initializeApp(Home.this);
        //  Log.i("XXXXXXXXXXXXXX","==>"+FirebaseInstanceId.getInstance().getToken());
        smartCaching = new SmartCaching(this);

        mViewPager.setOffscreenPageLimit(3);
        setupViewPager(mViewPager);
        setupTabIcons();
        selectHome();


    }


    //--VIEW PAGER SETUP---------------------------------------------------------------------------------------------------------------------------------------------------------------------


    private void setupTabIcons() {
        mViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                switch (position) {
                    case 0:
                        selectHome();
                        break;
                    case 1:
                        selectOrderHistory();
                        break;
                    case 2:
                        selectProfile();
                        break;
                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
        healthProfileTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectHome();
            }
        });
        bhoServiceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectOrderHistory();
            }
        });
        paymentHistoryTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectProfile();
            }
        });
    }


    public void selectHome() {
        mViewPager.setCurrentItem(0);

        healthProfileTv.setBackgroundColor(Color.parseColor("#FE6E30"));
        bhoServiceTv.setBackgroundColor(Color.parseColor("#314150"));
        paymentHistoryTv.setBackgroundColor(Color.parseColor("#314150"));

        setHeaderToolbar(getString(R.string.health_profile));
    }


    public void selectOrderHistory() {
        mViewPager.setCurrentItem(1);

        healthProfileTv.setBackgroundColor(Color.parseColor("#314150"));
        bhoServiceTv.setBackgroundColor(Color.parseColor("#FE6E30"));
        paymentHistoryTv.setBackgroundColor(Color.parseColor("#314150"));

        setHeaderToolbar(getString(R.string.bho_services));
    }


    public void selectProfile() {
        mViewPager.setCurrentItem(2);

        healthProfileTv.setBackgroundColor(Color.parseColor("#314150"));
        bhoServiceTv.setBackgroundColor(Color.parseColor("#314150"));
        paymentHistoryTv.setBackgroundColor(Color.parseColor("#FE6E30"));

        setHeaderToolbar(getString(R.string.payment_history));
    }


    private void setupViewPager(ViewPager viewPager) {

        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());

        adapter.addFragment(new HealthProfile(), "");
        adapter.addFragment(new BhoService(), "");
        adapter.addFragment(new PaymentHistory(), "");
        viewPager.setAdapter(adapter);
    }


    private class ViewPagerAdapter extends FragmentPagerAdapter {

        private List<Fragment> mFragmentList = new ArrayList<>();
        private List<String> mFragmentTitleList = new ArrayList<>();

        public ViewPagerAdapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }

        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }


        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.d("@requestCode", String.valueOf(requestCode));
       /* if (mViewPager.getCurrentItem() == 0) {
            if (transporterFormFragment != null) {
                transporterFormFragment.onActivityResult(requestCode, resultCode, data);
            }
        }
        if (mViewPager.getCurrentItem() == 2) {
            if (profileFragment != null) {
                profileFragment.onActivityResult(requestCode, resultCode, data);
            }
        }*/
    }

    private void uploadImage() {
        SmartUtils.showLoadingDialog(Home.this);

        Map<String, String> params = new HashMap<>();
        params.put(USER_ID, getUserId());

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UploadProfilePic");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, Home.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                   /* try {
                        String imageUrl = response.getString(RESULTS);
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(PROFILE_IMAGE, imageUrl);
                        Picasso.with(Home.this).load(imageUrl).placeholder(R.drawable.upload_placeholder).into(userImagePiIv);
                        setNavDetails(imageUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }*/
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueueMultipart(requestParams, getFilesDir().toString() + "/profileImage/profileImage.jpg", true);

    }


    @Override
    public void onBackPressed() {
        SmartUtils.getConfirmDialog(Home.this, "Quit App", "Are you sure you want to quit the app?",
                "Yes", "No", true, new AlertMagnatic() {
                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        finishAffinity();
                    }

                    @Override
                    public void NegativeMethod(DialogInterface dialog, int id) {

                    }
                });
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
