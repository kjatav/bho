package com.bho.bho.Activities.LandingScreens;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.webkit.WebResourceResponse;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.Activities.BHOServices.MyChart;
import com.bho.bho.LandingBaseActivity;
import com.bho.bho.MasterActivity;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartUtils;

import static com.bho.smart.framework.SmartUtils.getUserId;

public class OurServices extends LandingBaseActivity {

    private LinearLayout serviceTabsLl;
    private SmartTextView qcTabTv;
    private SmartTextView ecTabTv;
    private SmartTextView myChartTabTv;
    private SmartTextView myHealthTrendTabTv;
    private SmartTextView serviceNameTv;
    private SmartTextView serviceTextTv;
    private SmartTextView serviceDetailsTv;

    private LinearLayout videoBtn;
    private SmartTextView videoTv;


    @Override
    public int getLayoutID() {
        return R.layout.activity_our_services;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        setHeaderToolbar("Our Services");

        serviceTabsLl = findViewById(R.id.service_tabs_ll);
        qcTabTv = findViewById(R.id.qc_tab_tv);
        ecTabTv = findViewById(R.id.ec_tab_tv);
        myChartTabTv = findViewById(R.id.my_chart_tab_tv);
        myHealthTrendTabTv = findViewById(R.id.my_health_trend_tab_tv);
        serviceNameTv = findViewById(R.id.service_name_tv);
        serviceTextTv = findViewById(R.id.service_text_tv);
        serviceDetailsTv = findViewById(R.id.service_details_tv);

        videoBtn = findViewById(R.id.video_btn);
        videoTv = findViewById(R.id.video_tv);


        setTab(QC, qcTabTv, "QuickConsult :-", "ASK A DOCTOR ", "Do you have a medical problem that requires an immediate opinion from a world-class medical expert? QC may well be what you need! By using your computer or a portable smart device, and logging into the BlackHealthOnline (BHO) web portal, or through the BHO app, you can upload relevant medical records and ask our doctors questions to get very quick answers for a small fee. BHO offers the QC service to all its members. Registration makes one a Silver member. Payment of an annual subscription upgrades one to a Gold member. BHO Gold members are entitled to a 30% discount each time they request a QuickConsult.",
                "QC Video");
    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();

        qcTabTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(QC, qcTabTv, "QuickConsult :-", "ASK A DOCTOR ", "Do you have a medical problem that requires an immediate opinion from a world-class medical expert? QC may well be what you need! By using your computer or a portable smart device, and logging into the BlackHealthOnline (BHO) web portal, or through the BHO app, you can upload relevant medical records and ask our doctors questions to get very quick answers for a small fee. BHO offers the QC service to all its members. Registration makes one a Silver member. Payment of an annual subscription upgrades one to a Gold member. BHO Gold members are entitled to a 30% discount each time they request a QuickConsult.",
                        "QC Video");

            }
        });

        ecTabTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(EC, ecTabTv, "ExpandedConsult :-", "SEE A DOCTOR", "Do you have an immediate need to see or talk to a doctor? EC is your solution! and this you can access by downloading a BlackHealthOnline (BHO) App or login to BlackHealthOnline.com using your computer or smart mobile device. By using your computer or a portable smart device, and logging into the BlackHealthOnline (BHO) web portal, or through the BHO app, you can upload relevant medical records, describe your medical problem(s), and schedule a 20-minute appointment to consult with a world-class North American or Western European trained physician through video chat, live chat, or audio chat for a low fee. BHO offers the EC service to all its members. Registration makes one a Silver member. Payment of an annual subscription upgrades one to a Gold member. BHO Gold members are entitled to a 30% discount each time they request a ExpandedConsult.",
                        "EC Video");
            }
        });

        myChartTabTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(MC, myChartTabTv, "MyChart (MC) :-", "MY ONLINE MEDICAL RECORD", "MyChart is an online repository of patient medical records that all registered BlackHealthOnline (BHO) members can access, for medical record storage, through the portal at anytime from anywhere around the world, as long they can access the Internet with a mobile device or a computer.\n" +
                                "\n" +
                                "In MyChart there are various folders including “Doctor’s report”, “EKG”, “Echocardiogram”, “Medications list”, “Vaccine records”, “CAT scan”, etc. Through MyChart, BHO members are able to upload and store their medical records securely on the BHO portal, and can also periodically update the stored records by uploading new information to the appropriate folder(s) as it becomes available to them.\n" +
                                "\n" +
                                "Using MyChart, BHO members are also able to keep track of their drug allergies, medications list, vaccination records, lab reports, prior visits’ records from their various doctors, x-ray reports, prior EKG reports, etc. All these are relevant data that can positively impact safety as well as medical care costs to members if made readily available when needed by other health care providers. Encounters between the BHO members and any BHO physician that take place through QuickConsult or ExpandedConsult will generate a medical report that will be archived on MyChart in the BHO portal, ensuring continuity of care, and enabling the members to have future access to a secure record of their interactions with BHO physicians.",
                        "MC Video");
            }
        });

        myHealthTrendTabTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTab(MHT, myHealthTrendTabTv, "MyHealthTrends (MHT) :-", "KEEPS AN EYE ON YOU", "This is a comprehensive medical record review service offered by BlackHealthOnline (BHO). Through the MHT platform, BHO expert doctors are keeping an eye on your health through a six-monthly analysis of all the updated medical records that you provide to the site.\n" +
                                "\n" +
                                "MHT gives you peace of mind, knowing that our expert doctors are covering your back through a thorough review of all relevant medical records uploaded in our system. Using a team-based approach that the MHT process offers, our expert doctors will conduct a systematic review of your medical records, doctors’ reports, laboratory test results, imaging reports, and prescription records, consolidated in MyChart, in order to identify any dangerous medication interactions, or inconsistent treatment plans that our members are getting from different care locations. MHT's built-in surveillance system aims to identify emerging health issues such as cancer, diabetes, kidney disease, heart diseases, or other dangerous illnesses before they get out of control. BHO offers the MHT service to all its members, but BHO Gold members get a 30% discount for MyHealthTrends.",
                        "MHT Video");
            }
        });
    }


    private void setTab(final String ServiceType, SmartTextView textView, String serviceName, String serviceText, String serviceDetails, String videoText) {

        qcTabTv.setTextColor(getResources().getColor(R.color.black));
        ecTabTv.setTextColor(getResources().getColor(R.color.black));
        myChartTabTv.setTextColor(getResources().getColor(R.color.black));
        myHealthTrendTabTv.setTextColor(getResources().getColor(R.color.black));

        qcTabTv.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        ecTabTv.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        myChartTabTv.setBackground(getResources().getDrawable(R.drawable.trasparent_background));
        myHealthTrendTabTv.setBackground(getResources().getDrawable(R.drawable.trasparent_background));

        textView.setTextColor(getResources().getColor(R.color.white));
        textView.setBackground(getResources().getDrawable(R.drawable.bg_orange_tab));

        serviceNameTv.setText(serviceName);
        serviceTextTv.setText(serviceText);
        serviceDetailsTv.setText(serviceDetails);
        videoTv.setText(videoText);

        videoBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (ServiceType) {
                    case QC:
                        goToUrl("https://www.youtube.com/watch?v=067bVZiSWTo&feature=youtu.be");
                        break;
                    case EC:
                        goToUrl("https://www.youtube.com/watch?v=01bTYebvNEE&feature=youtu.be");
                        break;
                    case MC:
                        goToUrl("https://www.youtube.com/watch?v=u-Db6jWdi8A&feature=youtu.be");
                        break;
                    case MHT:
                        goToUrl("https://www.youtube.com/watch?v=cNry-qL-YDc&feature=youtu.be");
                        break;
                }
            }
        });

    }


    @SuppressLint("SetJavaScriptEnabled")
    private void goToUrl(String url) {
        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_sidemenu_icon);
    }
}
