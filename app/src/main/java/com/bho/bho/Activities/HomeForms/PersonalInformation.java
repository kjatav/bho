package com.bho.bho.Activities.HomeForms;

import android.app.Dialog;
import android.content.ContentValues;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Activities.SideMenuOptions.Testimonial.AddTestimonial;
import com.bho.bho.MasterActivity;
import com.bho.bho.POJO.CountryData;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.common.Object_Image;
import com.bho.smart.customViews.SmartEditText;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.darsh.multipleimageselect.activities.AlbumSelectActivity;
import com.darsh.multipleimageselect.helpers.Constants;
import com.darsh.multipleimageselect.models.Image;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;

import static com.bho.smart.framework.SmartUtils.focusEditTextRed;
import static com.bho.smart.framework.SmartUtils.getFileExtensionFromUrlSmart;
import static com.bho.smart.framework.SmartUtils.getImageFileUri;
import static com.bho.smart.framework.SmartUtils.getUserId;
import static java.lang.Boolean.FALSE;
import static java.lang.Boolean.TRUE;

public class PersonalInformation extends MasterActivity implements AdapterView.OnItemSelectedListener {

    private ImageView userImagePiIv;
    private SmartEditText firstNamePiEt;
    private SmartEditText lastNamePiEt;
    private Spinner dateDobPiSpn;
    private Spinner monthDobPiSpn;
    private Spinner yearDobPiSpn;
    private RadioGroup genderPiRg;
    private RadioButton malePiRb;
    private RadioButton femalePiRb;
    private RadioGroup maritalPiRg;
    private RadioButton singlePiRb;
    private RadioButton marriedPiRb;
    private RadioButton divorcedPiRb;
    private RadioButton widowedPiRb;
    private RadioButton separatedPiRb;
    private SmartEditText countryCodeEt;
    private SmartEditText mobileEt;
    private Spinner countryPiSpn;
    private SmartEditText statePiEt;
    private SmartEditText cityPiEt;
    private SmartEditText addressPiEt;
    private SmartEditText zipPiEt;
    private SmartEditText emailPiEt;
    private SmartEditText recoveryEmailPiEt;
    private SmartEditText occupationPiEt;
    private SmartEditText weightPiEt;
    private Spinner weightUnitPiSpn;
    private Spinner heightUnitPiSpn;
    private SmartEditText height1PiEt;
    private SmartEditText height2PiEt;
    private SmartTextView height1PiTv;
    private SmartTextView height2PiTv;
    private SmartTextView submitPiEt;

    private String weight_unit, height_unit, gender = "Male", maritalStatus = "Single";
    private ArrayList<CountryData> countryData = new ArrayList<>();
    private ArrayList<String> countryNames = new ArrayList<>();

    private ArrayList<String> dates = new ArrayList<>();
    private ArrayList<String> years = new ArrayList<>();

    private String countryId = "1";
    private String day = "1";
    private String month = "1";
    private String year = "1900";

    private SmartCaching smartCaching;

    private Uri currentImageUri;
    private String cameraImageName;
    private String selectedImagePath = "";


//    private boolean isUpdated = false;


    private ImageView height1UpBtn, height1DownBtn, height2UpBtn, height2DownBtn;

    @Override
    public int getDrawerLayoutID() {
        return 0;
    }

    @Override
    public int getLayoutID() {
        return R.layout.activity_personal_information;
    }

    @Override
    public void initComponents() {
        super.initComponents();
        drawerLayout.setScrimColor(getResources().getColor(android.R.color.transparent));

        setHeaderToolbar("Personal Information");

        userImagePiIv = findViewById(R.id.user_image_pi_iv);

        firstNamePiEt = findViewById(R.id.first_name_pi_et);
        lastNamePiEt = findViewById(R.id.last_name_pi_et);

        dateDobPiSpn = findViewById(R.id.date_dob_pi_spn);
        monthDobPiSpn = findViewById(R.id.month_dob_pi_spn);
        yearDobPiSpn = findViewById(R.id.year_dob_pi_spn);

        genderPiRg = findViewById(R.id.gender_pi_rg);
        malePiRb = findViewById(R.id.male_pi_rb);
        femalePiRb = findViewById(R.id.female_pi_rb);
        maritalPiRg = findViewById(R.id.marital_pi_rg);
        singlePiRb = findViewById(R.id.single_pi_rb);
        marriedPiRb = findViewById(R.id.married_pi_rb);
        divorcedPiRb = findViewById(R.id.divorced_pi_rb);
        widowedPiRb = findViewById(R.id.widowed_pi_rb);
        separatedPiRb = findViewById(R.id.separated_pi_rb);
        countryCodeEt = findViewById(R.id.country_code_et);
        mobileEt = findViewById(R.id.mobile_et);

        countryPiSpn = findViewById(R.id.country_pi_spn);

        statePiEt = findViewById(R.id.state_pi_et);
        cityPiEt = findViewById(R.id.city_pi_et);
        addressPiEt = findViewById(R.id.address_pi_et);
        zipPiEt = findViewById(R.id.zip_pi_et);
        emailPiEt = findViewById(R.id.email_pi_et);
        recoveryEmailPiEt = findViewById(R.id.recovery_email_pi_et);
        occupationPiEt = findViewById(R.id.occupation_pi_et);
        weightPiEt = findViewById(R.id.weight_pi_et);

        weightUnitPiSpn = findViewById(R.id.weight_unit_pi_spn);
        heightUnitPiSpn = findViewById(R.id.height_unit_pi_spn);

        height1PiEt = findViewById(R.id.height1_pi_et);
        height2PiEt = findViewById(R.id.height2_pi_et);
        height1PiTv = findViewById(R.id.height1_pi_tv);
        height2PiTv = findViewById(R.id.height2_pi_tv);
        submitPiEt = findViewById(R.id.submit_pi_et);

        height1UpBtn = findViewById(R.id.height1_up_pi_btn);
        height1DownBtn = findViewById(R.id.height1_down_pi_btn);
        height2UpBtn = findViewById(R.id.height2_up_pi_btn);
        height2DownBtn = findViewById(R.id.height2_down_pi_btn);


        smartCaching = new SmartCaching(this);

        malePiRb.setChecked(true);
        singlePiRb.setChecked(true);

        dateDobPiSpn.setOnItemSelectedListener(this);
        monthDobPiSpn.setOnItemSelectedListener(this);
        yearDobPiSpn.setOnItemSelectedListener(this);
        countryPiSpn.setOnItemSelectedListener(this);
        weightUnitPiSpn.setOnItemSelectedListener(this);
        heightUnitPiSpn.setOnItemSelectedListener(this);


        for (int i = Calendar.getInstance().get(Calendar.YEAR); i > 1900; i--) {
            years.add(String.valueOf(i));
        }

        ArrayAdapter<String> yearAdapter = new ArrayAdapter<>(PersonalInformation.this, android.R.layout.simple_spinner_item, years);
        yearAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        yearDobPiSpn.setAdapter(yearAdapter);

        getAndSetImage();
        getDataFromDataBase();
        setCountries();


    }

    @Override
    public void setActionListeners() {
        super.setActionListeners();
        userImagePiIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openImageDialog();
            }
        });

        genderPiRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
//                isUpdated = true;

                switch (checkedId) {
                    case R.id.male_pi_rb:
                        gender = "Male";
                        break;
                    case R.id.female_pi_rb:
                        gender = "Female";
                        break;
                }
            }
        });

        maritalPiRg.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                switch (checkedId) {
                    case R.id.single_pi_rb:
                        maritalStatus = SINGLE;
                        break;
                    case R.id.married_pi_rb:
                        maritalStatus = MARRIED;
                        break;
                    case R.id.divorced_pi_rb:
                        maritalStatus = DIVORCED;
                        break;
                    case R.id.widowed_pi_rb:
                        maritalStatus = WIDOWED;
                        break;
                    case R.id.separated_pi_rb:
                        maritalStatus = SEPARATED;
                        break;
                }
            }
        });


        height1UpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                isUpdated = true;
                height1PiEt.setText(String.valueOf(Integer.parseInt(height1PiEt.getText().toString().trim()) + 1));
            }
        });

        height1DownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(height1PiEt.getText().toString().trim()) - 1 > 0) {
//                    isUpdated = true;
                    height1PiEt.setText(String.valueOf(Integer.parseInt(height1PiEt.getText().toString().trim()) - 1));
                }
            }
        });

        height2UpBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                isUpdated = true;
                height2PiEt.setText(String.valueOf(Integer.parseInt(height2PiEt.getText().toString().trim()) + 1));
            }
        });

        height2DownBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (Integer.parseInt(height2PiEt.getText().toString().trim()) - 1 > 0) {
//                    isUpdated = true;
                    height2PiEt.setText(String.valueOf(Integer.parseInt(height2PiEt.getText().toString().trim()) - 1));
                }
            }
        });


        submitPiEt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (check()) {
                    putDataIntoDatabaseAndSync(viewToJson());
                }
            }
        });

    }


    /*------------------APIs START-----------------------------------------------------------------------------------------------------------*/


    private void getPersonalInformation() {
        SmartUtils.showLoadingDialog(PersonalInformation.this);
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        Log.d("@@Request_JSON", params.toString());

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "GetPersonalinformation");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PersonalInformation.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@PI_success", ": true");

                    try {
                        JSONObject jsonObject = response.getJSONObject(RESULTS);
                        jsonObject.put(IS_SYNCED, TRUE);
                        smartCaching.cacheResponse(jsonObject, TABLE_PERSONAL_INFO, false);
                        String query = "SELECT * FROM " + TABLE_PERSONAL_INFO + " WHERE userId = " + getUserId();
                        ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_PERSONAL_INFO, query).get(0);
                        setData(dataFromDatabase);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }

    private void updatePersonalInformation(final JSONObject params, final boolean isSubmit) {
        SmartUtils.showLoadingDialog(PersonalInformation.this);

        Log.d("@@Request_JSON", params.toString());

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UpdatePersonalInformation");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PersonalInformation.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    Log.d("@@@PI_success", ": true");
                    smartCaching.updateSyncStatus(TABLE_PERSONAL_INFO, getUserId(), true);
                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(HP_COMPLETED, true);

                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });

        if (isSubmit) {
            Toast.makeText(PersonalInformation.this, "Your details have been updated", Toast.LENGTH_LONG).show();
            startActivity(new Intent(PersonalInformation.this, Home.class));
        }

        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }




    /*------------------APIs END-----------------------------------------------------------------------------------------------------------*/


    private void setCountries() {
        ArrayList<ContentValues> countryFromDatabase = smartCaching.getDataFromCache(TABLE_COUNTRY_DATA);
        if (countryFromDatabase != null) {
            for (int i = 0; i < countryFromDatabase.size(); i++) {
                countryData.add(new CountryData(countryFromDatabase.get(i).getAsString(COUNTRY_ID),
                        countryFromDatabase.get(i).getAsString(COUNTRY_NAME),
                        countryFromDatabase.get(i).getAsString(COUNTRY_CODE)));

                countryNames.add(countryFromDatabase.get(i).getAsString(COUNTRY_NAME));
            }

            ArrayAdapter<String> dataAdapter = new ArrayAdapter<>(PersonalInformation.this, android.R.layout.simple_spinner_item, countryNames);
            dataAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
            countryPiSpn.setAdapter(dataAdapter);
        }
    }

    private void getDataFromDataBase() {
        String query = "SELECT * FROM " + TABLE_PERSONAL_INFO + " WHERE userId = " + getUserId();

        if (smartCaching.getDataFromCache(TABLE_PERSONAL_INFO, query) != null && smartCaching.getDataFromCache(TABLE_PERSONAL_INFO, query).size() > 0) {

            ContentValues dataFromDatabase = smartCaching.getDataFromCache(TABLE_PERSONAL_INFO, query).get(0);
            setData(dataFromDatabase);
            if (smartCaching.getSyncStatus(TABLE_PERSONAL_INFO, getUserId())) {
                getPersonalInformation();
            } else {
                updatePersonalInformation(viewToJson(), false);
            }
        } else {
            getPersonalInformation();
        }
    }


    private void setData(ContentValues dataToSet) {

        firstNamePiEt.setText(dataToSet.getAsString(FIRST_NAME));
        lastNamePiEt.setText(dataToSet.getAsString(LAST_NAME));

        day = dataToSet.getAsString(DAY);
        dateDobPiSpn.setSelection(Integer.parseInt(dataToSet.getAsString(DAY)) - 1);
        monthDobPiSpn.setSelection(Integer.parseInt(dataToSet.getAsString(MONTH)) - 1);
        int j = 0;
        for (int i = Calendar.getInstance().get(Calendar.YEAR); i > 1900; i--) {
            if (Integer.parseInt(dataToSet.getAsString(YEAR)) == i) {
                yearDobPiSpn.setSelection(j);
            }
            j++;
        }

        switch (dataToSet.getAsString(GENDER)) {
            case MALE:
                gender = MALE;
                malePiRb.setChecked(true);
                break;

            case FEMALE:
                gender = FEMALE;
                femalePiRb.setChecked(true);
                break;
        }

        switch (dataToSet.getAsString(MARITAL_STATUS)) {
            case SINGLE:
                maritalStatus = SINGLE;
                singlePiRb.setChecked(true);
                break;

            case MARRIED:
                maritalStatus = MARRIED;
                marriedPiRb.setChecked(true);
                break;

            case DIVORCED:
                maritalStatus = DIVORCED;
                divorcedPiRb.setChecked(true);
                break;

            case WIDOWED:
                maritalStatus = WIDOWED;
                widowedPiRb.setChecked(true);
                break;

            case SEPARATED:
                maritalStatus = SEPARATED;
                separatedPiRb.setChecked(true);
                break;
        }
        countryCodeEt.setText(dataToSet.getAsString(COUNTRY_CODE));
        mobileEt.setText(dataToSet.getAsString(MOBILE_NUMBER));
        countryPiSpn.setSelection(Integer.parseInt(dataToSet.getAsString(COUNTRY_ID)) - 1);
        statePiEt.setText(dataToSet.getAsString(STATE));
        cityPiEt.setText(dataToSet.getAsString(CITY));
        addressPiEt.setText(dataToSet.getAsString(ADDRESS));
        zipPiEt.setText(dataToSet.getAsString(ZIP_CODE));
        emailPiEt.setText(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_NAME, NO_DATA));
        recoveryEmailPiEt.setText(dataToSet.getAsString(RECOVERY_EMAIL));
        occupationPiEt.setText(dataToSet.getAsString(OCCUPATION));
        weightPiEt.setText(dataToSet.getAsString(WEIGHT));
        switch (dataToSet.getAsString(WEIGHT_UNIT)) {
            case POUNDS:
                weight_unit = POUNDS;
                weightUnitPiSpn.setSelection(0);
                break;

            case KILOGRAM:
                weight_unit = KILOGRAM;
                weightUnitPiSpn.setSelection(1);
                break;
        }
        switch (dataToSet.getAsString(HEIGHT_UNIT)) {
            case FEET_INCHES:
                height_unit = FEET_INCHES;
                heightUnitPiSpn.setSelection(0);
                break;

            case METER_CENTIMETRE:
                height_unit = METER_CENTIMETRE;
                heightUnitPiSpn.setSelection(1);
                break;
        }
        height1PiEt.setText(dataToSet.getAsString(HEIGHT_1));
        height2PiEt.setText(dataToSet.getAsString(HEIGHT_2));
        // Toast.makeText(PersonalInformation.this, dataToSet.getAsString("userName"), Toast.LENGTH_LONG).show();
    }


    private JSONObject viewToJson() {
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
            params.put(FIRST_NAME, firstNamePiEt.getText().toString().trim());
            params.put(MIDDLE_NAME, "");
            params.put(LAST_NAME, lastNamePiEt.getText().toString().trim());
            params.put(DAY, day);
            params.put(YEAR, year);
            params.put(MONTH, month);
            params.put(GENDER, gender);
            params.put(MARITAL_STATUS, maritalStatus);
            params.put(MOBILE_NUMBER, mobileEt.getText().toString().trim());
            params.put(COUNTRY_CODE, countryCodeEt.getText().toString().trim());
            params.put(COUNTRY_ID, countryId);
            params.put(STATE, statePiEt.getText().toString().trim());
            params.put(CITY, cityPiEt.getText().toString().trim());
            params.put(ADDRESS, addressPiEt.getText().toString().trim());
            params.put(ZIP_CODE, zipPiEt.getText().toString().trim());
            params.put(RECOVERY_EMAIL, recoveryEmailPiEt.getText().toString().trim());
            params.put(OCCUPATION, occupationPiEt.getText().toString().trim());
            params.put(WEIGHT, weightPiEt.getText().toString().trim());
            params.put(WEIGHT_UNIT, weight_unit);
            params.put(HEIGHT_UNIT, height_unit);
            params.put(HEIGHT_1, height1PiEt.getText().toString().trim());
            params.put(HEIGHT_2, height2PiEt.getText().toString().trim());
            params.put(IS_SYNCED, FALSE);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return params;
    }


    private void putDataIntoDatabaseAndSync(JSONObject data) {
        smartCaching.cacheResponse(data, TABLE_PERSONAL_INFO, false);
        //  Log.d("@@SYNC_STATUS", smartCaching.getSyncStatus(TABLE_PERSONAL_INFO, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_ID, NO_USER_ID)));
        if (!smartCaching.getSyncStatus(TABLE_PERSONAL_INFO, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_ID, NO_USER_ID))) {
            updatePersonalInformation(data, true);
        }
    }


    private boolean check() {
        boolean isValid = true;

        if (TextUtils.isEmpty(firstNamePiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, firstNamePiEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(lastNamePiEt.getText().toString().trim())) {
            isValid = false;
            focusEditTextRed(PersonalInformation.this, lastNamePiEt, true, "Please enter mandatory fields with stars", 0);
        } else if (TextUtils.isEmpty(countryCodeEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, mobileEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(mobileEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, mobileEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (mobileEt.getText().toString().trim().length() < 8) {
            focusEditTextRed(PersonalInformation.this, mobileEt, true, "Mobile Number should be greater than 8 numbers", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(countryCodeEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, countryCodeEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(addressPiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, addressPiEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(emailPiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, emailPiEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (!SmartUtils.emailValidator(emailPiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, emailPiEt, true, "Please enter a valid email address", 0);
            isValid = false;
        } else if (TextUtils.isEmpty(recoveryEmailPiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, recoveryEmailPiEt, true, "Please enter mandatory fields with stars", 0);
            isValid = false;
        } else if (!SmartUtils.emailValidator(recoveryEmailPiEt.getText().toString().trim())) {
            focusEditTextRed(PersonalInformation.this, recoveryEmailPiEt, true, "Please enter a valid recovery email address", 0);
            isValid = false;
        }

        return isValid;
    }


    private void setDateDays() {
        dates.clear();
        Calendar mycal = new GregorianCalendar(Integer.valueOf(year), Integer.valueOf(month) - 1, 1);
        //  Log.d("Days in month===", month + "----" + mycal.getActualMaximum(Calendar.DAY_OF_MONTH));

        for (int i = 1; i <= mycal.getActualMaximum(Calendar.DAY_OF_MONTH); i++) {
            dates.add(String.valueOf(i));
        }

        ArrayAdapter<String> dateAdapter = new ArrayAdapter<>(PersonalInformation.this, android.R.layout.simple_spinner_item, dates);
        dateAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        dateDobPiSpn.setAdapter(dateAdapter);
        dateDobPiSpn.setSelection(Integer.valueOf(day) - 1);
    }


    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

        if (parent.getId() == R.id.country_pi_spn) {
            countryId = countryData.get(position).getCountryId();
            //   Toast.makeText(parent.getContext(), "Selected: " + countryData.get(position).getCountryName() + "-----" + countryId, Toast.LENGTH_LONG).show();
        } else if (parent.getId() == R.id.date_dob_pi_spn) {
            day = String.valueOf(position + 1);
        } else if (parent.getId() == R.id.month_dob_pi_spn) {
            month = String.valueOf(position + 1);
            setDateDays();
        } else if (parent.getId() == R.id.year_dob_pi_spn) {
            year = parent.getItemAtPosition(position).toString();
            setDateDays();
        } else if (parent.getId() == R.id.weight_unit_pi_spn) {
            weight_unit = parent.getItemAtPosition(position).toString();
        } else if (parent.getId() == R.id.height_unit_pi_spn) {
            height_unit = parent.getItemAtPosition(position).toString();
            if (position == 0) {
                height1PiTv.setText(FEET);
                height2PiTv.setText(INCHES);
            } else if (position == 1) {
                height1PiTv.setText(METER);
                height2PiTv.setText(CENTIMETRE);
            }
        }
    }


    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    private void uploadImage() {
        SmartUtils.showLoadingDialog(PersonalInformation.this);

        Map<String, String> params = new HashMap<>();
        params.put(USER_ID, getUserId());

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "UploadProfilePic");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, PersonalInformation.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        String imageUrl = response.getString(RESULTS);
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(PROFILE_IMAGE, imageUrl);
                        Picasso.with(PersonalInformation.this).load(imageUrl).placeholder(R.drawable.upload_placeholder).into(userImagePiIv);
                        setNavDetails(imageUrl);
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueueMultipart(requestParams, selectedImagePath, true);

    }

    private void getAndSetImage() {
        String imagePath = SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(PROFILE_IMAGE, NO_DATA);
        Picasso.with(PersonalInformation.this).load(imagePath).placeholder(R.drawable.upload_placeholder).into(userImagePiIv);
    }

    private void openImageDialog() {
        final Dialog dialogImageSource = new Dialog(PersonalInformation.this);
        dialogImageSource.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialogImageSource.setContentView(R.layout.dialog_imagesource);

        SmartTextView cameraBtn = dialogImageSource.findViewById(R.id.camera_btn);
        SmartTextView galleryBtn = dialogImageSource.findViewById(R.id.gallery_btn);
        cameraBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Object_Image object_image = getImageFileUri(PersonalInformation.this);
                currentImageUri = object_image.getImageUri();
                cameraImageName = object_image.getImageName();

                Intent intentPicture = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                intentPicture.putExtra(MediaStore.EXTRA_OUTPUT, currentImageUri); // set the image file name
                startActivityForResult(intentPicture, CAMERA_PROFILE_SOURCE);  // 1 for REQUEST_CAMERA and 2 for REQUEST_CAMERA_ATT
                dialogImageSource.dismiss();
            }
        });

        galleryBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(PersonalInformation.this, AlbumSelectActivity.class);
                intent.putExtra(Constants.INTENT_EXTRA_LIMIT, 1);
                startActivityForResult(intent, GALLERY_PROFILE_SOURCE);
                dialogImageSource.dismiss();
            }
        });
        dialogImageSource.show();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == RESULT_OK) {
            if (requestCode == CAMERA_PROFILE_SOURCE) {
                selectedImagePath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES) + "/" + cameraImageName;
                //Picasso.with(PersonalInformation.this).load(new File(selectedImagePath)).placeholder(R.drawable.upload_placeholder).into(userImagePiIv);
                uploadImage();
            } else if (requestCode == GALLERY_PROFILE_SOURCE) {
                ArrayList<Image> images = data.getParcelableArrayListExtra(Constants.INTENT_EXTRA_IMAGES);
                selectedImagePath = images.get(0).path;
                uploadImage();


              /*  StringBuilder kk = new StringBuilder();
                String fileName = "";
                String[] arrayStr = selectedImagePath.split("/");
                for (int i = 0; i < arrayStr.length; i++) {
                    Log.d("Images1--" + i + "---", arrayStr[i] + "--kk");
                    if (i != arrayStr.length - 1) {
                        kk.append("/").append(arrayStr[i]);
                    } else {
                        fileName = arrayStr[i];
                    }

                }
                Log.d("Images1--", kk.toString());
                Log.d("Images1--", fileName);
                copyFile(kk.toString(), fileName, getFilesDir().toString() + "/profileImage");*/

            }
        }
    }


    private void copyFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath + "/" + inputFile);
            getFileExtensionFromUrlSmart(inputPath + "/" + inputFile);
            out = new FileOutputStream(outputPath + "/profileImage.jpg");

            byte[] buffer = new byte[1024];
            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file (You have now copied the file)
            out.flush();
            out.close();
            out = null;

            Picasso.with(PersonalInformation.this).load(new File(outputPath + "/profileImage.jpg")).placeholder(R.drawable.upload_placeholder).into(userImagePiIv);

        } catch (Exception e) {
            Log.e("tag", e.getMessage());
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        overridePendingTransition(R.anim.slide_in_left, R.anim.slide_out_left);
    }


    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {
        super.manageAppBar(actionBar, toolbar, actionBarDrawerToggle);
        toolbar.setNavigationIcon(R.drawable.ic_back);
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });
    }
}
