package com.bho.bho;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.view.Window;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Activities.LandingScreens.AboutUsLanding;
import com.bho.bho.Activities.LandingScreens.HealthList;
import com.bho.bho.Activities.LandingScreens.HomeLanding;
import com.bho.bho.Activities.LandingScreens.OurServices;
import com.bho.bho.Activities.LandingScreens.OurTeam;
import com.bho.bho.Activities.Login;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartSuperMaster;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.google.android.gms.common.api.GoogleApiClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;


public abstract class LandingBaseActivity extends SmartSuperMaster {

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;


    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec

    private static int FATEST_INTERVAL = 5000; // 5 sec

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x8;

    private final int PERMISSIONS_REQUEST_CODE = 501;


    private SmartTextView headerToolbarTv;
    private SmartTextView skipToolbar;

    private SmartTextView homeLandingNav;
    private SmartTextView aboutLandingNav;
    private SmartTextView servicesLandingNav;
    private SmartTextView teamLandingNav;
    private SmartTextView profileNav;
    private SmartTextView healthcareConsumersLandingNav;
    private SmartTextView healthProfessionalsLandingNav;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setAnimations() {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setAllowEnterTransitionOverlap(true);
        getWindow().setAllowReturnTransitionOverlap(true);
    }

    @Override
    public void preOnCreate() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_CODE);
            return;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void initComponents() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        headerToolbarTv = findViewById(R.id.header_toolbar_tv);

        if (getDrawerLayoutID() != 0) {
            skipToolbar = findViewById(R.id.skip_toolbar);
            homeLandingNav = findViewById(R.id.home_landing_nav);
            aboutLandingNav = findViewById(R.id.about_landing_nav);
            servicesLandingNav = findViewById(R.id.services_landing_nav);
            teamLandingNav = findViewById(R.id.team_landing_nav);
            profileNav = findViewById(R.id.profile_nav);
            healthcareConsumersLandingNav = findViewById(R.id.healthcare_consumers_landing_nav);
            healthProfessionalsLandingNav = findViewById(R.id.health_professionals_landing_nav);
        }
    }

    @Override
    public void prepareViews() {
        if (getDrawerLayoutID() != 0) {
            JSONObject user = SmartUtils.getUser();
            if (user != null) {
                //setNavDetails(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(PROFILE_IMAGE, NO_DATA));
            }
        }
    }

    /*public void setNavDetails(String img_url) {
        if (!TextUtils.isEmpty(img_url))
            Picasso.with(this).load(img_url).placeholder(R.drawable.ic_profile_place_holder).into(userProfileNav);
        else {
            userProfileNav.setImageResource(R.drawable.ic_profile_place_holder);
        }
//        userNameNav.setText(first_name + " " + last_name);
    }
*/
    @Override
    public void setActionListeners() {
        if (getDrawerLayoutID() != 0) {
            skipToolbar.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    autoLogin();
                }
            });
            homeLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(LandingBaseActivity.this, HomeLanding.class));
                }
            });
            servicesLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(LandingBaseActivity.this, OurServices.class));
                }
            });
            aboutLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(LandingBaseActivity.this, AboutUsLanding.class));
                }
            });
            teamLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(LandingBaseActivity.this, OurTeam.class));
                }
            });
            healthcareConsumersLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(new Intent(LandingBaseActivity.this, HealthList.class));
                    i.putExtra(IS_CONSUMER, true);
                    startActivity(i);
                }
            });
            healthProfessionalsLandingNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(new Intent(LandingBaseActivity.this, HealthList.class));
                    i.putExtra(IS_CONSUMER, false);
                    startActivity(i);
                }
            });
        }
    }


    public void setHeaderToolbar(String heading) {
        headerToolbarTv.setText(heading);
    }


    protected void autoLogin() {
        JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
            params.put(DEVICE_TOKEN, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(SP_FIREBASE_REGID, "no_id"));
            params.put(DEVICE_TYPE, ANDROID);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "AutoLogin");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, LandingBaseActivity.this);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                if (responseCode == 200) {
                    try {
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_NAME, response.getJSONObject(RESULTS).getString(USER_NAME));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(IS_MEMBER, response.getJSONObject(RESULTS).getBoolean(IS_MEMBER));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(HP_COMPLETED, response.getJSONObject(RESULTS).getBoolean(HP_COMPLETED));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(PROFILE_IMAGE, response.getJSONObject(RESULTS).getString(PROFILE_IMAGE));
                        SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, response.getJSONObject(RESULTS).toString());

                        Intent i = new Intent(LandingBaseActivity.this, Home.class);
                       // i.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i);
                       // supportFinishAfterTransition();


                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onResponseError() {
                startActivity(new Intent(LandingBaseActivity.this, Login.class));
                supportFinishAfterTransition();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, false);
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        Log.i("GoogleLocationApi", "User agreed to make required location settings changes.");
                        break;
                    case Activity.RESULT_CANCELED:

                        Log.i("GoogleLocationApi", "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }


    @Override
    public void postOnCreate() {

    }

    @Override
    public View getLayoutView() {
        return null;
    }

    @Override
    public View getHeaderLayoutView() {
        return null;
    }

    @Override
    public int getHeaderLayoutID() {
        return 0;
    }

    @Override
    public View getFooterLayoutView() {
        return null;
    }

    @Override
    public int getFooterLayoutID() {
        return 0;
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {

    }

    @Override
    public int getDrawerLayoutID() {
        return R.layout.landing_drawer;
    }

    @Override
    public boolean shouldKeyboardHideOnOutsideTouch() {
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
