package com.bho.bho.Fragments;


import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bho.R;
import com.bho.bho.POJO.EntriesObject;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartFragment;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getUserId;

/**
 * A simple {@link Fragment} subclass.
 */
public class PaymentHistory extends SmartFragment {

    JSONArray paymentDataJson = new JSONArray();
    ArrayList<EntriesObject> paymentDataArrayList = new ArrayList<>();
    ArrayList<JSONObject> paymentDataSetArrayList = new ArrayList<>();

    private RecyclerView paymentHistoryRv;
    private View paymentHistoryNoData;
    private SmartTextView paymentEntriesCountTv;
    private SmartTextView paymentPreTv;
    private SmartTextView paymentNxtTv;

    private PaymentHistoryAdapter paymentHistoryAdapter;

    private int pageNo = 0;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_payment_history;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public void initComponents(View currentView) {
        getPaymentHistory();
        paymentHistoryRv = currentView.findViewById(R.id.payment_history_rv);
        paymentHistoryNoData = currentView.findViewById(R.id.payment_history_no_data);
        paymentEntriesCountTv = currentView.findViewById(R.id.payment_entries_count_tv);
        paymentPreTv = currentView.findViewById(R.id.payment_pre_tv);
        paymentNxtTv = currentView.findViewById(R.id.payment_nxt_tv);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(getActivity());
        paymentHistoryRv.setLayoutManager(linearLayoutManager);

        paymentHistoryAdapter = new PaymentHistoryAdapter();
        paymentHistoryRv.setAdapter(paymentHistoryAdapter);
        //Log.e("@@Module_test", "" + (0 % 10));


    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

        paymentNxtTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo < paymentDataArrayList.size() - 1) {
                    pageNo = pageNo + 1;
                    setEntryData();
                }
            }
        });

        paymentPreTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (pageNo > 0) {
                    pageNo = pageNo - 1;
                    setEntryData();
                }
            }
        });
    }


    private void getPaymentHistory() {
        SmartUtils.showLoadingDialog(getActivity());
        final JSONObject params = new JSONObject();
        try {
            params.put(USER_ID, getUserId());
        } catch (JSONException e) {
            e.printStackTrace();
        }

        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "PaymentHistory");
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, getActivity());
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

            @Override
            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                SmartUtils.hideLoadingDialog();
                if (responseCode == 200) {
                    try {
                        //   noDataFound.setVisibility(View.GONE);
                         paymentDataJson = response.getJSONArray(RESULTS);

                       /* for (int i = 1; i <= 27; i++) {
                            try {
                                paymentDataJson.put(new JSONObject().put(PAYMENT_DESC, "desc" + i));
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
*/
                        if (paymentDataJson.length() > 0) {
                            paymentHistoryNoData.setVisibility(View.GONE);
                        } else {
                            paymentHistoryNoData.setVisibility(View.VISIBLE);
                        }

                        ArrayList<JSONObject> dataSet = new ArrayList<>();
                        for (int i = 1; i <= paymentDataJson.length(); i++) {
                            dataSet.add(paymentDataJson.getJSONObject(i - 1));
                        //    Log.e("@@Module_test", "" + (i % 10));
                            int k = i % 10;
                            if (k == 0 || i == paymentDataJson.length()) {
                                int firstEntry, lastEntry;
                                if (paymentDataArrayList.size() == 0) {
                                    firstEntry = 1;
                                } else {
                                    firstEntry = 10 * paymentDataArrayList.size();
                                }
                                lastEntry = i;

                                paymentDataArrayList.add(new EntriesObject(firstEntry, lastEntry, dataSet));
                                dataSet = new ArrayList<>();
                            }
                        }
                        paymentDataSetArrayList = paymentDataArrayList.get(pageNo).getDataSet();
                        setEntryData();
                        paymentHistoryAdapter.notifyDataSetChanged();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            }

            @Override
            public void onResponseError() {

                SmartUtils.hideLoadingDialog();
                Log.e("@@ERROR_HERE", "ERROR_HERE");
            }
        });
        SmartWebManager.getInstance(getActivity()).addToRequestQueue(requestParams, false);
    }

    private void setEntryData() {
        paymentDataSetArrayList = paymentDataArrayList.get(pageNo).getDataSet();
        paymentEntriesCountTv.setText("Showing " +
                paymentDataArrayList.get(pageNo).getFirstEntry()
                + " to " +
                paymentDataArrayList.get(pageNo).getLastEntry()
                + " of " +
                paymentDataJson.length()
                + " entries"
        );
        paymentHistoryAdapter.notifyDataSetChanged();
    }


    public class PaymentHistoryAdapter extends RecyclerView.Adapter<PaymentHistoryAdapter.ViewHolder> {


        @Override
        public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
            View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_payment_history, viewGroup, false);
            return new ViewHolder(v);
        }

        @Override
        public void onBindViewHolder(final ViewHolder holder, final int position) {
            try {
                holder.descPaymentTv.setText(paymentDataSetArrayList.get(position).getString(PAYMENT_DESC));
                holder.createdOnPaymentTv.setText(paymentDataSetArrayList.get(position).getString(PAYMENT_CREATED_DATE));
                holder.amountPaymentTv.setText(paymentDataSetArrayList.get(position).getString(PAYMENT_AMOUNT));
                holder.transactionNoPaymentTv.setText(paymentDataSetArrayList.get(position).getString(PAYMENT_TRASACTION_ID));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        @Override
        public int getItemCount() {
            return paymentDataSetArrayList.size();
        }

        public class ViewHolder extends RecyclerView.ViewHolder {
            private SmartTextView descPaymentTv;
            private SmartTextView createdOnPaymentTv;
            private SmartTextView amountPaymentTv;
            private SmartTextView transactionNoPaymentTv;


            public ViewHolder(View itemView) {
                super(itemView);
                descPaymentTv = itemView.findViewById(R.id.desc_payment_tv);
                createdOnPaymentTv = itemView.findViewById(R.id.created_on_payment_tv);
                amountPaymentTv = itemView.findViewById(R.id.amount_payment_tv);
                transactionNoPaymentTv = itemView.findViewById(R.id.transaction_no_payment_tv);
            }
        }
    }

}
