package com.bho.bho.Fragments;


import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.bho.R;
import com.bho.bho.Activities.BHOServices.ExpandedConsultRequest;
import com.bho.bho.Activities.BHOServices.ConsultChatArchive;
import com.bho.bho.Activities.Home;
import com.bho.bho.Activities.BHOServices.MyChart;
import com.bho.bho.Activities.BHOServices.MyHealthTrendRequest;
import com.bho.bho.Activities.BHOServices.QuickConsultRequest;
import com.bho.bho.Activities.SideMenuOptions.Testimonial.Testimonial;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartFragment;
import com.braintreepayments.api.dropin.DropInActivity;
import com.braintreepayments.api.dropin.DropInRequest;
import com.braintreepayments.api.dropin.DropInResult;
import com.braintreepayments.api.models.PaymentMethodNonce;

import static com.bho.smart.framework.SmartUtils.getUserId;

/**
 * A simple {@link Fragment} subclass.
 */
public class BhoService extends SmartFragment {

    boolean isDropDownOpen = false;

    int REQUEST_CODE = 508;
    String token = "eyJ2ZXJzaW9uIjoyLCJhdXRob3JpemF0aW9uRmluZ2VycHJpbnQiOiJhYzdkZmY5YzQwZDgyMmUzMDVkYzg5NTk1MGI5ZTg3YWJkYmUxZTZiZDY4YWZhYzczNTlhZDhmZGQwYWQwYjVmfGNyZWF0ZWRfYXQ9MjAxNy0xMi0yNlQwNzowNDoxNC44NjI2MTE2NTgrMDAwMFx1MDAyNm1lcmNoYW50X2lkPW1xbTlrbnoyOGZieHR4azJcdTAwMjZwdWJsaWNfa2V5PWIycW44dzMzcTN4dmZoM2YiLCJjb25maWdVcmwiOiJodHRwczovL2FwaS5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tOjQ0My9tZXJjaGFudHMvbXFtOWtuejI4ZmJ4dHhrMi9jbGllbnRfYXBpL3YxL2NvbmZpZ3VyYXRpb24iLCJjaGFsbGVuZ2VzIjpbXSwiZW52aXJvbm1lbnQiOiJzYW5kYm94IiwiY2xpZW50QXBpVXJsIjoiaHR0cHM6Ly9hcGkuc2FuZGJveC5icmFpbnRyZWVnYXRld2F5LmNvbTo0NDMvbWVyY2hhbnRzL21xbTlrbnoyOGZieHR4azIvY2xpZW50X2FwaSIsImFzc2V0c1VybCI6Imh0dHBzOi8vYXNzZXRzLmJyYWludHJlZWdhdGV3YXkuY29tIiwiYXV0aFVybCI6Imh0dHBzOi8vYXV0aC52ZW5tby5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tIiwiYW5hbHl0aWNzIjp7InVybCI6Imh0dHBzOi8vY2xpZW50LWFuYWx5dGljcy5zYW5kYm94LmJyYWludHJlZWdhdGV3YXkuY29tL21xbTlrbnoyOGZieHR4azIifSwidGhyZWVEU2VjdXJlRW5hYmxlZCI6dHJ1ZSwicGF5cGFsRW5hYmxlZCI6dHJ1ZSwicGF5cGFsIjp7ImRpc3BsYXlOYW1lIjoiRWJpenRyYWl0IiwiY2xpZW50SWQiOm51bGwsInByaXZhY3lVcmwiOiJodHRwOi8vZXhhbXBsZS5jb20vcHAiLCJ1c2VyQWdyZWVtZW50VXJsIjoiaHR0cDovL2V4YW1wbGUuY29tL3RvcyIsImJhc2VVcmwiOiJodHRwczovL2Fzc2V0cy5icmFpbnRyZWVnYXRld2F5LmNvbSIsImFzc2V0c1VybCI6Imh0dHBzOi8vY2hlY2tvdXQucGF5cGFsLmNvbSIsImRpcmVjdEJhc2VVcmwiOm51bGwsImFsbG93SHR0cCI6dHJ1ZSwiZW52aXJvbm1lbnROb05ldHdvcmsiOnRydWUsImVudmlyb25tZW50Ijoib2ZmbGluZSIsInVudmV0dGVkTWVyY2hhbnQiOmZhbHNlLCJicmFpbnRyZWVDbGllbnRJZCI6Im1hc3RlcmNsaWVudDMiLCJiaWxsaW5nQWdyZWVtZW50c0VuYWJsZWQiOnRydWUsIm1lcmNoYW50QWNjb3VudElkIjoiZWJpenRyYWl0IiwiY3VycmVuY3lJc29Db2RlIjoiVVNEIn0sIm1lcmNoYW50SWQiOiJtcW05a256MjhmYnh0eGsyIiwidmVubW8iOiJvZmYifQ==";


    private SmartTextView chooseBhoTv;
    private SmartTextView requestServiceTv;
    private SmartTextView testimonialTv;
    private RelativeLayout selectLanguageRl;
    private LinearLayout dropDownLl;
    private SmartTextView quickConsultTv;
    private SmartTextView expandedConsultTv;
    private SmartTextView myChartTv;
    private SmartTextView myHealthTv;
    private SmartTextView selectServiceTv;
    private SmartTextView qcArchiveTv;
    private SmartTextView ecArchiveTv;

    private int requestCode = 0, QC_REQUEST = 0, EC_REQUEST = 1, MC_REQUEST = 2, MHT_REQUEST = 3;

    private Dialog quickConsultDialog, expandedConsultDialog;

    @Override
    public int setLayoutId() {
        return R.layout.fragment_bho_service;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public void initComponents(View currentView) {
        selectLanguageRl = currentView.findViewById(R.id.select_language_rl);
        dropDownLl = currentView.findViewById(R.id.drop_down_ll);


        chooseBhoTv = currentView.findViewById(R.id.choose_bho_tv);

        requestServiceTv = currentView.findViewById(R.id.request_service_tv);
        testimonialTv = currentView.findViewById(R.id.testimonial_tv);

        quickConsultTv = currentView.findViewById(R.id.quick_consult_tv);
        expandedConsultTv = currentView.findViewById(R.id.expanded_consult_tv);
        myChartTv = currentView.findViewById(R.id.my_chart_tv);
        myHealthTv = currentView.findViewById(R.id.my_health_tv);

        selectServiceTv = currentView.findViewById(R.id.select_service_tv);

        qcArchiveTv = currentView.findViewById(R.id.qc_archive_tv);
        ecArchiveTv = currentView.findViewById(R.id.ec_archive_tv);
    }


    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {
        selectServiceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openCloseDropDown();
            }
        });

        testimonialTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                startActivity(new Intent(getActivity(), Testimonial.class));
            }
        });

        quickConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCode = 0;
                openCloseDropDown();
                selectServiceTv.setText("QuickConsult (QC)");
//                startActivity(new Intent(getActivity(), QuickConsultRequest.class));
            }
        });


        expandedConsultTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCode = 1;
                openCloseDropDown();
                selectServiceTv.setText("ExpandedConsult (EC)");
            }
        });


        myChartTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCode = 2;
                openCloseDropDown();
                selectServiceTv.setText("MyChart (MC)");
            }
        });


        myHealthTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestCode = 3;
                openCloseDropDown();
                selectServiceTv.setText("MyHealthTrends (MHT)");
            }
        });

        requestServiceTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                switch (requestCode) {
                    case 0:
                        showQuickConsultDialog();
                        break;
                    case 1:
                        showExpandedConsultDialog();
                        break;
                    case 2:
                        String url = "http://52.205.252.190/MemberProfile/ConsumerMyChartMobile?id=" + getUserId();
                        startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(url)));
                        break;
                    case 3:
                        startActivity(new Intent(getActivity(), MyHealthTrendRequest.class));
                        break;
                }
            }
        });

        final Intent i = new Intent(getActivity(), ConsultChatArchive.class);

        qcArchiveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra(CONSULT_TYPE, QC);
                startActivity(i);
            }
        });

        ecArchiveTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                i.putExtra(CONSULT_TYPE, EC);
                startActivity(i);
            }
        });

    }

    public void openCloseDropDown() {
        if (!isDropDownOpen) {
            dropDownLl.setVisibility(View.VISIBLE);
            Animation slideOut = AnimationUtils.loadAnimation(getActivity(), R.anim.slideout_animation);
            dropDownLl.startAnimation(slideOut);
            isDropDownOpen = true;
        } else {
            Animation slideIn = AnimationUtils.loadAnimation(getActivity(), R.anim.slidein_animation);
            slideIn.setAnimationListener(animationListener);
            dropDownLl.startAnimation(slideIn);
            isDropDownOpen = false;
        }
    }

    private Animation.AnimationListener animationListener = new Animation.AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {

        }

        @Override
        public void onAnimationEnd(Animation animation) {
            if (dropDownLl.getVisibility() == View.VISIBLE) {
                dropDownLl.clearAnimation();
                dropDownLl.setVisibility(View.GONE);
            }
        }

        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };

    private void showQuickConsultDialog() {
        quickConsultDialog = new Dialog(getActivity());
        quickConsultDialog.setContentView(R.layout.dialog_quick_consult);


        ImageView closeIv = quickConsultDialog.findViewById(R.id.close_iv);
        SmartTextView editHealthProfileTv = quickConsultDialog.findViewById(R.id.edit_health_profile_tv);
        SmartTextView proceedQuickTv = quickConsultDialog.findViewById(R.id.proceed_quick_tv);


        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quickConsultDialog.dismiss();
            }
        });

        editHealthProfileTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                quickConsultDialog.dismiss();
                startActivity(new Intent(getActivity(), Home.class));
            }
        });

        if (SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getBoolean(HP_COMPLETED, false)) {
            proceedQuickTv.setVisibility(View.VISIBLE);
            proceedQuickTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    quickConsultDialog.dismiss();
                    startActivity(new Intent(getActivity(), QuickConsultRequest.class));
                }
            });
        } else {
            proceedQuickTv.setVisibility(View.GONE);
        }


        quickConsultDialog.show();
    }

    private void showExpandedConsultDialog() {
        expandedConsultDialog = new Dialog(getActivity());
        expandedConsultDialog.setContentView(R.layout.dialog_expanded_consult);

        ImageView closeIv = expandedConsultDialog.findViewById(R.id.close_iv);
        SmartTextView editHealthProfileTv = expandedConsultDialog.findViewById(R.id.edit_health_profile_tv);
        SmartTextView proceedExpandedTv = expandedConsultDialog.findViewById(R.id.proceed_expanded_tv);

        closeIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandedConsultDialog.dismiss();
            }
        });

        editHealthProfileTv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                expandedConsultDialog.dismiss();
                startActivity(new Intent(getActivity(), Home.class));
            }
        });


        if (SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getBoolean(HP_COMPLETED, false)) {
            proceedExpandedTv.setVisibility(View.VISIBLE);
            proceedExpandedTv.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    expandedConsultDialog.dismiss();
                    startActivity(new Intent(getActivity(), ExpandedConsultRequest.class));
                }
            });
        } else {
            proceedExpandedTv.setVisibility(View.GONE);
        }


        expandedConsultDialog.show();
    }
}
