package com.bho.bho.Fragments;

import android.content.Intent;
import android.view.View;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.Activities.HomeForms.AllergiesReaction;
import com.bho.bho.Activities.HomeForms.CurrentMedication;
import com.bho.bho.Activities.HomeForms.FamilyHistory;
import com.bho.bho.Activities.HomeForms.PastMedicalHistory;
import com.bho.bho.Activities.HomeForms.PastSurgicalHistory;
import com.bho.bho.Activities.HomeForms.PersonalInformation;
import com.bho.bho.Activities.HomeForms.ReviewsOfSystems;
import com.bho.bho.Activities.HomeForms.SocialHistory;
import com.bho.bho.Activities.HomeForms.TravelHistory;
import com.bho.smart.caching.SmartCaching;
import com.bho.smart.framework.SmartFragment;


public class HealthProfile extends SmartFragment {

    private LinearLayout personalInfoLl;
    private LinearLayout allergiesLl;
    private LinearLayout currentMedicationLl;
    private LinearLayout medicalHistoryLl;
    private LinearLayout surgicalHistoryLl;
    private LinearLayout reviewSystemsLl;
    private LinearLayout socialHistoryLl;
    private LinearLayout travelHistoryLl;
    private LinearLayout familyHistoryLl;

    private SmartCaching smartCaching;

    public HealthProfile() {
        // Required empty public constructor
    }

    @Override
    public int setLayoutId() {
        return R.layout.fragment_health_profile;
    }

    @Override
    public View setLayoutView() {
        return null;
    }

    @Override
    public void initComponents(View currentView) {
        personalInfoLl = currentView.findViewById(R.id.personal_info_ll);
        allergiesLl = currentView.findViewById(R.id.allergies_ll);
        currentMedicationLl = currentView.findViewById(R.id.current_medication_ll);
        medicalHistoryLl = currentView.findViewById(R.id.medical_history_ll);
        surgicalHistoryLl = currentView.findViewById(R.id.surgical_history_ll);
        reviewSystemsLl = currentView.findViewById(R.id.review_systems_ll);
        socialHistoryLl = currentView.findViewById(R.id.social_history_ll);
        travelHistoryLl = currentView.findViewById(R.id.travel_history_ll);
        familyHistoryLl = currentView.findViewById(R.id.family_history_ll);

        smartCaching = new SmartCaching(getActivity());

        setHasOptionsMenu(true);

    }

    @Override
    public void prepareViews(View currentView) {

    }

    @Override
    public void setActionListeners(View currentView) {

        personalInfoLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PersonalInformation.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        allergiesLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), AllergiesReaction.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        currentMedicationLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), CurrentMedication.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        medicalHistoryLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PastMedicalHistory.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        surgicalHistoryLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), PastSurgicalHistory.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        reviewSystemsLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), ReviewsOfSystems.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);

            }
        });

        socialHistoryLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), SocialHistory.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        travelHistoryLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), TravelHistory.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });

        familyHistoryLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startActivity(new Intent(getActivity(), FamilyHistory.class));
                getActivity().overridePendingTransition(R.anim.slide_in_right, R.anim.slide_out_right);
            }
        });
    }
}
