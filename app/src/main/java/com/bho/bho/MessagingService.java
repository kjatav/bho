/**
 * Copyright 2016 Google Inc. All Rights Reserved.
 * <p>
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * <p>
 * http://www.apache.org/licenses/LICENSE-2.0
 * <p>
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.bho.bho;

import android.app.Notification;
import android.app.PendingIntent;
import android.content.Intent;
import android.os.Build;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.util.Log;

import com.bho.R;
import com.bho.bho.Activities.SideMenuOptions.Notifications;
import com.bho.smart.framework.Constants;
import com.bho.smart.framework.SmartApplication;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;

import java.util.Map;

public class MessagingService extends FirebaseMessagingService implements Constants {

    private static final String TAG = "@@Firebase";
    private static int count = 0;

    /**
     * Called when message is received.
     *
     * @param remoteMessage Object representing the message received from Firebase Cloud Messaging.
     */
    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        Log.i("XXXXXXXXXXXXX", "Pankaj");
        String s = remoteMessage.getNotification().getBody();
        Map s1 = remoteMessage.getData();
        Log.e("@@", s1.toString());
        Log.d(TAG, "Notification Body: " + s);
        Log.d(TAG, "Message Notification Title: " + remoteMessage.getNotification().getTitle());
        Log.d(TAG, "Message Notification Body: " + remoteMessage.getNotification().getBody());
        Log.d(TAG, "Message Data Payload: " + remoteMessage.getData());


        if (remoteMessage.getNotification() != null) {
            sendNotificationTest(remoteMessage.getNotification(), remoteMessage.getData());
        }
    }


    private void sendNotificationTest(RemoteMessage.Notification notification, Map<String, String> data) {
        Intent intent = new Intent(this, Notifications.class);


        if (intent != null) {
            String contentText = "", contentTitle = "";
            switch (data.get("messageFor")) {
                case "Payment":
                    contentTitle = "Payment done";
                    break;
                /*case "Payment":
                    contentTitle = "Payment done";
                    break;*/
            }
            if (notification.getTitle() != null) {
                switch (notification.getTitle()) {
                    case "QuickConsult":
                        contentText = "Payment for Quick Consult Request has been received";
                        break;
                    case "QuickConsult and Gold Membership":
                        contentText = "Payment for Quick Consult And Gold Membership Request has been received";
                        break;
                    case "ExpandedConsult":
                        contentText = "Payment for Expanded Consult Request has been received";
                        break;
                    case "ExpandedConsult and Gold Membership":
                        contentText = "Payment for Expanded Consult And Gold Membership Request has been received";
                        break;
                    case "MyhealthTrends":
                        contentText = "Payment for My Health Trend Request has been received";
                        break;
                    case "MyhealthTrends  and Gold Membership":
                        contentText = "Payment for My Health Trend And Gold Membership Request has been received";
                        break;
                    case "Gold Membership":
                        contentText = "Payment for Golden Member has been received";
                        break;
                }
            }

            PendingIntent pendingIntent = PendingIntent.getActivity(this, (int) (Math.random() * 100),
                    intent, PendingIntent.FLAG_ONE_SHOT);
            NotificationManagerCompat notificationManager = NotificationManagerCompat.from(this);
            NotificationCompat.Builder notificationBuilder = new NotificationCompat.Builder(this)
                    .setSmallIcon(R.drawable.ic_bho_notification)
                    .setContentTitle(contentTitle)
                    .setContentText(contentText)
                    .setTicker(getString(R.string.app_name))
                    .setAutoCancel(true)
                    .setDefaults(-1)
                    .setContentIntent(pendingIntent);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                notificationBuilder.setPriority(Notification.PRIORITY_HIGH);
            }
            notificationManager.notify(count, notificationBuilder.build());
            count++;
        }
    }
}