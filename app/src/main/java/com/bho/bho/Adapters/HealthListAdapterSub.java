package com.bho.bho.Adapters;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.Activities.LandingScreens.HealthListWebview;
import com.bho.bho.POJO.HealthListSubObject;
import com.bho.smart.customViews.SmartTextView;

import java.util.ArrayList;

import static com.bho.smart.framework.Constants.IS_CONSUMER;

/**
 * Created by ebiztrait321 on 25/1/18.
 */

public class HealthListAdapterSub extends RecyclerView.Adapter<HealthListAdapterSub.ViewHolder> {

    private Context context;
    private ArrayList<HealthListSubObject> healthListSubObjects;
    private boolean isConsumer;

    public HealthListAdapterSub(Context context, ArrayList<HealthListSubObject> healthListSubObjects, boolean isConsumer) {
        this.context = context;
        this.healthListSubObjects = healthListSubObjects;
        this.isConsumer = isConsumer;
    }

    @Override
    public HealthListAdapterSub.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new HealthListAdapterSub.ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_health_professionals_sub, viewGroup, false));
    }

    @Override
    public void onBindViewHolder(final HealthListAdapterSub.ViewHolder holder, final int position) {
        holder.titleHealthListSubTv.setText(healthListSubObjects.get(position).getContentSubjectName());
        holder.healthListSubLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(context, HealthListWebview.class);
                i.putExtra("html_data", healthListSubObjects.get(position).getContentSubjectDescription());
                i.putExtra(IS_CONSUMER, isConsumer);
                context.startActivity(i);
            }
        });
    }


    @Override
    public int getItemCount() {
        return healthListSubObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout healthListSubLl;
        private SmartTextView titleHealthListSubTv;


        public ViewHolder(View itemView) {
            super(itemView);

            healthListSubLl = itemView.findViewById(R.id.health_list_sub_ll);
            titleHealthListSubTv = itemView.findViewById(R.id.title_health_list_sub_tv);
        }
    }
}

