package com.bho.bho.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bho.R;
import com.bho.smart.customViews.SmartEditText;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 11/12/17.
 */

public class AdditionalFieldsAdapter extends RecyclerView.Adapter<AdditionalFieldsAdapter.ViewHolder> {

    Context context;
    ArrayList<String> additionalFieldsdata;
    boolean isUpdated = false;

    public AdditionalFieldsAdapter(Context context, ArrayList<String> additionlsFieldsdata) {
        this.context = context;
        this.additionalFieldsdata = additionlsFieldsdata;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_additional_fields, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.addFieldsMedicalHistoryEt.setText(additionalFieldsdata.get(position));
        holder.addFieldsMedicalHistoryEt.setEnabled(false);

        holder.doneFieldIv.setVisibility(View.GONE);
        holder.editFieldIv.setVisibility(View.VISIBLE);

        holder.editFieldIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                holder.editFieldIv.setVisibility(View.GONE);
                holder.doneFieldIv.setVisibility(View.VISIBLE);

                holder.addFieldsMedicalHistoryEt.setEnabled(true);
                holder.addFieldsMedicalHistoryEt.requestFocus();
                holder.addFieldsMedicalHistoryEt.setSelection(holder.addFieldsMedicalHistoryEt.getText().toString().trim().length());
            }
        });
        holder.doneFieldIv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(holder.addFieldsMedicalHistoryEt.getText().toString().trim())) {
                    holder.doneFieldIv.setVisibility(View.GONE);
                    holder.editFieldIv.setVisibility(View.VISIBLE);
                    holder.addFieldsMedicalHistoryEt.setEnabled(false);
                    additionalFieldsdata.set(position, holder.addFieldsMedicalHistoryEt.getText().toString().trim());
                    isUpdated = true;
                } else {
                    Toast.makeText(context, "Field cannot be empty", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    public void addAdditionalFields(String additionalFieldStr) {
        additionalFieldsdata.add(additionalFieldStr);

        notifyDataSetChanged();
    }

    public boolean giveUpadateStautus() {
        return isUpdated;
    }

    public JSONArray getAdditionalFields() {
        return new JSONArray(additionalFieldsdata);
    }

    @Override
    public int getItemCount() {
        return additionalFieldsdata.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private SmartEditText addFieldsMedicalHistoryEt;
        private ImageView editFieldIv;
        private ImageView doneFieldIv;


        public ViewHolder(View itemView) {
            super(itemView);
            addFieldsMedicalHistoryEt = itemView.findViewById(R.id.add_fields_allergies_et);
            editFieldIv = itemView.findViewById(R.id.edit_field_iv);
            doneFieldIv = itemView.findViewById(R.id.done_field_iv);
        }
    }
}