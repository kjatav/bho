package com.bho.bho.Adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.bho.R;
import com.bho.bho.Activities.HomeForms.PastMedicalHistory;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import static com.bho.smart.framework.Constants.ID;
import static com.bho.smart.framework.Constants.IS_CHECKED;
import static com.bho.smart.framework.Constants.NAME;
import static java.util.jar.Pack200.Unpacker.FALSE;
import static java.util.jar.Pack200.Unpacker.TRUE;

/**
 * Created by ebiztrait321 on 11/12/17.
 */

public class MedicalItemsAdapters extends RecyclerView.Adapter<MedicalItemsAdapters.ViewHolder> {

    Context context;
    ArrayList<JSONObject> medicalItemsArray;
    ArrayList<String> checkedItems = new ArrayList<>();
    private boolean isUpdated = false;


    public MedicalItemsAdapters(Context context, ArrayList<JSONObject> medicalItemsArray) {
        this.context = context;
        this.medicalItemsArray = medicalItemsArray;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_medical_items, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            holder.medicalItemCb.setText(medicalItemsArray.get(position).getString(NAME));
            if (medicalItemsArray.get(position).getString(IS_CHECKED).equalsIgnoreCase(TRUE)) {
                holder.medicalItemCb.setChecked(true);
                checkedItems.add(medicalItemsArray.get(position).getString(ID));
            }
            holder.medicalItemCb.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                    try {
                        isUpdated = true;
                        if (isChecked) {
                            medicalItemsArray.get(position).put(IS_CHECKED, TRUE);
                            checkedItems.add(medicalItemsArray.get(position).getString(ID));
                        } else {
                            medicalItemsArray.get(position).put(IS_CHECKED, FALSE);
                            checkedItems.remove(medicalItemsArray.get(position).getString(ID));

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    public int getItemCount() {
        return medicalItemsArray.size();
    }

    public JSONArray getCheckedItemsIds() {
        return new JSONArray(checkedItems);
    }

    public String getCheckedItems() {
        return medicalItemsArray.toString();
    }

    public boolean giveMedicalItemUpdateStatus() {
        return isUpdated;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private CheckBox medicalItemCb;


        public ViewHolder(View itemView) {
            super(itemView);
            medicalItemCb = itemView.findViewById(R.id.medical_item_cb);
        }
    }


}