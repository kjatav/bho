package com.bho.bho.Adapters;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Toast;

import com.bho.R;
import com.bho.smart.customViews.SmartEditText;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 11/12/17.
 */

public class PartnersAdapter extends RecyclerView.Adapter<PartnersAdapter.ViewHolder> {

    ArrayList<Drawable> drawableArrayList;

    public PartnersAdapter(ArrayList<Drawable> drawableArrayList) {
        this.drawableArrayList = drawableArrayList;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        View v = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_partners, viewGroup, false);
        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.partnersIv.setImageDrawable(drawableArrayList.get(position));
    }


    @Override
    public int getItemCount() {
        return drawableArrayList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        ImageView partnersIv;

        public ViewHolder(View itemView) {
            super(itemView);

            partnersIv =  itemView.findViewById(R.id.partners_iv);
        }
    }
}