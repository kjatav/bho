package com.bho.bho.Adapters;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.POJO.HealthListObject;
import com.bho.bho.POJO.HealthListSubObject;
import com.bho.smart.customViews.SmartTextView;

import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 25/1/18.
 */

public class HealthListAdapterMain extends RecyclerView.Adapter<HealthListAdapterMain.ViewHolder> {

    private Context context;
    private ArrayList<HealthListObject> healthListObjects;
    private RecyclerView rv;
    private boolean isConsumer;

    public HealthListAdapterMain(Context context, ArrayList<HealthListObject> healthListObjects, RecyclerView rv, boolean isConsumer) {
        this.context = context;
        this.healthListObjects = healthListObjects;
        this.rv = rv;
        this.isConsumer = isConsumer;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        return new ViewHolder(LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.row_health_professionals_main, viewGroup, false));
    }


    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        holder.titleHealthListTv.setText(healthListObjects.get(position).getSubjectName());

        holder.healthListLl.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                rv.scrollToPosition(position);
                if (healthListObjects.get(position).isOpen()) {
                    holder.healthListSubRv.setVisibility(View.GONE);
                    healthListObjects.get(position).setOpen(false);
                } else {
                    holder.healthListSubRv.setVisibility(View.VISIBLE);
                    healthListObjects.get(position).setOpen(true);
                }
            }
        });

        ArrayList<HealthListSubObject> healthListSubObjects = new ArrayList<>();
        try {
            for (int i = 0; i < healthListObjects.get(position).getHealthListSubJson().length(); i++) {
                healthListSubObjects.add(new HealthListSubObject(
                        healthListObjects.get(position).getHealthListSubJson().getJSONObject(i).getString("ContentSubjectName"),
                        healthListObjects.get(position).getHealthListSubJson().getJSONObject(i).getString("ContentDiscription")));
            }
            HealthListAdapterSub healthListAdapterSub = new HealthListAdapterSub(context, healthListSubObjects,isConsumer);
            holder.healthListSubRv.setAdapter(healthListAdapterSub);
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }


    @Override
    public int getItemCount() {
        return healthListObjects.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private LinearLayout healthListLl;
        private RecyclerView healthListSubRv;
        private SmartTextView titleHealthListTv;

        public ViewHolder(View itemView) {
            super(itemView);
            healthListLl = itemView.findViewById(R.id.health_list_ll);
            healthListSubRv = itemView.findViewById(R.id.health_list_sub_rv);
            titleHealthListTv = itemView.findViewById(R.id.title_health_list_tv);

            LinearLayoutManager linearLayoutManager = new LinearLayoutManager(context);
            healthListSubRv.setLayoutManager(linearLayoutManager);


        }
    }
}
