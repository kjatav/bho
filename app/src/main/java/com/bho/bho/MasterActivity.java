package com.bho.bho;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.Window;
import android.widget.LinearLayout;

import com.bho.R;
import com.bho.bho.Activities.Home;
import com.bho.bho.Activities.Login;
import com.bho.bho.Activities.SideMenuOptions.AboutUs;
import com.bho.bho.Activities.SideMenuOptions.Notifications;
import com.bho.bho.Activities.SideMenuOptions.Payment;
import com.bho.bho.Activities.SideMenuOptions.Profile;
import com.bho.bho.Activities.SideMenuOptions.TermsAndConditions;
import com.bho.bho.Activities.SideMenuOptions.Testimonial.Testimonial;
import com.bho.smart.customViews.RoundedImageView;
import com.bho.smart.customViews.SmartTextView;
import com.bho.smart.framework.AlertMagnatic;
import com.bho.smart.framework.SmartApplication;
import com.bho.smart.framework.SmartSuperMaster;
import com.bho.smart.framework.SmartUtils;
import com.bho.smart.weservice.SmartWebManager;
import com.google.android.gms.common.api.GoogleApiClient;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

import static com.bho.smart.framework.SmartUtils.getIsMember;


public abstract class MasterActivity extends SmartSuperMaster {

    /**
     * Provides the entry point to Google Play services.
     */
    protected GoogleApiClient mGoogleApiClient;


    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec

    private static int FATEST_INTERVAL = 5000; // 5 sec

    /**
     * Constant used in the location settings dialog.
     */
    private static final int REQUEST_CHECK_SETTINGS = 0x8;

    private final int PERMISSIONS_REQUEST_CODE = 501;


    private SmartTextView headerToolbarTv;

    private LinearLayout navHeaderLl;
    private LinearLayout ugLl;
    private RoundedImageView userProfileNav;
    private SmartTextView userNameNav;
    private SmartTextView upgradeNav;
    private SmartTextView homeNav;
    private SmartTextView aboutUsNav;
    private SmartTextView notificationsNav;
    private SmartTextView profileNav;
    private SmartTextView testimonialNav;
    private SmartTextView termsNav;
    private SmartTextView logoutNav;


    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    @Override
    public void setAnimations() {
        getWindow().requestFeature(Window.FEATURE_CONTENT_TRANSITIONS);
        getWindow().setAllowEnterTransitionOverlap(true);
        getWindow().setAllowReturnTransitionOverlap(true);
    }

    @Override
    public void preOnCreate() {

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M &&
                checkSelfPermission(Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED
                && checkSelfPermission(Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.ACCESS_FINE_LOCATION,
                            Manifest.permission.ACCESS_COARSE_LOCATION,
                            Manifest.permission.READ_PHONE_STATE,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA},
                    PERMISSIONS_REQUEST_CODE);
            return;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    public void initComponents() {
        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayShowTitleEnabled(false);
        }

        headerToolbarTv = findViewById(R.id.header_toolbar_tv);

        if (getDrawerLayoutID() != 0) {
            navHeaderLl = findViewById(R.id.nav_header_ll);
            ugLl = findViewById(R.id.ug_ll);
            userProfileNav = findViewById(R.id.user_profile_nav);
            userNameNav = findViewById(R.id.user_name_nav);
            homeNav = findViewById(R.id.home_nav);
            upgradeNav = findViewById(R.id.upgrade_nav);
            aboutUsNav = findViewById(R.id.about_us_nav);
            notificationsNav = findViewById(R.id.notifications_nav);
            profileNav = findViewById(R.id.profile_nav);
            testimonialNav = findViewById(R.id.testimonial_nav);
            termsNav = findViewById(R.id.terms_nav);
            logoutNav = findViewById(R.id.logout_nav);

            if (getIsMember()) {
                ugLl.setVisibility(View.GONE);
            }
        }
    }

    @Override
    public void prepareViews() {
        if (getDrawerLayoutID() != 0) {
            JSONObject user = SmartUtils.getUser();
            if (user != null) {
                setNavDetails(SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(PROFILE_IMAGE, NO_DATA));
            }
        }
    }

    public void setNavDetails(String img_url) {
        if (!TextUtils.isEmpty(img_url))
            Picasso.with(this).load(img_url).placeholder(R.drawable.ic_profile_place_holder).into(userProfileNav);
        else {
            userProfileNav.setImageResource(R.drawable.ic_profile_place_holder);
        }
//        userNameNav.setText(first_name + " " + last_name);
    }

    @Override
    public void setActionListeners() {
        if (getDrawerLayoutID() != 0) {
            navHeaderLl.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            upgradeNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MasterActivity.this, Payment.class);
                    i.putExtra(SERVICE_TYPE, UG);
                    startActivity(i);
                }
            });

            homeNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MasterActivity.this, Home.class));
                }
            });

            aboutUsNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MasterActivity.this, AboutUs.class);
                    i.putExtra(FROM_NAV, true);
                    startActivity(i);
                }
            });


            notificationsNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MasterActivity.this, Notifications.class));
                }
            });


            profileNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MasterActivity.this, Profile.class));
                }
            });


            testimonialNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(MasterActivity.this, Testimonial.class));
                }
            });


            termsNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent i = new Intent(MasterActivity.this, TermsAndConditions.class);
                    i.putExtra(FROM_NAV, false);
                    startActivity(i);
                }
            });


            logoutNav.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    doLogout();
                }
            });
        }
    }


    public void setHeaderToolbar(String heading) {
        headerToolbarTv.setText(heading);
    }

    public void setHeaderMarginZero() {
        Toolbar.LayoutParams lp = new Toolbar.LayoutParams(headerToolbarTv.getLayoutParams());
        lp.setMargins(0, 0, 0, 0);
        lp.gravity = Gravity.CENTER;
        headerToolbarTv.setLayoutParams(lp);
    }

    public void doLogout() {
        SmartUtils.getConfirmDialog(MasterActivity.this, "Logout", "Are you sure you want to logout?",
                "Yes", "No", true, new AlertMagnatic() {

                    @Override
                    public void PositiveMethod(DialogInterface dialog, int id) {
                        SmartUtils.showLoadingDialog(MasterActivity.this);
                        JSONObject params = new JSONObject();
                        try {
                            params.put(USER_ID, SmartApplication.REF_SMART_APPLICATION.readSharedPreferences().getString(USER_ID, NO_USER_ID));

                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                        HashMap<SmartWebManager.REQUEST_METHOD_PARAMS, Object> requestParams = new HashMap<>();
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.URL, getString(R.string.domain_name) + "Logout");
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.CONTEXT, MasterActivity.this);
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.PARAMS, params);
                        requestParams.put(SmartWebManager.REQUEST_METHOD_PARAMS.RESPONSE_LISTENER, new SmartWebManager.OnResponseReceivedListener() {

                            @Override
                            public void onResponseReceived(final JSONObject response, boolean isValidResponse, int responseCode) {
                                SmartUtils.hideLoadingDialog();
                                if (responseCode == 200) {
                                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_NAME, null);
                                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_ID, null);
                                    SmartApplication.REF_SMART_APPLICATION.writeSharedPreferences(USER_DATA, null);
                                    Intent intent = new Intent(MasterActivity.this, Login.class);
                                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(intent);
                                }
                            }

                            @Override
                            public void onResponseError() {

                                SmartUtils.hideLoadingDialog();
                                Log.e("@@ERROR_HERE", "ERROR_HERE");
                            }
                        });
                        SmartWebManager.getInstance(getApplicationContext()).addToRequestQueue(requestParams, true);

                    }

                    @Override
                    public void NegativeMethod(DialogInterface dialog, int id) {

                    }
                });
    }


    protected void storageRequest() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && checkSelfPermission(Manifest.permission.WRITE_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED && checkSelfPermission(Manifest.permission.READ_EXTERNAL_STORAGE)
                != PackageManager.PERMISSION_GRANTED) {

            requestPermissions(new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,
                    Manifest.permission.READ_EXTERNAL_STORAGE}, PERMISSIONS_REQUEST_CODE);
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        switch (requestCode) {
            // Check for the integer request code originally supplied to startResolutionForResult().
            case REQUEST_CHECK_SETTINGS:
                switch (resultCode) {
                    case Activity.RESULT_OK:

                        Log.i("GoogleLocationApi", "User agreed to make required location settings changes.");
                        break;
                    case Activity.RESULT_CANCELED:

                        Log.i("GoogleLocationApi", "User chose not to make required location settings changes.");
                        break;
                }
                break;
        }
    }


    @Override
    public void postOnCreate() {

    }

    @Override
    public View getLayoutView() {
        return null;
    }

    @Override
    public View getHeaderLayoutView() {
        return null;
    }

    @Override
    public int getHeaderLayoutID() {
        return 0;
    }

    @Override
    public View getFooterLayoutView() {
        return null;
    }

    @Override
    public int getFooterLayoutID() {
        return 0;
    }

    @Override
    public void manageAppBar(ActionBar actionBar, Toolbar toolbar, ActionBarDrawerToggle actionBarDrawerToggle) {

    }

    @Override
    public int getDrawerLayoutID() {
        return R.layout.drawer;
    }

    @Override
    public boolean shouldKeyboardHideOnOutsideTouch() {
        return true;
    }

    @Override
    protected void onPause() {
        super.onPause();
    }

    @Override
    protected void onStop() {
        super.onStop();

    }
}
