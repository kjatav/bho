package com.bho.bho.POJO;

/**
 * Created by ebiztrait321 on 12/12/17.
 */

public class CurrentMedicationField {

    String estimate;
    String medication;
    String dose;
    String frequency;

    public CurrentMedicationField(String medication, String dose, String frequency, String estimate) {
        this.medication = medication;
        this.dose = dose;
        this.frequency = frequency;
        this.estimate = estimate;
    }

    public String getMedication() {
        return medication;
    }

    public void setMedication(String medication) {
        this.medication = medication;
    }

    public String getDose() {
        return dose;
    }

    public void setDose(String dose) {
        this.dose = dose;
    }

    public String getFrequency() {
        return frequency;
    }

    public void setFrequency(String frequency) {
        this.frequency = frequency;
    }

    public String getEstimate() {
        return estimate;
    }

    public void setEstimate(String estimate) {
        this.estimate = estimate;
    }



}
