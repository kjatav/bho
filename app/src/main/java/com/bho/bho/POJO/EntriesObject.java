package com.bho.bho.POJO;

import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 15/12/17.
 */

public class EntriesObject {
    int firstEntry,lastEntry;
    ArrayList<JSONObject> dataSet;

    public EntriesObject(int firstEntry, int lastEntry, ArrayList<JSONObject> dataSet) {
        this.firstEntry = firstEntry;
        this.lastEntry = lastEntry;
        this.dataSet = dataSet;
    }

    public int getFirstEntry() {
        return firstEntry;
    }

    public void setFirstEntry(int firstEntry) {
        this.firstEntry = firstEntry;
    }

    public int getLastEntry() {
        return lastEntry;
    }

    public void setLastEntry(int lastEntry) {
        this.lastEntry = lastEntry;
    }

    public ArrayList<JSONObject> getDataSet() {
        return dataSet;
    }

    public void setDataSet(ArrayList<JSONObject> dataSet) {
        this.dataSet = dataSet;
    }
}
