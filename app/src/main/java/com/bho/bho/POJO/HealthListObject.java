package com.bho.bho.POJO;

import android.content.Context;

import org.json.JSONArray;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 29/1/18.
 */

public class HealthListObject {

    private Context context;
    private boolean isOpen;
    private String subjectName;
    private JSONArray healthListSubJson;

    public HealthListObject(Context context, boolean isOpen, String subjectName, JSONArray healthListSubJson) {
        this.context = context;
        this.isOpen = isOpen;
        this.subjectName = subjectName;
        this.healthListSubJson = healthListSubJson;
    }

    public Context getContext() {
        return context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public boolean isOpen() {
        return isOpen;
    }

    public void setOpen(boolean open) {
        isOpen = open;
    }

    public String getSubjectName() {
        return subjectName;
    }

    public void setSubjectName(String subjectName) {
        this.subjectName = subjectName;
    }

    public JSONArray getHealthListSubJson() {
        return healthListSubJson;
    }

    public void setHealthListSubJson(JSONArray healthListSubJson) {
        this.healthListSubJson = healthListSubJson;
    }
}
