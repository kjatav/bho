package com.bho.bho.POJO;

import android.content.Context;

import java.util.ArrayList;

/**
 * Created by ebiztrait321 on 29/1/18.
 */

public class HealthListSubObject {

    String contentSubjectName;
    String contentSubjectDescription;

    public HealthListSubObject( String contentSubjectName, String contentSubjectDescription) {
        this.contentSubjectName = contentSubjectName;
        this.contentSubjectDescription = contentSubjectDescription;
    }

    public String getContentSubjectName() {
        return contentSubjectName;
    }

    public void setContentSubjectName(String contentSubjectName) {
        this.contentSubjectName = contentSubjectName;
    }

    public String getContentSubjectDescription() {
        return contentSubjectDescription;
    }

    public void setContentSubjectDescription(String contentSubjectDescription) {
        this.contentSubjectDescription = contentSubjectDescription;
    }
}
